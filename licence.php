
<!doctype html>
<html lang="fr">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title><? echo $titre ?></title>
<link rel="stylesheet" href="/css/style.css" type="text/css">
<link rel="stylesheet" media="only screen and (max-width: 760px)" href="/css/mobile.css" type="text/css">
</head>
    
<body>
    <div id="conteneur">
        <div id="header">
        <a href="<? echo $urlsite ?>">
        <img src="logo-1lientop-min.jpg" class="logo" alt="logo 1lien.top"></a>
        <h1>Licence</h1>
        <input type="button" onclick="window.location.href ='<?php echo $urlsite;?>';" class="boutondesection" value="Revenir à l'accueil" style="margin-bottom: 3%;">
        </div>
        <div id="footer">
        <h4>"LICENCE TROC"   (T'es Ravi ? Offre une Contrepartie !) :</h4>
        <p> theophile/at/captp.fr a écrit ce logiciel. Tant que vous conservez cet avertissement, vous pouvez faire ce que vous voulez de ce truc, <b>SAUF</b> en rendre l'accès restreint par paiement obligatoire.<br>
  => Si on se rencontre un jour et que vous pensez que ce truc vaut le coup, vous pouvez me payer une bière en retour.<br>
  => Si vous êtes trop impatients pour m'offrir une bière, alors je serai ravi de recevoir une carte postale de vous ! Voici mon adresse :<br><br>
      Théophile Bellintani<br>
      21 Avenue Léon Blum, Batiment K<br>
      38100 Grenoble<br>
      FRANCE<br><br><br>
    </p>
    <p><?php echo $version; ?> — © Théophile Bellintani — Licence <a href="licence.php">TROC</a>, <a href="https://framagit.org/Cellophile/personalink">Opensource</a> et gratuit !</p>
    </div>
</body>
</html>
