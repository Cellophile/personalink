# PersonaLink
[![Latest Release](https://git.captp.fr/theophile/personalink/-/badges/release.svg)](https://git.captp.fr/theophile/personalink/-/releases)

Version 2.0.1
15/10/2024
Auteur : Théophile Bellintani (theophile at captp.fr)
Licence : TROC (voir Licence)

PersonaLink est un raccourcisseur et personnalisateur d'URLs codé en php, respectueux des données privées et bien plus facile à installer que Lstu. Un tableau de bord personnel permet de retrouver tous ses liens raccourcis, de voir le nombre de fois où ils ont servi, de les modifier...

**Démonstration : https://1lien.top**

**Avantages par rapport au LSTU** (Let's Shorten That URL) :
	
- Pas besoin de faire des manips compliquées pour l'installation, tout est décrit ci-dessous.
	
- 🥷 Anonymise les liens, pour que personne ne puisse retrouver ce qu'on a mis derrière un lien. 
	  *Ce n'est pas le cas sur LSTU, qui nous dit que le texte est déjà pris, mais du coup qui révèle le lien par la même...  
	  Pour la confidentialité, c'est pas top.*
	
- ✅ Vérifie que l'URL vers où rediriger existe bien. ça ne fait pas de lien mort d'avance. (Sauf si la page internet décède après création du lien...)
	
- 💾 Ne nécessite pas de database !!! Fonctionne avec un fichier csv.

- ⚙️ Possibilité de se créer un compte, pour retrouver tous ses liens, les modifier, les suivre...

- 👤 Aucune information utilisateur n'est enregistrée, aucune analyse des liens, ... Parce que l'ignorance, c'est le bonheur. Sauf pour celleux qui se créent un espace personnel, évidemment.

- 🤖 Vérification par captcha, pour éviter les robots spammeurs.

- 🧑‍💻️ Connectable en api (qualibrée pour répondre à privatebin) Toutes les informations sont dans le fichier /api/lis-moi.txt

- 🔒 Possibilité de mettre un mot de passe et une date d'expiration sur le lien court.

- ⛔ Blacklistage des adresse IP abusives. Pour l'instant, dans le but de lutter contre l'utilisation frauduleuse, cette fonctionnalité est agressive. A voir avec le temps comment ça évolue.



## INSTALLATION

1. Installer le présent programme sur votre hébergement, et faites pointer votre nom de domaine vers le dossier `/public`.
   
2. Rendez-vous sur votre domaine en ligne : Un formulaire d'installation vous permettra de définir votre utilisateur admin, ainsi que le titre et sous-titre du site. Ces paramètres seront modifiables ensuite depuis votre espace administrateur.
   
3. Pour activer le fichier cron.php, il faut avoir un hébergement qui le permet. Rapprochez-vous de votre hébergeur si vous ne savez pas comment faire.
   
4. Si vous connectez une API, vous devez activer l'api depuis votre espace administrateur.

**Il n'y a rien d'autre à tripoter pour que ça fonctionne !** 

## CONNEXION EN API

pour que l'API fonctionne, il faut que le client ait cette adresse à laquelle écrire :

```php
// https://VOTRE_DOMAINE.fr/api?action=raccourcir&key=API_KEY&format=json&mdp=VOTRE_MDP&datexpi=2020-01-01 00:00&url=LIEN_À_RACCOURCIR
```
### Règles d'utilisation

| variable | valeur(s) | utilisation | requis ? |
|----------|-----------|-------------|----------|
| action | raccourcir | permet de préciser qu'on utilise l'api pour raccourcir une url. n'attend aucune autre valeur actuellement. | oui |
| key | la clé d'api | Dans le cas où vous définissez une ou plusieurs clés d'api dans le fichier config, il faudra la donner au site externe. Il est fortement conseillé d'utiliser la méthode POST lorsqu'on utilise la clé d'api, pour des raisons de sécurité. | non |
| format | • json <br> • xml <br> • html <br> • text | permet de préciser sous quel format on veut recevoir les données après le raccourcissement. | oui |
| mdp | VOTRE_MDP | le mot de passe à mettre sur le lien | non |
| datexpi | 2025-01-01 00:00 | La date (et l'heure) d'expiration du lien | non |
| url | l'url à raccourcir | C'est la partie la plus importante.  | oui |



Vous devez également autoriser le site en question à utiliser l'API.
Pour cela, dans le fichier `config.php`, modifier et/ou ajoutez votre site externe à la variable `API_ACCESS`.
Il es possible de remplacer le tableau par "*" pour autoriser n'importe quel site, mais ce n'est pas recommandé, à moins que vous n'utilisiez des clés d'api. 

Dans ce cas, définissez les clés d'API dans la variable `API_KEY`. 

Il faudra enfin mettre `API_ACTIVATED` à `true`, sans quoi la porte restera close ! 😉

## INFORMATIONS COMPLEMENTAIRES

### Combien d'URLs peut-on raccourcir avec ce programme ?
Une infinité, que j'ai pas cherché à calculer.

### Quelles données personnelles sont récoltées ?
- Si quelqu'un crée un lien sans se connecter :

L'adresse IP est associée au lien créé, afin de lutter contre les robots spammeurs et contre les pirates qui font passer leurs liens frauduleux sous cette application.

- Si quelqu'un se crée un compte :

Dans ce cas, l'utilisateur concent à renseigner un nom, un prénom, une adresse mail, une adresse IP pour se créer un compte. Ces informations ne sont pas utilisées à d'autres fins que l'identification de la personne lors de sa connexion. 

### Combien de temps durent les liens ?
Si vous n'avez pas mis de date d'expiration, le lien n'expire jamais. (Tant que ce programme tourne !)
Si vous avez un compte, vous pouvez modifier la date d'expiration.

### Puis-je savoir si on a cliqué sur mes liens raccourcis ?
Si vous avez un compte, oui, le nombre de clic est enregistré. Sans compte, cela est impossible. 