<?php

use Services\Cache;

spl_autoload_register(function ($class) {
  $class = str_replace('\\', '/', $class);
  if (file_exists(__DIR__ . '/' . $class . '.php')) {
    include __DIR__ . '/' . $class . '.php';
  }
});


register_shutdown_function(function () {
  Cache::destroy();
});