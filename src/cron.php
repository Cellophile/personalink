<?php

include __DIR__ . "/autoload.php";

use Controllers\ParametreController;
use Models\IpBlacklistee;
use Repositories\IpBlacklisteeRepository;
use Repositories\LienRepository;
use Repositories\UserRepository;
ParametreController::initialize();

// Supprimer les liens expirés :
echo "première partie : ";
$lienRepo = new LienRepository();
$liens = $lienRepo->findAll();
$nbLiensSuppr = 0;

if (!empty($liens)) {
	foreach ($liens as $lien) {
		if ($lien->getDatexpi() && $lien->getDatexpi() < new DateTime('now')) {
			$lienRepo->delete($lien);
			$nbLiensSuppr++;
		}
	}
}

echo "a fonctionné : $nbLiensSuppr liens supprimés. <br>";

// Supprimer les comptes créés mais pas activés après un jour :
echo "Deuxième partie : ";

$userRepo = new UserRepository();
$users = $userRepo->findAll();
$nbComptesSuppr = 0;
$nbIpsBlacklist = 0;

if (!empty($users)) {
	foreach ($users as $user) {
		if (!$user->isActivated() && $user->getDateCreation() < new DateTime('yesterday') && $user->getDateDernierLogin() === null) {
			
			if ($user->getLiens()) {
				foreach ($user->getLiens() as $lien) {
					$lienRepo->delete($lien);
				}
			}
			$IpBlacklistRepo = new IpBlacklisteeRepository();
			$Ips = $user->getIp()->all();
			if (!is_array($Ips)) {
				$Ips = [$Ips];
			}
			foreach ($Ips as $ip) {
				if(!$IpBlacklistRepo->findBy(["ip" => $ip->getIp()])){
					$ipbl = $ip->blacklist();
				}
				$IpBlacklistRepo->add($ipbl);
				$nbIpsBlacklist++;
			}
			$userRepo->delete($user);
			$nbComptesSuppr++;
		}
	}
}

echo " a fonctionné : $nbComptesSuppr comptes supprimés, et $nbIpsBlacklist IPs blacklistées à vérifier.";