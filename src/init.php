<?php

use Controllers\InstallationController;
use Controllers\ParametreController;
use Services\SitemapRedactor;

session_start();

//Initiatisation :
date_default_timezone_set("Europe/Paris");
$av 	 	 = "a";
$urlbase 	 = "b";
$returnerror = 0;


if (isset($_SESSION['auth']) && isset($_SESSION['user']) && $_SESSION['auth'] == 'valide') {
	$auth = "valide";
}
include __DIR__ . "/autoload.php";
ParametreController::initialize();
SitemapRedactor::generateSitemap();
if ((new InstallationController())->install()) {
	require __DIR__ . '/router.php';
}