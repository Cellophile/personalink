<?php
namespace Controllers;

use Repositories\ParametreRepository;
use Services\Abstracts\AbstractController;

class ParametreController extends AbstractController
{

  /**
   * Cette méthode permet de charger les paramètres de l'application depuis le fichier DB_Parametres.csv. 
   * Cependant, si un de ces paramètres a été défini dans le fichier config.php, il ne sera pas pris en compte.
   * Les paramètres de config.php sont donc prioritaires.
   *
   * @return void
   */
  public static function initialize(): void
  {
    include __DIR__ . '/../../config.php';
    $parametreRepo = new ParametreRepository;
    $parametres = $parametreRepo->findAll();

    foreach ($parametres as $parametre) {
      if (!defined($parametre->getParametre())){
        define($parametre->getParametre(), $parametre->getValeur());
      }
    }
  }
}