<?php

namespace Controllers;

use DateTime;
use Models\Lien;
use Models\User;
use Repositories\LienRepository;
use Repositories\UserRepository;
use Services\Abstracts\AbstractController;
use Services\Validator;

class DashboardController extends AbstractController
{
  private $userRepo;
  private $lienRepo;
  public function __construct()
  {
    $this->userRepo = new UserRepository;
    $this->lienRepo = new LienRepository;
  }

  public function accueil()
  {
    $this->render('dashboard/index', [
      'user' => $this->getUser(),
      'liens' => $this->getUser()->getLiens()->all()
    ]);
  }

  /**   API   **/

  public function updateUser()
  {
    $donnees = $this->request()->getData();
    $updatedUser = new User;
    $user = $this->getUser();

    if (isset($donnees['nom'])) {
      $updatedUser->setNom(ucfirst(strtolower($donnees['nom'])));
    }
    if (isset($donnees['prenom'])) {
      $updatedUser->setPrenom(ucfirst(strtolower($donnees['prenom'])));
    }
    if (isset($donnees['email'])) {
      $updatedUser->setEmail($donnees['email']);
    }
    if (isset($donnees['password']) && isset($donnees['new_password'])) {
      if (password_verify($donnees['password'], $user->getPassword())) {
        $updatedUser->setPassword($donnees['new_password']);
        $updatedUser->setLenPwd(strlen($donnees['new_password']));
      } else {
        $this->json(null, 406);
        exit;
      }
    }
    $errors = Validator::validate($updatedUser);
    if ($errors) {
      foreach ($donnees as $donnee => $value) {
        if (isset($errors[$donnee])) {
          $error[] = $errors[$donnee];
        }
      }
      if ($error) {
        $this->json($error, 400);
        exit;
      }
    }
    foreach ($updatedUser->getObjectVars() as $key => $value) {
      $authorizedKeys = ['nom','prenom','email','password','lenPwd'];
      if (in_array($key, $authorizedKeys)) {
        $setter = 'set' . $key;
        if (method_exists($user, $setter) && $value !== null) {
          $user->$setter($value);
        }
      }
    }
    $this->userRepo->update($user);
    SecurityController::updateSession(['user' => $user]);

    $this->json(null, 202);
  }

  /**
   * Méthode pour supprimer un utilisateur. Si aucun utilisateur n'est donné, l'utilisateur courant est supprimé. 
   *
   * @param User|null $user
   * @return void
   */
  public function deleteUser()
  {
    $data = $this->request()->getData();
    if (password_verify($data['password'], $this->getUser()->getPassword())) {
      $user = $this->getUser();
      if ($data['suppr_liens']) {
        $liens = $user->getLiens()->all();
        foreach ($liens as $lien) {
          $this->lienRepo->delete($lien);
        }
      } else {
        $liens = $user->getLiens()->all();
        foreach ($liens as $lien) {
          $lien->setUser(null);
          $this->lienRepo->update($lien);
        }
      }
      $this->userRepo->delete($user);
      session_destroy();
      $this->json(null, 202);
    } else {
      $this->json(null, 401);
    }
  }

  public function updateLien()
  {
    $data = $this->request()->getData();
    $updatedLien = new Lien;
    if (!isset($data['upersok'])) {
      $this->json('Référence manquante', 400);
      exit;
    }
    $lien = $this->lienRepo->findBy(['upersok' => $data['upersok']]);
    if ($lien) {
      foreach ($data as $key => $value) {
        if ($key == 'datexpi' || $key == 'dcreation') {
          $value = $value === 'null' ? null : new DateTime($value);
        }
        $setter = 'set' . $key;
        if (method_exists($lien, $setter)) {
          $updatedLien->$setter($value);
        }
      }
      $errors = Validator::validate($updatedLien);
      if ($errors) {
        $error = [];
        foreach ($data as $donnee => $value) {
          if (isset($errors[$donnee])) {
            $error[] = $errors[$donnee];
          }
        }
        if ($error) {
          $this->json($error, 400);
          exit;
        }
      }

      foreach ($updatedLien->getObjectVars() as $key => $value) {
        if ($key == 'datexpi' || $key == 'dcreation') {
          $value = $value === null ? null : DateTime::createFromFormat('d/m/Y - H:i', $value);
        }
        $setter = 'set' . $key;
        if (method_exists($lien, $setter) && ($value !== null && $value !== false)) {
          $lien->$setter($value);
        }
      }
      $this->lienRepo->update($lien);
      $this->json(null, 202);
    } else {
      $this->json('Aucun lien à mettre à jour', 404);
    }
  }

  public function deleteLien()
  {
    $data = $this->request()->getData();
    $liens = $this->getUser()->getLiens()->all();
    foreach ($liens as $lien) {
      if ($lien->getUpersok() == $data['upersok']) {
        $this->lienRepo->delete($lien);
        $this->json(null, 202);
        exit;
      }
    }
    $this->json('Aucun de vos liens n\'a été supprimé', 404);
  }

  public function erreurJson404()
  {
    $this->json('api inexistante', 404);
  }
}
