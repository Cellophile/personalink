<?php

namespace Controllers;

use DateTime;
use Models\User;
use Repositories\UserRepository;
use Services\Abstracts\AbstractController;
use Services\AntiPishing;
use Services\Cache;
use Services\Components\Collection;
use Services\EnvoiEmail;
use Services\Functions;
use Services\Validator;

class SecurityController extends AbstractController
{
	private UserRepository $UserRepo;

	public function __construct()
	{
		$this->UserRepo = new UserRepository;
	}

	public function inscription()
	{
		AntiPishing::isIpBlacklist(AntiPishing::getIp());

		if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['adressemail']) && isset($_POST['pass_word']) && isset($_POST['confirm_pass_word'])) {
			if ($_POST['pass_word'] === $_POST['confirm_pass_word']) {
				$data = [
					'email' => Functions::sanitizeString($_POST['adressemail']),
					'nom' => Functions::sanitizeString($_POST['nom']),
					'prenom' => Functions::sanitizeString($_POST['prenom']),
					'password' => password_hash($_POST['pass_word'], PASSWORD_DEFAULT),
					'lenPwd' => strlen($_POST['pass_word']),
					'DateCreation' => (new DateTime())->format('d/m/Y - H:i'),
					'ip' => new Collection(AntiPishing::getIp())
				];

				if ($this->UserRepo->findBy(['email' => $data['email']])) {
					$this->redirectToRoute(HOME_URL . '?echec=302', self::HTTP_NOT_ACCEPTABLE);
				}

				$user = new User($data);
				$errors = Validator::validate($user);
				if ($errors) {
					$erreurs = [];
					foreach ($data as $donnee => $value) {
						if (isset($errors[$donnee])) {
							$erreurs[$donnee] = $errors[$donnee];
						}
					}
					if ($erreurs) {
						$this->render('accueil', [
							'erreurs' => $erreurs,
							'user' => $user							
						]);
						exit;
					}
				}
				$this->UserRepo->add($user);
				$this->EmailConfirmationCreationCompte($user);
			} else {
				$this->redirectToRoute(HOME_URL . '?echec=307', self::HTTP_UNAUTHORIZED);
			}
		} else {
			$this->redirectToRoute(HOME_URL . '?echec=308', self::HTTP_UNPROCESSABLE_ENTITY);
		}
	}

	public function activation()
	{
		if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['token'])) {


			$email = htmlentities($_POST['email'], ENT_QUOTES);
			$password = htmlentities($_POST['password'], ENT_QUOTES);
			$token = htmlentities($_POST['token'], ENT_QUOTES);

			$user = $this->UserRepo->findBy(['email' => $email]);
			if ($user && password_verify($password, $user->getPassword())) {

				$user->setActivated(true);
				$this->UserRepo->update($user);

				$this->redirectToRoute(HOME_URL . '?succes=4', self::HTTP_OK);
			} else {
				$this->redirectToRoute(HOME_URL . 'activation?token=' . $token . '&echec=1', self::HTTP_UNAUTHORIZED);
			}
		}

		if (isset($_GET["token"]) && isset($_GET["email"])) {
			$email = htmlentities($_GET['email'], ENT_QUOTES);
			$user = $this->UserRepo->findBy(['email' => $email]);
			$this->render('activationCompte', [
				'user' => $user,
				'token' => $_GET['token']
			]);
		} else {
			$this->redirectToRoute(HOME_URL . '?echec=309', self::HTTP_FORBIDDEN);
		}
	}

	public function connexion()
	{
		$ip = AntiPishing::getIp();
		AntiPishing::isIpBlacklist($ip);

		if (isset($_POST['identifiant']) && isset($_POST['password'])) {

			$identifiant = htmlentities($_POST['identifiant'], ENT_QUOTES);
			$password = htmlentities($_POST['password'], ENT_QUOTES);

			$user = $this->UserRepo->findBy(['email' => $identifiant]);
			if ($user) {
				$user->addIp($ip);
				if (!$user->isActivated()) {
					$this->redirectToRoute(HOME_URL . '?echec=304', self::HTTP_UNAUTHORIZED);
				}
				if (password_verify($password, $user->getPassword())) {
					$_SESSION['auth'] = 'valide';
					$user->getLiens();
					$this->updateSession(['user' => $user]);
					$this->redirectToRoute(HOME_URL . 'dashboard/', self::HTTP_OK);
				} else {
					$user->setErreursAuthentification($user->getErreursAuthentification() + 1);
					$this->UserRepo->update($user);
					$this->redirectToRoute(HOME_URL . '?echec=301', self::HTTP_FORBIDDEN);
				}
			}
			$this->redirectToRoute(HOME_URL . '?echec=301', self::HTTP_FORBIDDEN);
		}
	}

	public function deconnexion(): void
	{
		$user = $this->getUser();
		if ($user) {
			$user->setDateDernierLogin(new DateTime());
			$this->UserRepo->update($user);
			session_destroy();
			Cache::destroy();
		}
		$this->redirectToRoute(HOME_URL, self::HTTP_OK);
	}

	public function oubliMotDePasse()
	{
		if (isset($_GET["mdp"]) && $_GET["mdp"] === "oubli" && isset($_GET["email"])) {
			echo 'condition';
			$identifiant = htmlentities($_GET["email"], ENT_QUOTES);

			self::EmailOubliMotDePasse($identifiant);
		} else {
			$this->redirectToRoute(HOME_URL, self::HTTP_OK);
		}
	}

	private function EmailOubliMotDePasse(string $email): void
	{
		$user = $this->UserRepo->findBy(['email' => $email]);

		if ($user) {

			$dest = $user->getEmail();
			$sujet = $this->getNomDomaine() . " : Réinitialisation de mot de passe.";

			$token = password_hash($user->getPassword(), PASSWORD_DEFAULT);

			$corps = "Bonjour,<br>
				Vous venez de demander la réinitialisation de votre mot de passe sur <a href=" . URLSITE . ">" . URLSITE . "</a>.<br>
				Pour modifier votre mot de passe, veuillez suivre ce lien :\n
				<a href='" . URLSITE . "reinitialisation-mdp?m=$dest&t=$token'>" . URLSITE . "reinitialisation-mdp?m=$dest&t=$token</a><br>
				Si vous n'avez pas demandé cette réinitialisation, ne faites rien, ou choisissez de renforcer votre mot de passe en le remplaçant par un nouveau plus solide.<br>
				<br>A bientôt !<br>
				<a href=\"" . URLSITE . "\">" . URLSITE . "</a><br>
				Ceci est un mail automatique.";

			if (EnvoiEmail::send($corps, $sujet, $dest)) {
				$this->redirectToRoute(HOME_URL . '?succes=5', self::HTTP_OK);
			} else {
				$this->redirectToRoute(HOME_URL . '?echec=305', self::HTTP_INTERNAL_SERVER_ERROR);
			}
		} else {
			// l'utilisateur n'existe pas, mais on affiche quand même un message de succès, pour éviter qu'un utilisateur malveillant puisse trouver les comptes des gens.
			$this->redirectToRoute(HOME_URL . '?succes=5', self::HTTP_OK);
		}
	}

	public function reinitialisationMotDePasse()
	{
		if (isset($_POST['password1']) && isset($_POST['password2']) && isset($_POST['email']) && isset($_POST['token'])) {
			if ($_POST['password1'] === $_POST['password2']) {
				$user = $this->UserRepo->findBy(['email' => $_POST['email']]);
				if ($user) {
					if (password_verify($user->getPassword(), $_POST['token'])) {
						$user->setPassword(password_hash($_POST['password1'], PASSWORD_DEFAULT));
						$user->setLenPwd(strlen($_POST['password1']));
						$this->UserRepo->update($user);
						$this->redirectToRoute(HOME_URL . '?succes=6', self::HTTP_OK);
					}
				}
				$this->redirectToRoute(HOME_URL . '?echec=306', self::HTTP_FOUND);
			} else {
				$this->redirectToRoute(HOME_URL . 'reinitialisation-mdp?echec=1', self::HTTP_UNAUTHORIZED);
			}
		}

		if (isset($_GET["m"]) && isset($_GET["t"])) {
			$dest = htmlentities($_GET["m"], ENT_QUOTES);
			$token = htmlentities($_GET["t"], ENT_QUOTES);
			$user = $this->UserRepo->findBy(['email' => $dest]);
			if ($user) {
				if (password_verify($user->getPassword(), $token)) {
					$this->render('reinitialisationMotDePasse', [
						'user' => $user,
						'token' => $token
					]);
					exit;
				}
			}
		}
		$this->redirectToRoute(HOME_URL . '?echec=306', self::HTTP_FOUND);
	}

	private function EmailConfirmationCreationCompte(User $user)
	{
		// On envoie un mail pour demander à la personne de confirmer la création de son compte.
		$token = password_hash($user->getEmail(), PASSWORD_DEFAULT);
		$dest = $user->getEmail();
		$sujet = $this->getNomDomaine() . " : Activez votre compte !";
		$corps = "Bonjour " . $user->getPrenom() . ",<br>
			Vous venez de demander la création d'un compte sur <a href=" . URLSITE . ">" . URLSITE . "</a>.<br>
			Pour activer votre compte, veuillez suivre ce lien :\n
			<a href=\"" . URLSITE . "activation?token=$token&email=" . $user->getEmail() . "\">" . URLSITE . "activation?token=$token&email=" . $user->getEmail() . "</a><br>
			<br>A bientôt !<br>
			<a href=\"" . URLSITE . "\">" . URLSITE . "</a><br>
			Ceci est un mail automatique.";

		if (EnvoiEmail::send($corps, $sujet, $dest)) {
			$this->redirectToRoute(HOME_URL . '?succes=2', self::HTTP_OK);
		} else {
			$this->redirectToRoute(HOME_URL . '?echec=305', self::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public static function updateSession(array $data): void
	{
		if (isset($_SESSION['auth']) && $_SESSION['auth'] === 'valide') {
			foreach ($data as $key => $value) {
				if (is_object($value)) {
					$value = serialize($value);
				}
				$_SESSION[$key] = $value;
			}
		}
	}
}
