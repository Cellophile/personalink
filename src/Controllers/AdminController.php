<?php

namespace Controllers;

use Models\IpBlacklistee;
use Models\urlBlacklistee;
use Repositories\LienRepository;
use Repositories\UserRepository;
use Repositories\UrlBlacklisteeRepository;
use Repositories\IpBlacklisteeRepository;
use Repositories\ParametreRepository;
use Services\Abstracts\AbstractController;
use Services\Statistic;
use Services\Validator;
use stdClass;

class AdminController extends AbstractController
{
  private $userRepo, $lienRepo, $urlBlacklisteeRepo, $ipBlacklisteeRepo, $parametreRepo;

  public function __construct()
  {
    $this->userRepo = new UserRepository;
    $this->lienRepo = new LienRepository;
    $this->urlBlacklisteeRepo = new UrlBlacklisteeRepository;
    $this->ipBlacklisteeRepo = new IpBlacklisteeRepository;
    $this->parametreRepo = new ParametreRepository;
  }

  public function accueil()
  {
    $users = $this->userRepo->findAll();
    $liens = $this->lienRepo->findAll();
    $urlBlacklistees = $this->urlBlacklisteeRepo->findAll();
    $ipBlacklistees = $this->ipBlacklisteeRepo->findAll();

    $stats = new stdClass;
    $Statistic = new Statistic;
    $stats->nb_users = count($users);
    $stats->nb_liens = count($liens);
    $stats->MoyenneNbLiensParUser = $Statistic->getMoyenneNbLiensParUser($users);
    $stats->MoyenneNbClicsParLien = $Statistic->getMoyenneNbClicsParLien($liens);
    $stats->nb_liensAvecDatePeremption = $Statistic->getTotalLiensAvecDatePeremption($liens);
    $stats->nb_liensAvecMdp = $Statistic->getTotalLiensAvecMdp($liens);
    $stats->nb_urlBlacklistees = count($urlBlacklistees);
    $stats->nb_ipBlacklistees = count($ipBlacklistees);
    $stats->NbLiensCreesParPeriode = $Statistic->getNbLiensCreesParPeriode($liens);
    $stats->NbUsersCreesParPeriode = $Statistic->getNbUsersCreesParPeriode($users);

    $this->render('admin/index', [
      'currentUser' => $this->getUser(),
      'users' => $users,
      'liens' => $liens,
      'urlBlacklistees' => $urlBlacklistees,
      'ipBlacklistees' => $ipBlacklistees,
      'stats' => $stats
    ]);
  }

  public function desactivationUser()
  {
    $data = $this->request()->getData();
    if (!isset($data['user'])) {
      $this->json('Email manquant', self::HTTP_BAD_REQUEST);
      exit;
    }
    $user = $this->userRepo->findBy(['email' => $data['user']]);
    $user->setActivated(false);
    $errors = Validator::validate($user);
    if ($errors) {
      $this->json($errors, self::HTTP_BAD_REQUEST);
      exit;
    }
    $this->userRepo->update($user);

    $this->json(null, self::HTTP_OK);
  }

  public function activationUser()
  {
    $data = $this->request()->getData();
    if (!isset($data['user'])) {
      $this->json('Email manquant', self::HTTP_BAD_REQUEST);
      exit;
    }
    $user = $this->userRepo->findBy(['email' => $data['user']]);
    $user->setActivated(true);
    $errors = Validator::validate($user);
    if ($errors) {
      $this->json($errors, self::HTTP_BAD_REQUEST);
      exit;
    }
    $this->userRepo->update($user);

    $this->json(null, self::HTTP_OK);
  }

  public function blacklistUser()
  {
    $data = $this->request()->getData();
    if (!isset($data['user'])) {
      $this->json('Email manquant', self::HTTP_BAD_REQUEST);
      exit;
    }
    $user = $this->userRepo->findBy(['email' => $data['user']]);
    $ips = [];
    foreach ($user->getIp()->all() as $ip) {
      $ipBlacklistee = $ip->blacklist();
      $ipBlacklistee->setEtat(IpBlacklistee::ETAT_VERIFIE);
      if (!$this->ipBlacklisteeRepo->findBy(['ip' => $ip->getIp()])) {
        $this->ipBlacklisteeRepo->add($ipBlacklistee);
        $ips[] = $ip->getIp();
      } else {
        $this->json('IP déjà blacklistée', self::HTTP_OK);
        exit();
      }
    }
    $this->json($ips, self::HTTP_OK);
  }

  public function deblacklistUser()
  {
    $data = $this->request()->getData();
    if (!isset($data['user'])) {
      $this->json('Email manquant', self::HTTP_BAD_REQUEST);
      exit;
    }
    $user = $this->userRepo->findBy(['email' => $data['user']]);
    $ips = [];
    foreach ($user->getIp()->all() as $ip) {
      $ip = $this->ipBlacklisteeRepo->findBy(['ip' => $ip->getIp()]);
      if ($ip) {
        $this->ipBlacklisteeRepo->delete($ip);
        $ips[] = $ip->getIp();
      }
    }
    $this->json($ips, self::HTTP_OK);
  }

  public function deleteUser()
  {
    $data = $this->request()->getData();
    $user = $this->userRepo->findBy(['email' => $data['user']]);
    if ($data['suppr_all_liens']) {
      $liens = $user->getLiens()->all();
      foreach ($liens as $lien) {
        $this->lienRepo->delete($lien);
      }
    } else {
      $liens = $user->getLiens()->all();
      foreach ($liens as $lien) {
        $lien->setUser(null);
        $this->lienRepo->update($lien);
      }
    }
    $this->userRepo->delete($user);
    $this->json(null, self::HTTP_OK);
  }

  public function blacklistIp()
  {
    $data = $this->request()->getData();
    if (!isset($data['ip'])) {
      $this->json('Adresse IP manquante', self::HTTP_BAD_REQUEST);
      exit;
    } else if (!filter_var($data['ip'], FILTER_VALIDATE_IP)) {
      $this->json('Adresse IP invalide', self::HTTP_BAD_REQUEST);
      exit;
    }
    $ip = $this->ipBlacklisteeRepo->findBy(['ip' => $data['ip']]);
    if (!$ip) {
      $ip = new IpBlacklistee();
      $ip->setIp($data['ip']);
      $ip->setDateBlacklistage(new \DateTime());
      $ip->setEtat(IpBlacklistee::ETAT_VERIFIE);
      $this->ipBlacklisteeRepo->add($ip);
      $this->json($ip, self::HTTP_OK);
    } else {
      $this->json('IP déjà blacklistée', self::HTTP_ALREADY_REPORTED);
    }
  }

  public function deblacklistIp()
  {
    $data = $this->request()->getData();
    if (!isset($data['ip'])) {
      $this->json('Adresse IP manquante', self::HTTP_BAD_REQUEST);
      exit;
    }
    $ip = $this->ipBlacklisteeRepo->findBy(['ip' => $data['ip']]);
    if ($ip) {
      $this->ipBlacklisteeRepo->delete($ip);
      $this->json(null, self::HTTP_OK);
    } else {
      $this->json('IP non blacklistée', self::HTTP_OK);
    }
  }

  public function blacklistUrl()
  {
    $data = $this->request()->getData();
    if (!isset($data['url'])) {
      $this->json('URL manquante', self::HTTP_BAD_REQUEST);
      exit;
    } else if (!filter_var($data['url'], FILTER_VALIDATE_DOMAIN)) {
      $this->json('URL invalide', self::HTTP_BAD_REQUEST);
      exit;
    }
    $url = $this->urlBlacklisteeRepo->findBy(['url' => $data['url']]);
    if (!$url) {
      $url = new urlBlacklistee;
      $url->setUrl($data['url']);
      $errors = Validator::validate($url);
      if ($errors) {
        $this->json($errors, self::HTTP_BAD_REQUEST);
        exit;
      }
      $this->urlBlacklisteeRepo->add($url);
      $this->json($url, self::HTTP_OK);
    } else {
      $this->json('URL déjà blacklistée', self::HTTP_ALREADY_REPORTED);
    }
  }

  public function deblacklistUrl()
  {
    $data = $this->request()->getData();
    if (!isset($data['url'])) {
      $this->json('URL manquante', self::HTTP_BAD_REQUEST);
      exit;
    }
    $url = $this->urlBlacklisteeRepo->findBy(['url' => $data['url']]);
    if ($url) {
      $this->urlBlacklisteeRepo->delete($url);
      $this->json(null, self::HTTP_OK);
    } else {
      $this->json('URL non blacklistée', self::HTTP_OK);
    }
  }

  public function erreurJson404()
  {
    $this->json('api inexistante', 404);
  }

  public function VerificationIp()
  {
    $data = $this->request()->getData();
    if (!isset($data['ip'])) {
      $this->json('Adresse IP manquante', self::HTTP_BAD_REQUEST);
      exit;
    } else if (!filter_var($data['ip'], FILTER_VALIDATE_IP)) {
      $this->json('Adresse IP invalide', self::HTTP_BAD_REQUEST);
      exit;
    }
    $ip = $this->ipBlacklisteeRepo->findBy(['ip' => $data['ip']]);
    if ($ip) {
      $ip->setEtat(IpBlacklistee::ETAT_VERIFIE);
      $this->ipBlacklisteeRepo->update($ip);
      $this->json(null, self::HTTP_OK);
    } else {
      $this->json('IP non blacklistée', self::HTTP_OK);
    }
  }

  public function UpdateParametre()
  {
    $data = $this->request()->getData();
    if (!isset($data['parametre'])) {
      $this->json('Parametre manquant', self::HTTP_BAD_REQUEST);
      exit;
    }
    if (!isset($data['valeur']) && !isset($_FILES['valeur']) && $_FILES['valeur']['error'] !== 0) {
      $this->json('Valeur manquante', self::HTTP_BAD_REQUEST);
      exit;
    }

    $parametre = $this->parametreRepo->findBy(['parametre' => $data['parametre']]);
    if ($parametre) {
      if ($data['parametre'] === 'LOGO') {
        $data['valeur'] = $this->controlsImage();
      }
      if ($data['parametre'] === 'API_ACTIVATED') {
        $data['valeur'] = $data['valeur'] == 'true' ? true : false;
      }
      $parametre->setValeur($data['valeur']);
      $errors = Validator::validate($parametre);
      if ($errors) {
        $this->json($errors, self::HTTP_BAD_REQUEST);
        exit;
      }
      $this->parametreRepo->update($parametre);
      $this->json(null, self::HTTP_OK);
    } else {
      $this->json('Parametre non existant', self::HTTP_OK);
    }
  }

  private function controlsImage(): string
  {
    $maxSize = ini_get('upload_max_filesize');
    $maxSize = str_replace('M', '', $maxSize);
    $maxSize = $maxSize * 1024 * 1024;

    if (isset($_FILES['valeur'])) {
      $file = $_FILES['valeur'];

      if ($file['size'] > $maxSize || $_FILES['valeur']['error'] !== 0) {
        $this->json('Le fichier est trop volumineux. ' . str_replace('M', 'Mo', ini_get('upload_max_filesize')) . ' maximum.', self::HTTP_BAD_REQUEST);
      }
      $allowedTypes = ['image/jpeg', 'image/png', 'image/jpg'];
      if (!in_array($file['type'], $allowedTypes)) {
        $this->json('Type de fichier non autorisé.', self::HTTP_BAD_REQUEST);
      }
      $filename = basename($file['name']);
      if (move_uploaded_file($file['tmp_name'], __DIR__ . '/../../public/assets/images/' . $filename)) {
        return $filename;
      } else {
        $this->json('Le fichier n\'a pas pu être téléchargé.', self::HTTP_BAD_REQUEST);
      }
    } else {
      $this->json('Le fichier n\'a pas pu être téléchargé.', self::HTTP_BAD_REQUEST);
    }
  }
}
