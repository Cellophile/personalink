<?php

namespace Controllers;

use Services\Abstracts\AbstractController;

class AccueilController extends AbstractController
{
  public function accueil()
  {
    $this->render('accueil');
  }

}
