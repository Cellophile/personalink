<?php

namespace Controllers;

use DateTime;
use Models\User;
use Repositories\ParametreRepository;
use Services\Abstracts\AbstractController;
use Repositories\UserRepository;
use Services\AntiPishing;
use Services\Components\Collection;
use Services\EnvoiEmail;
use Services\Validator;

class InstallationController extends AbstractController
{
  public function install(): bool
  {
    if (!$this->isFirstAdminExists()) {
      if (
        isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] === '/installation'
        && isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST'
      ) {
        if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['ADMINEMAIL']) && isset($_POST['password']) && isset($_POST['confirm_password'])) {
          if ($_POST['password'] === $_POST['confirm_password']) {
            $data = [
              'email' => htmlentities($_POST['ADMINEMAIL'], ENT_QUOTES),
              'nom' => htmlentities($_POST['nom'], ENT_QUOTES),
              'prenom' => htmlentities($_POST['prenom'], ENT_QUOTES),
              'password' => password_hash(htmlentities($_POST['password'], ENT_QUOTES), PASSWORD_DEFAULT),
              'lenPwd' => strlen(htmlentities($_POST['password'], ENT_QUOTES)),
              'DateCreation' => (new DateTime())->format('d/m/Y - H:i'),
              'ip' => new Collection(AntiPishing::getIp()),
              'role' => 'admin'
            ];

            $user = new User($data);
            $user->setActivated(true);
            $UserRepo = new UserRepository();

            $errors = Validator::validate($user);
            if ($errors) {
              $this->render('installation', ['erreurs' => $errors, 'user' => $user]);
              exit;
            }
            $UserRepo->add($user);

            $ParametreRepo = new ParametreRepository();
            $parametres = [
              $ParametreRepo->findBy(['parametre' => 'TITRE']),
              $ParametreRepo->findBy(['parametre' => 'SOUSTITRE']),
              $ParametreRepo->findBy(['parametre' => 'ADMINEMAIL']),
              $ParametreRepo->findBy(['parametre' => 'URLSITE'])
            ];

            foreach ($parametres as $parametre) {
              if (isset($_POST[$parametre->getParametre()]) && !empty($_POST[$parametre->getParametre()])) {
                if ($parametre->getParametre() === 'URLSITE') {
                  if (filter_var($_POST[$parametre->getParametre()], FILTER_VALIDATE_URL) === false) {
                    $this->render('installation', ['erreur' => 'URL invalide.']);
                    exit;
                  }
                  substr($_POST[$parametre->getParametre()], -1) !== '/' ?? $_POST[$parametre->getParametre()] .= '/';
                  $urlsite = $_POST[$parametre->getParametre()];
                }
                if ($_POST[$parametre->getParametre()] === 'ADMINEMAIL') {
                  if (filter_var($_POST[$parametre->getParametre()], FILTER_VALIDATE_EMAIL) === false) {
                    $this->render('installation', ['erreur' => 'Email invalide.']);
                    exit;
                  }
                }
                $parametre->setValeur(htmlentities($_POST[$parametre->getParametre()], ENT_QUOTES));
              }
              $ParametreRepo->update($parametre);
            }
            $adminEmail = $parametres[2]->getValeur();
            $ENVOIEMAIL = $ParametreRepo->findBy(['parametre' => 'ENVOIEMAIL']);
            $ENVOIEMAIL->setValeur($adminEmail);
            $ParametreRepo->update($ENVOIEMAIL);
            $REPONSEEMAIL = $ParametreRepo->findBy(['parametre' => 'REPONSEEMAIL']);
            $REPONSEEMAIL->setValeur($adminEmail);
            $ParametreRepo->update($REPONSEEMAIL);
            $BLACKLISTEMAIL = $ParametreRepo->findBy(['parametre' => 'BLACKLISTEMAIL']);
            $BLACKLISTEMAIL->setValeur($adminEmail);
            $ParametreRepo->update($BLACKLISTEMAIL);

            $this->EmailConfirmationInstallation($user, $urlsite);
            $this->redirectToRoute('/?succes=7');
            exit;
          } else {
            $this->render('installation', ['erreur' => 'les mots de passes ne sont pas identiques.']);
            exit;
          }
        } else {
          $this->render('installation', ['erreur' => 'Veuillez remplir tous les champs.']);
          exit;
        }
      } else {
        $this->render('installation');
        exit;
      }
    } else {
      return true;
    }
  }

  private function isFirstAdminExists(): bool
  {
    $UserRepo = new UserRepository();
    $users = $UserRepo->findAll();
    return count($users) > 0;
  }

  private function EmailConfirmationInstallation(User $user, $urlsite): void
  {
    $headers = [
      "Content-Type" => "text/html; charset=UTF-8",
      "From" => $urlsite . " <" . $user->getEmail() . ">",
      "Reply-To" => $urlsite . " <" . $user->getEmail() . ">"
    ];

    EnvoiEmail::send(
      "Bonjour " . $user->getNom() . " " . $user->getPrenom() . ",<br/><br/>" . "L'installation du site $urlsite est terminée ! <br/> Vous pouvez vous connecter sur <a href='$urlsite'>$urlsite</a>, et explorer votre espace administrateur.<br/>",
      "Installation terminée !",
      $user->getEmail(),
      $headers
    );
  }
}
