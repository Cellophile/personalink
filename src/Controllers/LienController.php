<?php

namespace Controllers;

use DateTime;
use Models\Lien;
use Services\AntiPishing;
use Repositories\LienRepository;
use Services\Abstracts\AbstractController;

class LienController extends AbstractController
{
	private LienRepository $LienRepo;

	public function __construct()
	{
		$this->LienRepo = new LienRepository;
	}

	public function creationLien()
	{
		if (
			$this->isCaptchaValid()
			&& isset($_POST["ubase"]) && isset($_POST["uperso"])
			&& AntiPishing::antipishing()
			&& $this->isUrlValid($_POST["ubase"])
		) {
			$lien = new Lien;
			$lien->setUpersok($this->sanitize($_POST["uperso"]));
			$lien->setUrlbase($_POST["ubase"]);
			!empty($_POST["mdp"]) ? $lien->setMdp($_POST["mdp"]) : null;
			$lien->setDcreation(new DateTime('now'));

			if (!empty($_POST["datexpi"])) {
				$lien->setDatexpi(new DateTime($_POST["datexpi"]));
				if ($lien->getDatexpi() <= new DateTime('now')) {
					$this->redirectToRoute(HOME_URL . "?echec=3", self::HTTP_UNPROCESSABLE_ENTITY);
				}
			}
			if (isset($_SESSION['auth']) && $_SESSION['auth'] == 'valide') {
				$user = $this->getUser();
				$lien->setUser($user);
				$user->setLiens($lien);
				if (in_array($user->getForfait(), ['basique', 'premium'])) {
					$lien->setUpersok($this->sanitize($_POST["uperso"], 0));
				}
				SecurityController::updateSession(['user' => $user]);
			}

			$lien->setIp(AntiPishing::getIp());
			$this->LienRepo->add($lien);

			$urlfinale = URLSITE . $lien->getUpersok();
			if ($urlfinale) {
				$this->render(view: 'accueil', data: ['urlfinale' => $urlfinale, 'reponse' => 1, 'lien' => $lien]);
			} else {
				$this->redirectToRoute(HOME_URL . "?echec=5", self::HTTP_INTERNAL_SERVER_ERROR);
			}
		}
	}

	private function isCaptchaValid(): bool
	{
		if (!isset($_POST['captcha']) || $_POST['captcha'] != $_SESSION['captcha']) {
			$this->redirectToRoute(HOME_URL . "?echec=2", self::HTTP_FORBIDDEN);
		}
		return true;
	}

	public function sanitize(string $valeur = '', int $randomCars = 3)
	{
		$o = ['\'', '?', '!', '~', '{', '[', '(', '|', '#', '@', '=', ';', ':', '"', '<', '>', ','];
		$valeur = str_replace($o, '', $valeur);
		$valeur = ucfirst(mb_strtolower($valeur, 'UTF-8'));
		$valeur = str_replace(' ', '-', $valeur);

		$liens = $this->LienRepo->findAll();
		if ($liens) {
			$upersokVerifie = false;
			$i = 0;
			while (!$upersokVerifie) {
				if ($i > 0 && $randomCars == 0) {
					$randomCars = 'premium_deja_existant';
				}
				$av = ($valeur == '') ? $this->random($randomCars) : (($randomCars == 0) ? $valeur : (($randomCars == 'premium_deja_existant') ? $valeur . "-" . $this->random(3) : $this->random($randomCars) . "-" . $valeur));


				foreach ($liens as $lien) {
					if ($lien->getUpersok() == $av) {
						$upersokVerifie = false;
						break;
					} else {
						$upersokVerifie = $av;
					}
				}
				$i++;
			}
			return $upersokVerifie;
		}
		return $this->random(3) . "-" . $valeur;
	}

	public function random($car1)
	{
		$string1 = "";
		$chaine1 = "0123456789abcdefghijklmnopkrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for ($i = 0; $i < $car1; $i++) {
			$string1 .= $chaine1[rand() % strlen($chaine1)];
		}
		return $string1;
	}

	private function isUrlValid(string $urlbase): bool
	{
		// Vérification que l'URL n'est pas blacklistée :
		$blackoupas = AntiPishing::isUrlblacklist($urlbase);

		// Vérification que l'url ne contient pas de data:application/pdf;base64, sinon ça prend vraiment trop de place dans le fichier.
		$data_app = stripos($urlbase, "data:application");

		if ($data_app !== false) {
			$this->redirectToRoute(HOME_URL . '?echec=4', self::HTTP_UNPROCESSABLE_ENTITY);
		}

		// Vérification que l'URL emmène bien quelque part :
		$urlbasetest = @get_headers($urlbase);
		if ($urlbasetest === false || strpos($urlbasetest[0], '404') !== false) {
			// URL inaccessible ou renvoyant une erreur 404
			$this->redirectToRoute(HOME_URL . '?echec=1', self::HTTP_UNPROCESSABLE_ENTITY);
		}

		return true;
	}


	public function lireLien(string $lien): void
	{
		$lien = ltrim($lien, HOME_URL);
		$lien = $this->LienRepo->findBy(['upersok' => $lien]);

		if ($lien) {
			if ($lien->getMdp()) {
				$erreurpassword = 0;
				if (isset($_POST['mdpaverifier'])) {
					if ($_POST["mdpaverifier"] == $lien->getMdp()) {
						self::executerRedirection($lien);
					} else {
						$erreurpassword = 1;
					}
				}
				self::render(view: 'password', data: ['erreurpassword' => $erreurpassword]);
				exit();
			}

			if ($lien->getDatexpi() && $lien->getDatexpi() < new DateTime('now')) {
				$this->LienRepo->delete($lien);
				self::erreur404();
			}
			self::executerRedirection($lien);
		} else {
			self::erreur404();
		}
	}

	private function executerRedirection(Lien $lien): void
	{
		$lien->setNbClics($lien->getNbClics() + 1);
		$this->LienRepo->update($lien);
		self::redirection($lien->getUrlbase());
	}

	public function apiExterne(array $data, string $method, string $domaine): void
	{
		$type = isset($data['type']) ? $data['type'] : 'json';
		if (API_ACTIVATED) {
			if (in_array($method, API_METHOD)) {
				if (API_ACCESS === '*') {
					if (is_null(API_KEY) || (is_array(API_KEY) && in_array($data['key'], API_KEY)) || (is_string(API_KEY) && $data['key'] == API_KEY)) {
						$this->apiTraitement($data);
					} else {
						$this->$type([
							'message' => 'Clé d\'api invalide',
							'key' => $data['key'],
							"api_key" => API_KEY
						], self::HTTP_FORBIDDEN);
						exit();
					}
				} else if (in_array($domaine, API_ACCESS)) {
					$this->apiTraitement($data);
				} else {
					$this->$type(['message' => 'Domaine d\'api invalide'], self::HTTP_FORBIDDEN);
					exit();
				}
			} else {
				$this->$type(['message' => 'Méthode non-authorisée'], self::HTTP_FORBIDDEN);
				exit();
			}
		} else {
			$this->$type(['message' => 'api inactive'], self::HTTP_FORBIDDEN);
			exit();
		}
	}

	private function apiTraitement(array $data): void
	{

		if (
			isset($data['url'])
			&& !empty($data['url'])
			&& isset($data['action'])
			&& $data['action'] === 'raccourcir'
			&& isset($data['format'])
			&& array_key_exists($data['format'], parent::CONTENT_TYPES)
		) {
			$type = $data['format'];

			$lien = new Lien;
			$lien->setUpersok($this->sanitize('', 8));
			$lien->setUrlbase($data['url']);
			!empty($data["mdp"]) ? $lien->setMdp($data["mdp"]) : null;
			$lien->setDcreation(new DateTime('now'));

			if (!empty($data["datexpi"])) {
				$lien->setDatexpi(new DateTime($data["datexpi"]));
				if ($lien->getDatexpi() <= new DateTime('now')) {
					$this->$type(['message' => "La date d'expiration ne peut pas être antérieure à aujourd'hui."], self::HTTP_UNPROCESSABLE_ENTITY);
					exit();
				}
			}

			$lien->setIp(AntiPishing::getIp());
			$this->LienRepo->add($lien);

			$this->$type(
				[
					"message" => "l'url a été raccourcie avec succès. Voici la nouvelle url : " . URLSITE . $lien->getUpersok() . ".",
					"url" => URLSITE . $lien->getUpersok()
				],
				self::HTTP_CREATED
			);
			exit();
		} else {
			$this->json(['message' => 'Requête mal formulée'], self::HTTP_UNPROCESSABLE_ENTITY);
			exit();
		}
	}
}
