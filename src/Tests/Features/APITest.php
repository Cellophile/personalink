<?php

declare(strict_types=1);

namespace Tests\Features;

use PHPUnit\Framework\TestCase;

final class APITest extends TestCase
{
  public function testApiFonctionnelle(): void
  {
    $urlApi = "http://personalink/api?action=raccourcir&key=bsdq45cnkfsdvdsakop563h89azpotgzjh&format=json&mdp=VOTRE_MDP&datexpi=2025-01-01%2000:00&url=https://www.php.net/manual/en/function.ltrim.php";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlApi);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $retour = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($retour, true);

    $this->assertTrue(isset($data['message']), "il n'y a pas de message.");
    $this->assertTrue(isset($data['url']), "il n'y a pas d'url.");

    $patternAttendu = '/^http:\/\/personalink\/[A-Za-z0-9]{8}$/';

    $this->assertMatchesRegularExpression($patternAttendu, $data['url'], "Le lien ne correspond pas au pattern attendu.");
  }

  public function testApiDateExpirationIncorrecteReconnue(): void
  {
    $urlApi = "http://personalink/api?action=raccourcir&key=bsdq45cnkfsdvdsakop563h89azpotgzjh&format=json&mdp=VOTRE_MDP&datexpi=2020-01-01%2000:00&url=https://www.php.net/manual/en/function.ltrim.php";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $urlApi);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $retour = curl_exec($ch);
      curl_close($ch);
      $data = json_decode($retour, true);

      $this->assertTrue(isset($data['message']), "il n'y a pas de message.");
      $this->assertTrue($data['message'] === "La date d'expiration ne peut pas être antérieure à aujourd'hui.", "Le message n'est pas bon.");
  }
}
