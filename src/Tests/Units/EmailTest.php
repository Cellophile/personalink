<?php
declare(strict_types=1);

namespace Tests\Units;

use PHPUnit\Framework\TestCase;
use Services\EnvoiEmail;

final class EmailTest extends TestCase
{
  public function testLaFonctionSendMarche(): void
  {
    require __DIR__ . '/../../../config.php';
    $retourEnvoi = EnvoiEmail::send("test", "test", "theophile@captp.fr");
    $this->assertTrue($retourEnvoi, "le mail n'a pas été envoyé. Avez-vous pensé à allumer maildev ?");
  }
}