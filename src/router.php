<?php

use Controllers\AccueilController;
use Controllers\AdminController;
use Controllers\DashboardController;
use Controllers\LienController;
use Controllers\SecurityController;
use Services\Components\captcha\Captcha;
use Services\Routing;

$AccueilController = new AccueilController;
$SecurityController = new SecurityController;
$LienController = new LienController;
$DashboardController = new DashboardController;
$AdminController = new AdminController;

$route = $_SERVER['REDIRECT_URL'];
$methode = $_SERVER['REQUEST_METHOD'];
$routeComposee = Routing::routeComposee($route);

switch ($route) {
  case HOME_URL:
    $AccueilController->accueil();
    break;

  case $route == HOME_URL . 'connexion' && $methode == 'POST':
    $SecurityController->connexion();
    break;

  case $route == HOME_URL . 'deconnexion':
    $SecurityController->deconnexion();
    break;

  case $route == HOME_URL . 'inscription' && $methode == 'POST':
    $SecurityController->inscription();
    break;

  case HOME_URL . 'activation':
    $SecurityController->activation();
    break;

  case HOME_URL . 'oubli-mdp':
    $SecurityController->oubliMotDePasse();
    break;

  case HOME_URL . 'reinitialisation-mdp':
    $SecurityController->reinitialisationMotDePasse();
    break;

  case $route == HOME_URL . 'creation-lien' && $methode == 'POST':
    $LienController->creationLien();
    break;

  case $routeComposee[0] == 'dashboard':
    if (isset($_SESSION['auth']) && $_SESSION['auth'] == 'valide') {
      switch (true) {
        case $routeComposee[1] == null ||
          $routeComposee[1] == 'mes-liens' ||
          $routeComposee[1] == 'mon-compte' ||
          $routeComposee[1] == 'confidentialite':

          $DashboardController->accueil();
          break;

        case $routeComposee[1] == 'webapi':
          switch (true) {
            case $routeComposee[2] == 'user':
              if ($routeComposee[3] == 'update' && $methode == "POST") {
                $DashboardController->updateUser();
              } elseif ($routeComposee[3] == 'delete' && $methode == "POST") {
                $DashboardController->deleteUser();
              } else {
                $DashboardController->erreurJson404();
              }
              break;

            case $routeComposee[2] == 'lien':
              if ($routeComposee[3] == 'update' && $methode == "POST") {
                $DashboardController->updateLien();
              } elseif ($routeComposee[3] == 'delete' && $methode == "POST") {
                $DashboardController->deleteLien();
              } else {
                $DashboardController->erreurJson404();
              }
              break;

            default:
              $DashboardController->erreurJson404();
              break;
          }
          break;
        default:
          $DashboardController->accueil();
          break;
      }
    } else {
      header('location: ' . HOME_URL . '?echec=300');
      exit();
    }

    break;

  case $routeComposee[0] == 'admin':
    if (isset($_SESSION['auth']) && $_SESSION['auth'] == 'valide') {
      switch (true) {
        case $routeComposee[1] == null ||
          $routeComposee[1] == 'liens' ||
          $routeComposee[1] == 'statistiques' ||
          $routeComposee[1] == 'utilisateurs' ||
          $routeComposee[1] == 'blacklists' ||
          $routeComposee[1] == 'parametres'
          :

          $AdminController->accueil();
          break;

        case $routeComposee[1] == 'webapi':
          switch (true) {
            case $routeComposee[2] == 'user':
              if ($routeComposee[3] == 'desactivation' && $methode == "POST") {
                $AdminController->desactivationUser();
              } else if ($routeComposee[3] == 'activation' && $methode == "POST") {
                $AdminController->activationUser();
              } else if ($routeComposee[3] == 'blacklist' && $methode == "POST") {
                $AdminController->blacklistUser();
              } else if ($routeComposee[3] == 'deblacklist' && $methode == "POST") {
                $AdminController->deblacklistUser();
              } else if ($routeComposee[3] == 'delete' && $methode == "POST") {
                $AdminController->deleteUser();
              } else if ($routeComposee[3] == 'blacklists' && $methode == "POST") {
                
              } else {
                $AdminController->erreurJson404();
              }
              break;

            case $routeComposee[2] == 'lien':
              if ($routeComposee[3] == 'update' && $methode == "POST") {
                $DashboardController->updateLien();
              } elseif ($routeComposee[3] == 'delete' && $methode == "POST") {
                $DashboardController->deleteLien();
              } else {
                $AdminController->erreurJson404();
              }
              break;

            case $routeComposee[2] == 'blacklists':
              switch (true) {
                case $routeComposee[3] == 'addip':
                  $AdminController->blacklistIp();
                  break;
                case $routeComposee[3] == 'addurl':
                  $AdminController->blacklistUrl();
                  break;
                case $routeComposee[3] == 'removeip':
                  $AdminController->deblacklistIp();
                  break;
                case $routeComposee[3] == 'removeurl':
                  $AdminController->deblacklistUrl();
                  break;
                case $routeComposee[3] == 'validateip':
                  $AdminController->VerificationIp();
                  break;
                default:
                  $AdminController->erreurJson404();
                  break;
              }
              break;

            case $routeComposee[2] == 'parametres':
              if ($routeComposee[3] == 'update' && $methode == "POST") {
                $AdminController->updateParametre();
              } else {
                $AdminController->erreurJson404();
              }
              break;

            default:
              $AdminController->erreurJson404();
              break;
          }
      }
    } else {
      header('location: ' . HOME_URL . '?echec=300');
      exit();
    }
    break;

  case $route == HOME_URL . 'api' && ($methode == 'POST' || $methode == 'GET'):
    $data = empty($_POST) ? $_GET : $_POST;
    $LienController->apiExterne($data, $methode, $_SERVER['SERVER_NAME']);
    break;

  case '/captcha':
    new Captcha;
    break;

  default:
    $LienController->lireLien($route);
    break;
}
