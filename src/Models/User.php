<?php

namespace Models;

use DateTime;
use Repositories\IpBlacklisteeRepository;
use Repositories\LienRepository;
use Services\Abstracts\AbstractModel;
use Services\Components\Collection;
use Services\Components\Constraints as Constraint;
use Services\Functions;

class User extends AbstractModel
{
	private ?int      	 $id = null;

	#[Constraint\NotBlank(message: 'L\'email ne doit pas être vide.')]
	#[Constraint\Email(message: 'L\'email %s n\'est pas valide.')]
	#[Constraint\UniqueEntity(message: 'L\'email %s est déjà utilisé. Veuillez vous connecter.')]
	private ?string      $email = null;

	#[Constraint\NotBlank(message: 'Le nom ne doit pas être vide.')]
	#[Constraint\NotContains(containedString: ',', message: 'Le nom contient un caractère interdit: la virgule.')]
	private ?string      $nom = null;

	#[Constraint\NotBlank(message: 'Le prenom ne doit pas être vide.')]
	#[Constraint\NotContains(containedString: ',', message: 'Le prénom contient un caractère interdit : la virgule.')]
	private ?string   	 $prenom = null;

	#[Constraint\NotBlank(message: 'Le mot de passe ne doit pas être vide.')]
	#[Constraint\Length(minLength: 8, minMessage: 'Le mot de passe doit contenir au moins 8 caractères.')]
	private ?string   	 $password = null;
	private ?int         $lenPwd = null;
	private ?DateTime    $DateCreation = null;
	private ?DateTime    $DateDernierLogin = null;
	private ?int         $ErreursAuthentification = 0;
	private ?bool        $activated = false;
	private ?string      $role = null;
	private ?string      $forfait = null;
	private ?Collection  $ip = null;
	private ?Collection  $liens = null;

	public const FORFAITS = ['gratuit', 'basique', 'premium'];
	public const FORFAIT_DEFAULT = 'gratuit';
	public const ROLES = ['admin', 'user'];
	public const ROLE_DEFAULT = 'user';

	public function convert(array $data = []): array
	{
		return [
			'id' => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
			'email' => isset($data[1]) ? $data[1] : (isset($data['email']) ? $data['email'] : null),
			'nom' => isset($data[2]) ? $data[2] : (isset($data['nom']) ? $data['nom'] : null),
			'prenom' => isset($data[3]) ? $data[3] : (isset($data['prenom']) ? $data['prenom'] : null),
			'password' => isset($data[4]) ? $data[4] : (isset($data['password']) ? $data['password'] : null),
			'lenPwd' => isset($data[5]) ? $data[5] : (isset($data['lenPwd']) ? $data['lenPwd'] : null),
			'dateCreation' => isset($data[6]) && !empty($data[6]) ? DateTime::createFromFormat('d/m/Y - H:i', $data[6]) : (isset($data['dateCreation']) ? DateTime::createFromFormat('d/m/Y - H:i', $data['dateCreation']) : new DateTime('now')),
			'dateDernierLogin' => isset($data[7]) && !empty($data[7]) ? DateTime::createFromFormat('d/m/Y - H:i', $data[7]) : (isset($data['dateDernierLogin']) ? DateTime::createFromFormat('d/m/Y - H:i', $data['dateDernierLogin']) : null),
			'erreursAuthentification' => isset($data[8]) ? $data[8] : (isset($data['erreursAuthentification']) ? $data['erreursAuthentification'] : 0),
			'activated' => isset($data[9]) ? (bool)$data[9] : (isset($data['activated']) ? (bool)$data['activated'] : false),
			'role' => isset($data[10]) ? $data[10] : (isset($data['role']) ? $data['role'] : self::ROLE_DEFAULT),
			'forfait' => isset($data[11]) ? $data[11] : (isset($data['forfait']) ? $data['forfait'] : self::FORFAIT_DEFAULT),
			'ip' => isset($data['ip']) ? $data['ip'] : null,
			'liens' => isset($data['Liens']) ? $data['Liens'] : null,

		];
	}

	/**
	 * Get the value of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @param   int  $id  
	 *
	 * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the value of email
	 *
	 * @return  string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @param   string  $email  
	 *
	 * @return void
	 */
	public function setEmail(?string $email): void
	{
		$this->email = $email;
	}

	/**
	 * Get the value of nom
	 *
	 * @return  string
	 */
	public function getNom(): string
	{
		return $this->nom;
	}

	/**
	 * Set the value of nom
	 *
	 * @param   string  $nom  
	 *
	 * @return void
	 */
	public function setNom(?string $nom): void
	{
		$this->nom = $nom;
	}

	/**
	 * Get the value of prenom
	 *
	 * @return  string
	 */
	public function getPrenom(): string
	{
		return $this->prenom;
	}

	/**
	 * Set the value of prenom
	 *
	 * @param   string  $prenom  
	 *
	 * @return void
	 */
	public function setPrenom(?string $prenom): void
	{
		$this->prenom = $prenom;
	}

	/**
	 * Get the value of password
	 *
	 * @return  string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * Set the value of password
	 *
	 * @param   string  $password  
	 *
	 * @return void
	 */
	public function setPassword(?string $password): void
	{
		$this->password = $password;
	}

	/**
	 * Get the value of lenPwd
	 *
	 * @return  int
	 */
	public function getLenPwd(): int
	{
		return $this->lenPwd;
	}

	/**
	 * Set the value of lenPwd
	 *
	 * @param   int  $lenPwd  
	 *
	 * @return void
	 */
	public function setLenPwd(?int $lenPwd): void
	{
		$this->lenPwd = $lenPwd;
	}

	/**
	 * Get the value of DateCreation
	 *
	 * @return  DateTime
	 */
	public function getDateCreation(): DateTime
	{
		return $this->DateCreation;
	}

	/**
	 * Set the value of DateCreation
	 *
	 * @param   DateTime  $DateCreation  
	 *
	 * @return void
	 */
	public function setDateCreation(?DateTime $DateCreation): void
	{
		$this->DateCreation = $DateCreation;
	}

	/**
	 * Get the value of DateDernierLogin
	 *
	 * @return  ?DateTime
	 */
	public function getDateDernierLogin(): ?DateTime
	{
		return $this->DateDernierLogin;
	}

	/**
	 * Set the value of DateDernierLogin
	 *
	 * @param   DateTime  $DateDernierLogin  
	 *
	 * @return void
	 */
	public function setDateDernierLogin(?DateTime $DateDernierLogin): void
	{
		$this->DateDernierLogin = $DateDernierLogin;
	}

	/**
	 * Get the value of ErreursAuthentification
	 *
	 * @return  int
	 */
	public function getErreursAuthentification(): int
	{
		return $this->ErreursAuthentification;
	}

	/**
	 * Set the value of ErreursAuthentification
	 *
	 * @param   int  $ErreursAuthentification  
	 *
	 * @return void
	 */
	public function setErreursAuthentification(?int $ErreursAuthentification): void
	{
		$this->ErreursAuthentification = $ErreursAuthentification;
	}

	/**
	 * Get the value of activated
	 *
	 * @return  bool
	 */
	public function isActivated(): bool
	{
		return $this->activated;
	}

	/**
	 * Set the value of activated
	 *
	 * @param   bool  $activated  
	 *
	 * @return void
	 */
	public function setActivated(?bool $activated): void
	{
		$this->activated = $activated;
	}

	/**
	 * Get the value of ip
	 *
	 * @return  ?Collection
	 */
	public function getIp(): ?Collection
	{
		return $this->ip;
	}

	/**
	 * Set the value of ip
	 *
	 * @param   ?Collection  $ip  
	 *
	 * @return void
	 */
	public function setIp(?Collection $ip): void
	{
		$this->ip = $ip;
	}

	public function addIp(Ip $ip): void
	{
		if ($this->ip instanceof Collection) {
			$this->ip->add($ip);
		} else {
			$this->ip = new Collection([$ip]);
		}
	}
	/**
	 * Get the value of liens
	 *
	 * @return  ?Collection
	 */
	public function getLiens(): ?Collection
	{
		$LienRepo = new LienRepository();
		$this->liens = new Collection($LienRepo->findBy(['user' => $this->getId()]));
		return $this->liens;
	}

	/**
	 * Set the value of liens
	 *
	 * @param   null|Collection|Lien|array  $liens  
	 *
	 * @return void
	 */
	public function setLiens(null|Collection|Lien|array $liens = null): void
	{
		if ($liens !== null && $this->liens == null) {
			$this->liens = new Collection();
		}
		if (is_array($liens)) {
			foreach ($liens as $lien) {
				$this->liens->add($lien);
			}
		} else if ($liens instanceof Lien) {
			$this->liens->add($liens);
		} else if ($liens instanceof Collection) {
			$this->liens = $liens;
		}
	}

	/**
	 * Get the value of role
	 *
	 * @return  ?string
	 */
	public function getRole(): ?string
	{
		return $this->role;
	}

	/**
	 * Set the value of role
	 *
	 * @param   ?string  $role  
	 *
	 * @return void
	 */
	public function setRole(?string $role): void
	{
		if (in_array($role, self::ROLES)) {
			$this->role = $role;
		} else {
			$this->role = self::ROLE_DEFAULT;
		}
	}

	public function isAdmin(): bool
	{
		return $this->role === 'admin';
	}

	public function isBlacklisted(): bool
	{
		$IpBlacklisteeRepo = new IpBlacklisteeRepository();
		$IpsBlacklistees = $IpBlacklisteeRepo->findAll();
		$UserIps = $this->getIp()->all();
		foreach ($UserIps as $ip) {
			if (Functions::ObjectInArray($ip, $IpsBlacklistees, 'ip')) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the value of forfait
	 *
	 * @return  string
	 */
	public function getForfait(): string
	{
		return $this->forfait;
	}

	/**
	 * Set the value of forfait
	 *
	 * @param   string  $forfait  
	 *
	 * @return void
	 */
	public function setForfait(string $forfait): void
	{
		if (in_array($forfait, self::FORFAITS)) {
			$this->forfait = $forfait;
		} else {
			$this->forfait = self::FORFAIT_DEFAULT;
		}
	}
}
