<?php
namespace Models;

use Services\Abstracts\AbstractModel;
use Services\Components\Constraints as Constraint;

class Parametre extends AbstractModel
{

  #[Constraint\UniqueEntity("l'id  %d est déjà enregistré")]
  private ?int $id;

  #[Constraint\UniqueEntity("Le paramètre %s est déjà enregistré")]
  #[Constraint\NotBlank("L'intitulé du paramètre est obligatoire")]
  private ?string $parametre;

  #[Constraint\NotBlank("La valeur du paramètre est obligatoire")]
  private mixed $valeur;

  public function convert(array $data = []): array
  {
    return [
      'id' => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
      'parametre' => isset($data[1]) ? $data[1] : (isset($data['parametre']) ? $data['parametre'] : null),
      'valeur' => isset($data[2]) ? $data[2] : (isset($data['valeur']) ? $data['valeur'] : null),
    ];
  }

	/**
	 * Get the valeur of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
  
	/**
	 * Set the valeur of id
	 *
	 * @param   ?int  $id  
	 *
   * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the valeur of parametre
	 *
	 * @return  ?string
	 */
	public function getParametre(): ?string
	{
		return $this->parametre;
	}
  
	/**
	 * Set the valeur of parametre
	 *
	 * @param   ?string  $parametre  
	 *
   * @return void
	 */
	public function setParametre(?string $parametre): void
	{
		$this->parametre = $parametre;
	}

	/**
	 * Get the valeur of valeur
	 *
	 * @return  mixed
	 */
	public function getValeur(): mixed
	{
		return $this->valeur;
	}
  
	/**
	 * Set the valeur of valeur
	 *
	 * @param   mixed  $valeur  
	 *
   * @return void
	 */
	public function setValeur(mixed $valeur): void
	{
		$this->valeur = $valeur;
	}
}