<?php
namespace Models;

use Services\Abstracts\AbstractModel;
use Services\Components\Collection;
use Services\Components\Constraints as Constraint;

class Ip extends AbstractModel
{
	#[Constraint\UniqueEntity(message: "l'id  %d est déjà enregistré")]
  private ?int $id = null;

	#[Constraint\Ip(message: "l'ip %s n'est pas une ip valide.")]
	#[Constraint\UniqueEntity(message: "l'ip %s est déjà enregistrée")]
	#[Constraint\NotBlank(message: "l'ip ne peut pas être vide")]
  private ?string $ip = null;

  private ?Collection  $user = null;
	private ?Collection  $lien = null;

  public function convert(array $data = []): array
  {
    return [
      'id' => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
      'ip' => isset($data[1]) ? $data[1] : (isset($data['ip']) ? $data['ip'] : null),
      'user' => isset($data['user']) ? $data['user'] : new Collection,
			'liens' => isset($data['liens']) ? $data['liens'] : new Collection
    ];
  }
	

	/**
	 * Get the value of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
  
	/**
	 * Set the value of id
	 *
	 * @param   int  $id  
	 *
   * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the value of ip
	 *
	 * @return  string
	 */
	public function getIp(): string
	{
		return $this->ip;
	}
  
	/**
	 * Set the value of ip
	 *
	 * @param   string  $ip  
	 *
   * @return void
	 */
	public function setIp(?string $ip): void
	{
		$this->ip = $ip;
	}

	/**
	 * Get the value of user
	 *
	 * @return  Collection
	 */
	public function getUser(): Collection
	{
		return $this->user;
	}
  
	/**
	 * Set the value of user
	 *
	 * @param   Collection  $user  
	 *
   * @return void
	 */
	public function setUser(?Collection $user): void
	{
		$this->user = $user;
	}

	/**
	 * Get the value of lien
	 *
	 * @return  ?Collection
	 */
	public function getLiens(): ?Collection
	{
		return $this->lien;
	}
  
	/**
	 * Set the value of lien
	 *
	 * @param   ?Collection  $lien  
	 *
   * @return void
	 */
	public function setLien(?Collection $lien): void
	{
		$this->lien = $lien;
	}

	/**
	 * Fonction qui permet de transformer une Ip en IpBlacklistee
	 * On ne passe que l'ip, afin de ne pas faire de conflit d'id.
	 *
	 * @return IpBlacklistee
	 */
	public function blacklist(): IpBlacklistee
	{
		return new IpBlacklistee([1 => $this->ip]);
	}
}