<?php
namespace Models;

use DateTime;
use Repositories\IpRepository;
use Repositories\UserRepository;
use Services\Abstracts\AbstractModel;
use Services\Components\Constraints as Constraint;

class Lien extends AbstractModel
{
  private ?int      $id = null;

	#[Constraint\NotBlank(message: 'Le lien personnalisé est obligatoire.')]
	#[Constraint\UniqueEntity(message: 'Ce lien personnalisé existe déjà.')]
  private ?string   $upersok = null;

	#[Constraint\NotBlank(message: 'L\'url à raccourcir est obligatoire.')]
	#[Constraint\Url(message: 'L\'url %s n\'est pas valide.')]
  private ?string   $urlbase = null;

  private ?string   $mdp = null;

  private ?DateTime $dcreation = null;

	#[Constraint\IsDateTime(message: 'La date de validité du lien %s n\'est pas valide.')]
  private ?DateTime $datexpi = null;

  private ?User     $user = null;
  
	private ?Ip       $ip = null;
	
	private ?int      $nbClics = 0;

	public function convert(array $data = []): array
	{
		$userRepo = new UserRepository;
		$ipRepo = new IpRepository;
		return [
			'id' => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
			'upersok' => isset($data[1]) ? $data[1] : (isset($data['upersok']) ? $data['upersok'] : null),
			'urlbase' => isset($data[2]) ? $data[2] : (isset($data['urlbase']) ? $data['urlbase'] : null),
			'mdp' => isset($data[3]) ? $data[3] : (isset($data['mdp']) ? $data['mdp'] : null),
			'dcreation' => isset($data[4]) && !empty($data[4]) ? DateTime::createFromFormat('d/m/Y - H:i', $data[4]) : (isset($data['dcreation']) ? DateTime::createFromFormat('d/m/Y - H:i', $data['dcreation']) : null),
			'datexpi' => isset($data[5]) && !empty($data[5]) ? DateTime::createFromFormat('d/m/Y - H:i', $data[5]) : (isset($data['datexpi']) ? DateTime::createFromFormat('d/m/Y - H:i', $data['datexpi']) : null),
			'user' => isset($data[6]) && !empty($data[6]) ? $userRepo->find((int) $data[6]) : (isset($data['user']) ? $userRepo->find((int) $data['user']) : null),
			'ip' => isset($data[7]) && !empty($data[7]) ? $ipRepo->find($data[7]) : (isset($data['ip']) ? $ipRepo->find($data['ip']) : null),
			'nbClics' => isset($data[8]) ? $data[8] : (isset($data['nbClics']) ? $data['nbClics'] : 0),
		];
	}


	/**
	 * Get the value of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
  
	/**
	 * Set the value of id
	 *
	 * @param   ?int  $id  
	 *
   * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the value of upersok
	 *
	 * @return  string
	 */
	public function getUpersok(): string
	{
		return $this->upersok;
	}
  
	/**
	 * Set the value of upersok
	 *
	 * @param   string  $upersok  
	 *
   * @return void
	 */
	public function setUpersok(?string $upersok): void
	{
		$this->upersok = $upersok;
	}

	/**
	 * Get the value of urlbase
	 *
	 * @return  string
	 */
	public function getUrlbase(): string
	{
		return $this->urlbase;
	}
  
	/**
	 * Set the value of urlbase
	 *
	 * @param   string  $urlbase  
	 *
   * @return void
	 */
	public function setUrlbase(?string $urlbase): void
	{
		$this->urlbase = $urlbase;
	}

	/**
	 * Get the value of user
	 *
	 * @return  User
	 */
	public function getUser(): User|null
	{
		return $this->user;
	}
  
	/**
	 * Set the value of user
	 *
	 * @param   User  $user  
	 *
   * @return void
	 */
	public function setUser(?User $user): void
	{
		$this->user = $user;
	}

	/**
	 * Get the value of mdp
	 *
	 * @return  string
	 */
	public function getMdp(): string|null
	{
		return $this->mdp;
	}
  
	/**
	 * Set the value of mdp
	 *
	 * @param   string  $mdp  
	 *
   * @return void
	 */
	public function setMdp(?string $mdp): void
	{
		$this->mdp = $mdp;
	}

	/**
	 * Get the value of dcreation
	 *
	 * @return  DateTime
	 */
	public function getDcreation(): DateTime
	{
		return $this->dcreation;
	}
  
	/**
	 * Set the value of dcreation
	 *
	 * @param   DateTime  $dcreation  
	 *
   * @return void
	 */
	public function setDcreation(?DateTime $dcreation): void
	{
		$this->dcreation = $dcreation;
	}

	/**
	 * Get the value of datexpi
	 *
	 * @return  DateTime
	 */
	public function getDatexpi(): DateTime|null
	{
		return $this->datexpi;
	}
  
	/**
	 * Set the value of datexpi
	 *
	 * @param   DateTime  $datexpi  
	 *
   * @return void
	 */
	public function setDatexpi(?DateTime $datexpi): void
	{
		$this->datexpi = $datexpi;
	}

	/**
	 * Get the value of ip
	 *
	 * @return  Ip
	 */
	public function getIp(): Ip
	{
		return $this->ip;
	}
  
	/**
	 * Set the value of ip
	 *
	 * @param   Ip  $ip  
	 *
   * @return void
	 */
	public function setIp(?Ip $ip): void
	{
		$this->ip = $ip;
	}

	/**
	 * Get the value of nbClics
	 *
	 * @return  int
	 */
	public function getNbClics(): int
	{
		return $this->nbClics;
	}
  
	/**
	 * Set the value of nbClics
	 *
	 * @param   int  $nbClics  
	 *
   * @return void
	 */
	public function setNbClics(?int $nbClics): void
	{
		$this->nbClics = $nbClics;
	}
}