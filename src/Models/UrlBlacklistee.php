<?php
namespace Models;

use Services\Abstracts\AbstractModel;
use Services\Components\Constraints as Constraint;

class urlblacklistee extends AbstractModel
{
  private ?int $id = null;

	#[Constraint\NotBlank(message: 'l\'URL ne peut pas être vide.')]
	#[Constraint\Url(message: 'l\'URL %s n\'est pas valide.')]
  private ?string $url = null;
  
	public function convert(array $data = []): array
	{
		return [
			'id' => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
			'url' => isset($data[1]) ? $data[1] : (isset($data['url']) ? $data['url'] : null),
		];
	}


	/**
	 * Get the value of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
  
	/**
	 * Set the value of id
	 *
	 * @param   ?int  $id  
	 *
   * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the value of url
	 *
	 * @return  string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}
  
	/**
	 * Set the value of url
	 *
	 * @param   string  $url  
	 *
   * @return void
	 */
	public function setUrl(?string $url): void
	{
		$this->url = $url;
	}
}