<?php
namespace Models;

use DateTime;
use Services\Abstracts\AbstractModel;
use Services\Components\Constraints as Constraint;

class IpBlacklistee extends AbstractModel
{
  public const ETAT_A_VERIFIER = "À vérifier";
  public const ETAT_VERIFIE = "Vérifiée";

	private ?int $id = null;

	#[Constraint\Ip(message: "l'ip %s n'est pas une ip valide.")]
	#[Constraint\UniqueEntity(message: "l'ip %s est déjà enregistrée")]
	#[Constraint\NotBlank(message: "l'ip ne peut pas être vide")]
	private ?string $ip = null;

	#[Constraint\IsDateTime(message:"La date doit être au format DateTime.")]
  private ?DateTime $dateBlacklistage = null;

	#[Constraint\InArray(array:[self::ETAT_A_VERIFIER, self::ETAT_VERIFIE], message: "l'état ne peut pas être vide")]
  private string $etat = self::ETAT_A_VERIFIER;

	public function convert(array $data = []): array
  {
    return [
			'id'               => isset($data[0]) ? $data[0] : (isset($data['id']) ? $data['id'] : null),
			'ip' 							 => isset($data[1]) ? $data[1] : (isset($data['ip']) ? $data['ip'] : null),
			'dateBlacklistage' => isset($data[2]) ? DateTime::createFromFormat('d/m/Y - H:i', $data[2]) : (isset($data['dateBlacklistage']) ? DateTime::createFromFormat('d/m/Y - H:i', $data['dateBlacklistage']) : new DateTime()),
			'etat'             => isset($data[3]) ? $data[3] : (isset($data['etat']) ? $data['etat'] : self::ETAT_A_VERIFIER)
    ];
  }


	/**
	 * Get the value of id
	 *
	 * @return  ?int
	 */
	public function getId(): ?int
	{
		return $this->id;
	}
  
	/**
	 * Set the value of id
	 *
	 * @param   int  $id  
	 *
   * @return void
	 */
	public function setId(?int $id): void
	{
		$this->id = $id;
	}

	/**
	 * Get the value of ip
	 *
	 * @return  string
	 */
	public function getIp(): string
	{
		return $this->ip;
	}
  
	/**
	 * Set the value of ip
	 *
	 * @param   string  $ip  
	 *
   * @return void
	 */
	public function setIp(?string $ip): void
	{
		$this->ip = $ip;
	}

	/**
	 * Get the value of dateBlacklistage
	 *
	 * @return  DateTime
	 */
	public function getDateBlacklistage(): DateTime
	{
		return $this->dateBlacklistage;
	}
  
	/**
	 * Set the value of dateBlacklistage
	 *
	 * @param   DateTime  $dateBlacklistage  
	 *
   * @return void
	 */
	public function setDateBlacklistage(?DateTime $dateBlacklistage): void
	{
		$this->dateBlacklistage = $dateBlacklistage;
	}

	/**
	 * Get the value of etat
	 *
	 * @return  string
	 */
	public function getEtat(): string
	{
		return $this->etat;
	}
  
	/**
	 * Set the value of etat
	 *
	 * @param   string  $etat  
	 *
   * @return void
	 */
	public function setEtat(?string $etat): void
	{
		$this->etat = $etat;
	}

	/**
	 * Fonction qui permet de transformer l'objet IpBlacklistee en Ip neutre.
	 * On ne passe que l'ip, afin de ne pas faire de conflit d'id.
	 *
	 * @return IpBlacklistee
	 */
	public function whitelist(): IpBlacklistee
	{
		return new IpBlacklistee([1 => $this->ip]);
	}
}