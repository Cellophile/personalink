<?php

namespace Services;

use ReflectionClass;
use Services\Interfaces\ConstraintInterface;

class Validator
{
  public static function validate($object): array
  {
    $reflectionClass = new ReflectionClass($object);
    $properties = $reflectionClass->getProperties();
    $errors = [];

    foreach ($properties as $property) {
      $attributes = $property->getAttributes();

      foreach ($attributes as $attribute) {
        $instance = $attribute->newInstance();
        if ($instance instanceof ConstraintInterface) {
          if(method_exists($instance, 'setClassName')){
            $setClassName = 'setClassName';
            $instance->$setClassName($reflectionClass->getName()); 
          }
          $property->setAccessible(true);
          $value = $property->getValue($object);
          $error = $instance->validate($property->getName(), $value);
          if ($error) {
            $errors[$property->getName()][] = $error;
          }
        }
      }
    }
    return $errors;
  }
}
