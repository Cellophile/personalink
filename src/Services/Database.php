<?php

namespace Services;

use Exception;
use Services\Components\CachedItem;

/**
 * La classe Database permet de manipuler les fichiers csv.
 */
class Database
{
  use FileAccess;

  private $OpenCSV = null;
  private ?int $lastInsertId = null;
  private ?Cache $cache = null;

  public function __construct()
  {
    $this->cache = Cache::initialize();
  }

  public function getCache(): Cache
  {
    $this->cache = Cache::initialize();
    return $this->cache;
  }

  private function readCache($NomCSV): CachedItem|bool
  {
    $this->cache = Cache::initialize();
    return $this->cache->get($NomCSV) !== null ? $this->cache->get($NomCSV) : false;
  }

  private function writeCache(string $NomCSV, mixed $Data, int $state): void
  {
    $this->cache = Cache::initialize();
    $this->cache->set($NomCSV, $Data, $state);
  }

  public function WriteIntoCSV(string $NomCSV, mixed $Data, string $Mode = "ab", int $cacheState = CachedItem::DB_TOTAL_UPDATED): int|false
  {
    $this->setLastInsertId($NomCSV);
    $CleanedData = $this->CleanData($Data);
    $this->OpenCSV($NomCSV, $Mode);
    if (!fwrite($this->OpenCSV, $CleanedData)) {
      return false;
    }
    $this->CloseCSV();

    if (is_array($Data)) {
      $cachedData = array_map(function ($element) {
        return is_object($element) ? $element->getObjectVars() : $element;
      }, $Data);
    } else if (is_object($Data)) {
      $cachedData = $Data->getObjectVars();
    } else {
      $cachedData = explode("\n", $Data);
    }
    $this->writeCache($NomCSV, $cachedData, $cacheState);
    $this->setLastInsertId($NomCSV);
    return $this->getLastInsertId();
  }

  public function ReadAllCSV(string $NomCSV, string $Mode = "r"): array
  {
    if (
      $this->readCache($NomCSV)
      && ($this->readCache($NomCSV)->isTotalCopy()
        || $this->readCache($NomCSV)->isTotalUpdated())
    ) {
      return $this->readCache($NomCSV)->getData();
    } else {
      $this->OpenCSV($NomCSV, $Mode);
      $Data = fread($this->OpenCSV, filesize($this->getCSV($NomCSV)) + 1);
      $this->CloseCSV();

      $rawData = explode("\n", $Data);
      $Data = [];
      foreach ($rawData as $ligne) {
        $ligne = explode(",", $ligne);
        if (!empty($ligne[0])) {
          $ligne[array_key_last($ligne)] =  rtrim(end($ligne));
          $Data[] = $ligne;
        }
      }

      $this->writeCache($NomCSV, $Data, CachedItem::DB_TOTAL_EXACT_COPY);
      return $Data;
    }
  }

  public function ReadOneUniqueDataOfCSV(string $NomCSV, string $Research, int $Position = 0, string $Mode = "r"): array|null
  {
    if (
      $this->readCache($NomCSV)
      && ($this->readCache($NomCSV)->isTotalCopy()
        || $this->readCache($NomCSV)->isTotalUpdated())
    ) {
      $data = $this->readCache($NomCSV)->getData();
      foreach ($data as $value) {
        if ($value[$Position] === $Research) {
          return $value;
        }
      }
    } else {
      // Si on a pas trouvé dans le cache :
      $data = null;
      $this->OpenCSV($NomCSV, $Mode);
      $cache = [];
      while (($ligne = fgetcsv($this->OpenCSV)) !== false) {
        $ligne[array_key_last($ligne)] = rtrim(end($ligne));
        $cache[] = $ligne;
        if ($ligne[$Position] === $Research) {
          $data = $ligne;
        }
      }
      $this->CloseCSV();
      $this->writeCache($NomCSV, $cache, CachedItem::DB_TOTAL_EXACT_COPY);
      return $data;
    }
  }

  public function ReadOneMaybeMultiDataOfCSV(string $NomCSV, string $Research, int $Position, string $Mode = "r"): array
  {
    $data = [];
    if (
      $this->readCache($NomCSV)
      && ($this->readCache($NomCSV)->isTotalCopy()
        || $this->readCache($NomCSV)->isTotalUpdated())
    ) {
      $cachedData = $this->readCache($NomCSV)->getData();
      foreach ($cachedData as $value) {
        if (array_values($value)[$Position] === $Research) {
          $data[] = $value;
        }
      }
      return $data;
    } else {
      // Si on a pas trouvé dans le cache :
      $data = [];
      $this->OpenCSV($NomCSV, $Mode);
      $cache = [];
      while (($ligne = fgetcsv($this->OpenCSV)) !== false) {
        $ligne[array_key_last($ligne)] = rtrim(end($ligne));
        $cache[] = $ligne;
        if ($ligne[$Position] === $Research) {
          $data[] = $ligne;
        }
      }
      $this->CloseCSV();
      $this->writeCache($NomCSV, $cache, CachedItem::DB_TOTAL_EXACT_COPY);
      return $data;
    }
  }

  private function OpenCSV(string $NomCSV, string $Mode = "r")
  {
    $this->OpenCSV = fopen($this->getCSV($NomCSV), $Mode);
  }

  /**
   * Ferme le fichier CSV ouvert en mode lecture.
   *
   * Cette méthode est appelée automatiquement par la méthode `CloseCSV` et
   * ferme le fichier CSV ouvert en mode lecture.
   *
   * @throws Exception Si le fichier CSV n'est pas ouvert en mode lecture.
   * @return void
   */
  /** */
  private function CloseCSV()
  {
    fclose($this->OpenCSV);
  }


  /**
   * Nettoie les données en les implémentant sous forme de chaîne de caractères.
   *
   * Cette méthode nettoie les données en les implémentant sous forme de chaîne 
   * de caractères. Elle est utilisée pour nettoyer les données avant de les 
   * stocker dans un fichier CSV.
   * 
   * Elle permet aussi d'attribuer le dernier ID pour un objet qui n'en a pas encore.
   *
   * @param mixed $Data Les données à nettoyer.
   * @return string Les données nettoyées sous forme de chaîne de caractères.
   */
  private function CleanData(mixed $Data)
  {
    if (is_array($Data)) {
      $Data = array_map(array($this, 'CleanData'), $Data);

      $cleanedData = implode("\n", $Data);
    } elseif (is_object($Data)) {
      if (!$Data->getId()) {
        $Data->setId($this->getLastInsertId() + 1);
      }

      $data = $Data->getObjectVars();
      foreach ($data as $key => $part) {
        if (is_array($part)) {
          unset($data[$key]);
        }
      }

      if ($Data->getId() === 1) {
        $cleanedData = implode(",", $data);
      } else {
        $cleanedData = "\n" . implode(",", $data);
      }
    } elseif (is_string($Data)) {
      $cleanedData = $Data; // On part du postulat que si on envoie une chaîne de caractères, C'est qu'on a déjà fait le traitement.
    }
    $cleanedData = str_replace("\n\n", "\n", $cleanedData);

    return $cleanedData;
  }

  private function getCSV(string $NomCSV): ?string
  {
    switch ($NomCSV) {
      case 'DB_IpBlacklistees':
        return self::CSV_IP_BLACKLIST;
        break;
      case 'DB_Ips':
        return self::CSV_IP;
        break;
      case 'DB_Relations_Ip_User':
        return self::CSV_REL_IP_USER;
        break;
      case 'DB_Liens':
        return self::CSV_URL_PERSO;
        break;
      case 'DB_UrlBlacklistees':
        return self::CSV_URL_BLACKLIST;
        break;
      case 'DB_Users':
        return self::USERS;
        break;
      case 'DB_Parametres':
        return self::PARAMETRES;
        break;


      default:
        throw new Exception("Le fichier $NomCSV n'existe pas.");
        return null;
        break;
    }
  }

  private function getClassName(string $NomCSV): ?string
  {
    switch ($NomCSV) {
      case 'DB_IpBlacklistees':
        return 'IpBlacklistee';
        break;
      case 'DB_Ips':
        return 'Ip';
        break;
      case 'DB_Liens':
        return 'Lien';
        break;
      case 'DB_UrlBlacklistees':
        return 'UrlBlacklistee';
        break;
      case 'DB_Users':
        return 'User';
        break;
      case 'DB_Parametres':
        return 'Parametre';
        break;


      default:
        throw new Exception("La classe liée $NomCSV n'existe pas.");
        return null;
        break;
    }
  }

  /**
   * Get the value of lastInsertId
   *
   * @return  int
   */
  public function getLastInsertId(): int
  {
    return $this->lastInsertId;
  }

  /**
   * Set the value of lastInsertId
   *
   * @param   int  $lastInsertId  
   *
   * @return void
   */
  public function setLastInsertId(string $NomCSV): void
  {
    if (!str_contains($NomCSV, 'Relations')) {
      $data = $this->ReadAllCSV($NomCSV, "r");
      $lastInsertId = isset($data[count($data) - 1][0]) ? $data[count($data) - 1][0] : (isset($data[count($data) - 1]['id']) ? $data[count($data) - 1]['id'] : 0);
      $this->lastInsertId = $lastInsertId;
    }
  }
}
