<?php

namespace Services;

use Models\Lien;
use Models\User;

class Statistic
{
  public function getMoyenneNbLiensParUser(array $users): int|float
  {
    if (!empty($users)) {
      $nbLiens = 0;
      $nbUsers = count($users);
      foreach ($users as $user) {
        $nbLiens += $user->getLiens()->count();
      }
      $moyenne = $nbLiens / $nbUsers;
      return round($moyenne, 2);
    } else {
      return 0;
    }
  }

  public function getMoyenneNbClicsParLien(array $liens): int|float
  {
    if (!empty($liens)) {
      $nbClics = 0;
      $nbLiens = count($liens);
      foreach ($liens as $lien) {
        $nbClics += $lien->getNbClics();
      }
      $moyenne = $nbClics / $nbLiens;
      return round($moyenne, 2);
    } else {
      return 0;
    }
  }

  public function getTotalLiensAvecDatePeremption(array $liens): int|float
  {
    $total = 0;
    foreach ($liens as $lien) {
      if ($lien->getDatexpi()) {
        $total++;
      }
    }
    return round($total, 2);
  }

  public function getTotalLiensAvecMdp(array $liens): int|float
  {
    $total = 0;
    foreach ($liens as $lien) {
      if ($lien->getMdp()) {
        $total++;
      }
    }
    return round($total, 2);
  }

  public function getNbLiensCreesParPeriode(array $liens): array
  {
    return $this->getNbModelCreesParPeriode($liens);
  }

  public function getNbUsersCreesParPeriode(array $users): array
  {
    return $this->getNbModelCreesParPeriode($users);
  }

  private function getNbModelCreesParPeriode(array $models): array
  {
    $dateActuelle = new \DateTime();
    $tableauAnnees = [];
    $tableauMois = [];
    $tableauJours = [];

    $dateDebutMois = (clone $dateActuelle)->modify('-11 months');
    $interval = new \DateInterval('P1M');
    $periode = new \DatePeriod($dateDebutMois, $interval, (clone $dateActuelle)->modify('+1 month'));

    $tableauMois = [];

    foreach ($periode as $date) {
      $mois = $date->format('Y-m');
      $tableauMois[$mois] = 0;
    }

    $dateDebutJours = (clone $dateActuelle)->modify('-30 days');
    $interval = new \DateInterval('P1D');
    $periode = new \DatePeriod($dateDebutJours, $interval, (clone $dateActuelle)->modify('+1 day'));

    $tableauJours = [];

    foreach ($periode as $date) {
      $jour = $date->format('Y-m-d');
      $tableauJours[$jour] = 0;
    }

    foreach ($models as $model) {
      if ($model instanceof User) {
        $ModelCreationDate = $model->getDateCreation();
      } else if ($model instanceof Lien) {
        $ModelCreationDate = $model->getDcreation();
      }
      if (isset($tableauAnnees[$ModelCreationDate->format('Y')])) {
        $tableauAnnees[$ModelCreationDate->format('Y')]++;
      } else {
        $tableauAnnees[$ModelCreationDate->format('Y')] = 1;
      }
      ksort($tableauAnnees);

      if ($ModelCreationDate >= $dateDebutMois) {
        $tableauMois[$ModelCreationDate->format('Y-m')]++;
      }

      if ($ModelCreationDate >= $dateDebutJours) {
        $tableauJours[$ModelCreationDate->format('Y-m-d')]++;
      }
    }
    return ['annees' => $tableauAnnees, 'mois' => $tableauMois, 'jours' => $tableauJours];
  }
}
