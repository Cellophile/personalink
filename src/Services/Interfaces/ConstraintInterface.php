<?php
namespace Services\Interfaces;

interface ConstraintInterface
{
  public function validate($property, $value): ?string;
}