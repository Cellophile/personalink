<?php

namespace Services;

trait FileAccess
{

  private const CSV_IP_BLACKLIST    = __DIR__ . "/../csv/DB_IpBlacklistees.csv";
  private const CSV_IP              = __DIR__ . "/../csv/DB_Ips.csv";
  private const CSV_REL_IP_USER     = __DIR__ . "/../csv/DB_Relations_Ip_User.csv";
  private const CSV_URL_PERSO       = __DIR__ . "/../csv/DB_Liens.csv";
  private const CSV_URL_BLACKLIST   = __DIR__ . "/../csv/DB_UrlBlacklistees.csv";
  private const USERS               = __DIR__ . '/../csv/DB_Users.csv';
  private const PARAMETRES          = __DIR__ . '/../csv/DB_Parametres.csv';
  private const SITEMAP             = __DIR__ . '/../../sitemap.xml';

  public static function getNomDomaine()
  {
    $parsedUrl = parse_url(URLSITE);
    $nomDomaine = isset($parsedUrl['host']) ? $parsedUrl['host'] : $parsedUrl['path'];

    return ucfirst(rtrim($nomDomaine, '/'));
  }

  public static function getAllCSV()
  {
    return [
      self::CSV_IP_BLACKLIST,
      self::CSV_IP,
      self::CSV_REL_IP_USER,
      self::CSV_URL_PERSO,
      self::CSV_URL_BLACKLIST,
      self::USERS,
      self::PARAMETRES
    ];
  }

  public static function isSitemapCreated(): bool
  {
    return file_exists(self::SITEMAP);
  }
}
