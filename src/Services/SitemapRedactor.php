<?php

namespace Services;

use DateTime;

class SitemapRedactor
{
  use FileAccess;

  public static function generateSitemap(): bool
  {
    if (!self::isSitemapCreated()) {
      $xml = "<?xml version='1.0' encoding='UTF-8'?>
<urlset
  xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'
  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
  xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'>

  <url>
    <loc>". URLSITE ."</loc>
    <lastmod>".(new DateTime())->format('Y-m-d\TH:i:sP')."</lastmod>
    <priority>1.00</priority>
  </url>
  <url>
    <loc>". URLSITE ."licence.php</loc>
    <lastmod>".(new DateTime())->format('Y-m-d\TH:i:sP')."</lastmod>
    <priority>0.80</priority>
  </url>
  <url>
    <loc>". URLSITE ."dashboard/</loc>
    <lastmod>".(new DateTime())->format('Y-m-d\TH:i:sP')."</lastmod>
    <priority>0.20</priority>
  </url>

</urlset>
      ";

      if (file_put_contents(self::SITEMAP, $xml)) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
}
