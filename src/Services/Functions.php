<?php
namespace Services;

class Functions
{
  /**
   * Permet de chercher si un objet existe dans un tableau
   *
   * @param object $object  l'objet à trouver 
   * @param array $array    Le tableau qui contient les objets
   * @param string $getter  Ce par quoi on veut chercher l'objet (ip, email, ...)
   * @return boolean
   */
  public static function ObjectInArray(object $object, array $array, string $getter): bool
  {
    $getter = 'get' . ucfirst($getter);
    foreach ($array as $item) {
      if ($object->$getter() === $item->$getter()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Permet de nettoyer une string
   *
   * @param string $string
   * @return string
   */
  public static function sanitizeString(string $string): string
  {
    $string = str_replace(',', '', $string);
    $string = trim($string);
    $string = htmlentities($string, ENT_QUOTES, 'UTF-8');

    return $string;
  }
}