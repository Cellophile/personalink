<?php

namespace Services;

use Services\Components\CachedItem;

class Cache
{
  private $cache = array();

  public static function initialize()
  {
    if (isset($_SESSION['cache'])) {
      return unserialize($_SESSION['cache']);
    } else {
      $cache = new Cache();
      $_SESSION['cache'] = serialize($cache);
      return $cache;
    }
  }

  public function get($key): ?CachedItem
  {
    return isset($this->cache[$key]) ? $this->cache[$key] : null;
  }

  public function set(string $key, mixed $value, int $state): void
  {
    if (!isset($this->cache[$key])) {
      $this->cache[$key] = new CachedItem($value, $state);
    } else {
      $this->update($key, $value, $state);
    }
    $this->UpdateSession();
  }

  public function update(string $key, mixed $value, int $state): void
  {
    switch ($this->cache[$key]->getState()) {
      case CachedItem::DB_UNIQUE_EXACT_COPY:
        if (!isset($this->cache[$key]->getdata()[0])) {
          $this->cache[$key]->setState($state);
          $this->cache[$key]->setData($value);
        } else {
          switch ($state) {
            case CachedItem::DB_UNIQUE_EXACT_COPY:
              if ($this->cache[$key]->getdata()[0] === $value[0] && count($this->cache[$key]->getdata()) === 1) {
                $this->cache[$key]->setState(CachedItem::DB_UNIQUE_EXACT_COPY);
              } else {
                $this->cache[$key]->setState(CachedItem::DB_PARTIAL_EXACT_COPY);
                $this->cache[$key]->setData($value);
              }
              break;

            case CachedItem::DB_UNIQUE_UPDATED:
              if ($this->cache[$key]->getdata()[0][0] === $value[0] && count($this->cache[$key]->getdata()) === 1) {
                $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
                $this->cache[$key]->setData($value, totalReplace: true);
              } else {
                $this->cache[$key]->setState(CachedItem::DB_UNIQUE_UPDATED);
                $this->cache[$key]->setData($value);
              }
              break;

            case CachedItem::DB_PARTIAL_EXACT_COPY:
              $this->cache[$key]->setState(CachedItem::DB_PARTIAL_EXACT_COPY);
              $this->cache[$key]->setData($value);
              break;

            case CachedItem::DB_PARTIAL_UPDATED:
              $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
              $this->cache[$key]->setData($value);
              break;

            case CachedItem::DB_NEW:
              $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
              $this->cache[$key]->setData($value);
              break;

            case CachedItem::DB_TOTAL_EXACT_COPY:
              $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
              $this->cache[$key]->setData($value);
              break;

            case CachedItem::DB_TOTAL_UPDATED:
              $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
              $this->cache[$key]->setData($value, totalReplace: true);
              break;
          }
        }
        break;

      case CachedItem::DB_UNIQUE_UPDATED:
        switch ($state) {
          case CachedItem::DB_UNIQUE_EXACT_COPY:
            if ($this->cache[$key]->getdata()[0] === $value[0] && count($this->cache[$key]->getdata()) === 1) {
              $this->cache[$key]->setState(CachedItem::DB_UNIQUE_UPDATED);
            } else {
              $this->cache[$key]->setState(CachedItem::DB_PARTIAL_EXACT_COPY);
              $this->cache[$key]->setData($value);
            }
            break;

          case CachedItem::DB_UNIQUE_UPDATED:
            if ($this->cache[$key]->getdata()[0][0] === $value[0] && count($this->cache[$key]->getdata()) === 1) {
              $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
              $this->cache[$key]->setData($value, totalReplace: true);
            } else {
              $this->cache[$key]->setState(CachedItem::DB_UNIQUE_UPDATED);
              $this->cache[$key]->setData($value);
            }
            break;

          case CachedItem::DB_PARTIAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_PARTIAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_NEW:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;
        }
        break;

      case CachedItem::DB_PARTIAL_EXACT_COPY:
        switch ($state) {
          case CachedItem::DB_UNIQUE_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_EXACT_COPY);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_UNIQUE_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_PARTIAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_EXACT_COPY);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_PARTIAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_NEW:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;

          case CachedItem::DB_TOTAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;
        }
        break;

      case CachedItem::DB_PARTIAL_UPDATED:
        switch ($state) {
          case CachedItem::DB_UNIQUE_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_UNIQUE_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_PARTIAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_PARTIAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_NEW:
            $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;
        }
        break;

      case CachedItem::DB_NEW:
        if ($state === CachedItem::DB_TOTAL_UPDATED || $state === CachedItem::DB_TOTAL_EXACT_COPY) {
          $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
        } else if ($state === CachedItem::DB_PARTIAL_EXACT_COPY || $state === CachedItem::DB_PARTIAL_UPDATED) {
          $this->cache[$key]->setState(CachedItem::DB_PARTIAL_UPDATED);
        } else if ($state === CachedItem::DB_UNIQUE_EXACT_COPY || $state === CachedItem::DB_UNIQUE_UPDATED) {
          $this->cache[$key]->setState(CachedItem::DB_UNIQUE_UPDATED);
        }
        $this->cache[$key]->setData($value);
        break;

      case CachedItem::DB_TOTAL_EXACT_COPY:
        switch ($state) {
          case CachedItem::DB_UNIQUE_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            break;

          case CachedItem::DB_UNIQUE_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_PARTIAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            break;

          case CachedItem::DB_PARTIAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_NEW:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_TOTAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_EXACT_COPY);
            $this->cache[$key]->setData($value);
            break;

          case CachedItem::DB_TOTAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;
        }
        break;

      case CachedItem::DB_TOTAL_UPDATED:
        switch ($state) {
          case CachedItem::DB_UNIQUE_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            break;

          case CachedItem::DB_UNIQUE_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_PARTIAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            break;

          case CachedItem::DB_PARTIAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_NEW:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, replaceOneWithNewValue: true);
            break;

          case CachedItem::DB_TOTAL_EXACT_COPY:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            break;

          case CachedItem::DB_TOTAL_UPDATED:
            $this->cache[$key]->setState(CachedItem::DB_TOTAL_UPDATED);
            $this->cache[$key]->setData($value, totalReplace: true);
            break;
        }
        break;
    }
  }

  public static function destroy(): void
  {
    unset($_SESSION['cache']);
  }

  public function UpdateSession(): void
  {
    $_SESSION['cache'] = serialize($this);
  }
}
