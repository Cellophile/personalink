<?php

namespace Services\Components;

class Request
{
  private array $POST;
  private array $GET;
  private array $REQUEST;
  private array $SERVER;
  private array $FILES;

  public function __construct()
  {
    if ($_POST !== null) {
      $this->POST = $this->sanitize($_POST);
    } else {
      $this->POST = $this->sanitize(json_decode(file_get_contents('php://input'), true));
    }

    if ($_GET !== null) {
      $this->GET = $this->sanitize($_GET);
    }

    if ($_REQUEST !== null) {
      $this->REQUEST = $this->sanitize($_REQUEST);
    }

    if ($_SERVER !== null) {
      $this->SERVER = $_SERVER;
    }

    if ($_FILES !== null) {
      foreach ($_FILES as $key => $value) {
        $this->FILES[$key] = $this->sanitize($value);
      }
    }
  }

  public function POST(): array
  {
    return $this->POST;
  }

  public function GET(): array
  {
    return $this->GET;
  }

  public function getData(): array
  {
    return array_merge($this->POST, $this->GET);
  }

  public function REQUEST(): array
  {
    return $this->REQUEST;
  }

  public function SERVER(): array
  {
    return $this->SERVER;
  }

  public function FILES(): array
  {
    return $this->FILES;
  }

  public function all(): array
  {
    return [
      'POST' => $this->POST,
      'GET' => $this->GET,
      'REQUEST' => $this->REQUEST,
      'SERVER' => $this->SERVER,
      'FILES' => $this->FILES
    ];
  }

  private function sanitize(mixed $rawData){
    if(is_array($rawData)) {
      $data = [];
      foreach($rawData as $key => $value)
      {
        $key = htmlentities($key);
        $value = htmlentities($value);
        $value = str_replace("\\","&#92;",$value); 
        $data[$key] = $value;
      }
    } else {
      $data = htmlentities($rawData);
    }
    return $data;
  }
}
