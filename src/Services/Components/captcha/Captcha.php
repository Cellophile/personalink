<?php

namespace Services\Components\captcha;

final class Captcha
{

  public function __construct()
  {
    $captcha = $this->creation();

    // Afficher l'image
    header('Content-type: image/jpeg');
    imagejpeg($captcha);

    // Libérer la mémoire
    imagedestroy($captcha);
    die;
  }

  private function creation()
  {
    // Générer le captcha
    for ($code = '', $i = 0, $z = strlen($a = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789') - 1; $i < 5; $i++) {
      $x = rand(0, $z);
      $code .= $a[$x];
    }

    $_SESSION['captcha'] = $code;

    // Créer l'image
    $captcha = imagecreate(130, 50);
    $colorbackground = imagecolorallocate($captcha, rand(200, 255), rand(200, 255), rand(200, 255)); // Couleur de fond aléatoire
    $textcolor = imagecolorallocate($captcha, 0, 0, 0);


    // Choisir une police aléatoire
    $input = array(__DIR__ . "/fonts/28DaysLater.ttf", __DIR__ . "/fonts/Allrightsreserved.ttf", __DIR__ . "/fonts/BrokenGlass.ttf", __DIR__ . "/fonts/NewspaperCutoutWhiteOnBlac-Rg.ttf");
    $choixfont = array_rand($input);
    $font = $input[$choixfont];

    // Dessiner le texte sur l'image
    imagettftext($captcha, 30, 0, 10, 40, $textcolor, $font, $_SESSION['captcha']);

    return $captcha;
  }
}
