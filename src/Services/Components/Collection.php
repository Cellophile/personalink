<?php
namespace Services\Components;

class Collection
{
  private ?array $data;

  public function __construct(null|object|array $data = null)
  {
    if(is_object($data)) {
      $this->data = [$data];
    } else if (is_null($data)) {
      $this->data = [];
    } else {
      $this->data = $data;
    }
  }

  public function add(Object $object)
  {
    $this->data[] = $object;
  }

  public function get(array $data): array|object
  {
    $array = [];
    foreach ($data as $key => $value) {
      $getter = 'get' . ucfirst($key);
  
      foreach ($this->data as $element) {
        if (method_exists($element, $getter) && $element->$getter() == $value) {
          $array[] = $element;
        }
      }
    }
    return count($array) > 1 ? $array : $array[0];
  }

  public function all()
  {
    return $this->data;
  }

  public function count()
  {
    if(is_null($this->data)) {
      return 0;
    }
    return count($this->data);
  }

  public function remove(Object $object)
  {
    $key = array_search($object, $this->data);
    unset($this->data[$key]);
  }

  public function clear()
  {
    $this->data = [];
  }

  public function has(Object $object)
  {
    return in_array($object, $this->data);
  }

  public function isEmpty()
  {
    return empty($this->data);
  }
}