<?php

namespace Services\Components\Constraints;

use DateTime;
use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IsDateTime implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  ) {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if ($value !== null &&
      (!$value instanceof DateTime || (is_string($value) &&
      DateTime::createFromFormat('Y-m-d H:i:s', $value) === false)
    )) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be a valid dateTime.';
    }
    return null;
  }
}
