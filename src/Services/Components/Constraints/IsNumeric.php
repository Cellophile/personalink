<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IsNumeric implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (!is_numeric($value)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be numeric.';
    }
    return null;
  }
  
}