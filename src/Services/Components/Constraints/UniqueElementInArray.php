<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class UniqueElementInArray implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (array_unique($value) !== $value) {
      return $this->message ? sprintf($this->message, $value) : 'Each element must be unique in the array.';
    }
    return null;
  }
  
}