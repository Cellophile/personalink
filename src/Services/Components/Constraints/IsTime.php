<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IsTime implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (@strtotime($value) === false || @strtotime(@time('H:i:s', @strtotime($value))) !== @strtotime($value)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be a valid time.';
    }
    return null;
  }
  
}