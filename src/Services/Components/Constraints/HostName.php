<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class HostName implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (preg_match('/^([a-zA-Z0-9][-a-zA-Z0-9]*[a-zA-Z0-9]?\.)*[a-zA-Z][-a-zA-Z0-9]{0,61}[a-zA-Z0-9]$/', $value)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be a valid host name.';
    }
    return null;
  }
  
}