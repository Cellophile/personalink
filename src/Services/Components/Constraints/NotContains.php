<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

/**
 * Class NotContains
 * 
 * @package Services\Components\Constraints
 * 
 * @property string $containedString la chaine de caractère à ne pas contenir
 * @property string $message le message d'erreur. Si la valeur est vide, le message d'erreur sera le nom de la propriété + ' must not contain ' + $containedString. Si vous voulez ajouter le nom de la propriété, ajoutez %s
 * @property bool $ignoreCase
 * 
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
class NotContains implements ConstraintInterface
{
  public function __construct(
    public string $containedString,
    public string $message = '',
    public bool $ignoreCase = false
  )
  {
    // $this->containedString = $containedString;
    // $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if ($this->ignoreCase) {
      $value = strtolower($value);
      $this->containedString = strtolower($this->containedString);
    }
    if (str_contains($value, $this->containedString)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must not contain ' . $this->containedString . '.';
    }
    return null;
  }
  
}