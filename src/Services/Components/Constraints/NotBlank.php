<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class NotBlank implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (empty($value)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' cannot be blank.';
    }
    return null;
  }
  
}