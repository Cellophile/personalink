<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY, Attribute::IS_REPEATABLE)]
class InArray implements ConstraintInterface
{
  public function __construct(
    public array $array = [],
    public string $message = ''
  )
  {
    $this->array = $array;
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (!in_array($value, $this->array, implode(', ', $this->array))) {
      return $this->message ? sprintf($this->message, $value, implode(', ', $this->array)) : $property . ' must be one of : ' . implode(', ', $this->array) . '.';
    }
    return null;
  }
  
}