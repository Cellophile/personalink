<?php

namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Type implements ConstraintInterface
{
  public function __construct(
    public string $message = '',
    public string $type = '',
  ) {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if ($this->type) {
      switch (true) {
        case ($this->type === 'integer'
          || $this->type === 'int'):
          if (!is_int($value)) {
            return $this->message ?: $property . ' must be an integer.';
          }
          break;

        case ($this->type === 'float'):
          if (!is_float($value)) {
            return $this->message ?: $property . ' must be a float.';
          }
          break;

        case ($this->type === 'string'):
          if (!is_string($value)) {
            return $this->message ?: $property . ' must be a string.';
          }
          break;

        case ($this->type === 'boolean'
          || $this->type === 'bool'):
          if (!is_bool($value)) {
            return $this->message ?: $property . ' must be a boolean.';
          }
          break;

        case ($this->type === 'array'):
          if (!is_array($value)) {
            return $this->message ?: $property . ' must be an array.';
          }
          break;

        case ($this->type === 'object'
          || str_contains($this->type, 'Models')):
          if (!is_object($value)) {
            return $this->message ?: $property . ' must be an object.';
          }
          break;

        case ($this->type === 'null'):
          if (!is_null($value)) {
            return $this->message ?: $property . ' must be null.';
          }
          break;

        case ($this->type === 'numeric'
          || $this->type === 'number'):
          if (!is_numeric($value)) {
            return $this->message ?: $property . ' must be a number.';
          }
          break;

        default:
          return $this->message ? sprintf($this->message, $value, $this->type) : $property . ' : the asked type ' . $this->type . ' is not supported.';
          break;
      }
    }
    return null;
  }
}
