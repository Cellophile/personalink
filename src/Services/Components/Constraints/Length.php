<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Length implements ConstraintInterface
{
  public function __construct(
    public ?int $minLength = null,
    public ?int $maxLength = null,
    public string $minMessage = '',
    public string $maxMessage = '',
  )
  {
    $this->minLength = $minLength;
    $this->maxLength = $maxLength;
    $this->minMessage = $minMessage;
    $this->maxMessage = $maxMessage;
  }

  public function validate($property, $value): ?string
  {
    if($value == null) {
      return $this->minMessage ? sprintf($this->minMessage, $value, $this->minLength) : $property . ' must be at least ' . $this->minLength . ' characters long.';
    }
    if($this->minLength && $this->minLength > strlen($value)) {
      return $this->minMessage ? sprintf($this->minMessage, $value, $this->minLength) : $property . ' must be at least ' . $this->minLength . ' characters long.';
    }

    if($this->maxLength && $this->maxLength < strlen($value)) {
      return $this->maxMessage ? sprintf($this->maxMessage, $value, $this->maxLength) : $property . ' must be no more than ' . $this->maxLength . ' characters long.';
    }
    return null;
  }
  
}