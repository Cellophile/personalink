<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Contains implements ConstraintInterface
{
  public function __construct(
    public string $containedString,
    public string $message = '',
    public bool $ignoreCase = false
  )
  {
    // $this->containedString = $containedString;
    // $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if ($this->ignoreCase) {
      $value = strtolower($value);
      $this->containedString = strtolower($this->containedString);
    }
    if (!str_contains($value, $this->containedString)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must contain ' . $this->containedString . '.';
    }
    return null;
  }
  
}