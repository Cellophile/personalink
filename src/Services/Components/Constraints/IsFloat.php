<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IsFloat implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (!filter_var($value, FILTER_VALIDATE_FLOAT)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be a float.';
    }
    return null;
  }
  
}
