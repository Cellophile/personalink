<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class IsDate implements ConstraintInterface
{
  public function __construct(
    public string $message = ''
  )
  {
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if (@strtotime($value) === false || @strtotime(@date('Y-m-d', @strtotime($value))) !== @strtotime($value)) {
      return $this->message ? sprintf($this->message, $value) : $property . ' must be a valid date.';
    }
    return null;
  }
  
}