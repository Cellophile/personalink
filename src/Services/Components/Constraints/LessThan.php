<?php
namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY, Attribute::IS_REPEATABLE)]
class LessThan implements ConstraintInterface
{
  public function __construct(
    public mixed $comparedValue = null,
    public string $message = ''
  )
  {
    $this->comparedValue = $comparedValue;
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if ($value > $this->comparedValue) {
      if(is_array($this->comparedValue)){
        $this->comparedValue = implode(', ',$this->comparedValue);
      }
      if(is_object($this->comparedValue)){
        $this->comparedValue = get_class($this->comparedValue);
      }
      return $this->message ? sprintf($this->message, $value, $this->comparedValue) : $property . ' must be less than ' . $this->comparedValue . '.';
    }
    return null;
  }
  
}