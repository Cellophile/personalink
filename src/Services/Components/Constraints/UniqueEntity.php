<?php

namespace Services\Components\Constraints;

use Attribute;
use Services\Interfaces\ConstraintInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class UniqueEntity implements ConstraintInterface
{
  public function __construct(
    public ?string $entityClass = null,
    public string $message = ''
  ) {
    $this->entityClass = $entityClass;
    $this->message = $message;
  }

  public function validate($property, $value): ?string
  {
    if($value === null) {
      return $property . ' should not be null.';
    }
    $repo = $this->getRepository();
    $repo = new $repo();
    if ($repo->alreadyExists([$property => $value])) {
      return $this->message ? sprintf($this->message, $value, $property) : $property . ' must be unique.';
    }
    return null;
  }

  public function setClassName(string $entityClass): void
  {
    $this->entityClass = $entityClass;
  }

  public function getRepository(): string
  {
    return str_replace('Models', 'Repositories', $this->entityClass) . 'Repository';
  }
}
