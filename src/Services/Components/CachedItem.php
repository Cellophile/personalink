<?php

namespace Services\Components;

use Services\FileAccess;

class CachedItem
{
	private mixed $data;
	private int $state;

	public const DB_UNIQUE_EXACT_COPY = 0;
	public const DB_UNIQUE_UPDATED = 1;
	public const DB_PARTIAL_EXACT_COPY = 2;
	public const DB_PARTIAL_UPDATED = 3;
	public const DB_TOTAL_EXACT_COPY = 4;
	public const DB_TOTAL_UPDATED = 5;
	public const DB_NEW = 6;

	public static array $states = [
		self::DB_UNIQUE_EXACT_COPY,
		self::DB_UNIQUE_UPDATED,
		self::DB_PARTIAL_EXACT_COPY,
		self::DB_PARTIAL_UPDATED,
		self::DB_TOTAL_EXACT_COPY,
		self::DB_TOTAL_UPDATED,
		self::DB_NEW
	];

	use FileAccess;

	public function __construct(mixed $data, int $state)
	{
		$this->setData($data);
		if (!in_array($state, self::$states)) {
			throw new \Exception('CachedItem : Invalid state');
		}
		$this->setState($state);
	}

	/**
	 * Get the value of Data
	 *
	 * @return  mixed
	 */
	public function getData(): mixed
	{
		return $this->data;
	}

	/**
	 * Set the value of data
	 *
	 * @param   mixed  $data  
	 *
	 * @return void
	 */
	public function setData(array $data, bool $totalReplace = false, bool $replaceOneWithNewValue = false): void
	{
		if(!empty($data) && !is_array($data[array_key_first($data)])) {
			$data = [$data];
		}
		if (!empty($this->data)) {
			if ($totalReplace) {
				$this->data = $data;
			} else if ($replaceOneWithNewValue) {
				foreach ($data as $value) {
					$exists = false;
					foreach ($this->data as $cle => $element) {
						if ($element[array_key_first($element)] === $value[array_key_first($value)]) {
							$exists = $cle;
							break;
						}
					}
					if ($exists !== false) {
						// On garde le second element
						$this->data[$exists] = $value;
					} else {
						$this->data[] = $value;
					}
				}
			} else {
				foreach ($data as $value) {
					$exists = false;
					foreach ($this->data as $cle => $element) {
						if ($element[0] == $value[0]) {
							$exists = $cle;
							break;
						}
					}
					if ($exists === false) {
						$this->data[] = $value;
					}
				}
			}
		} else {
			$this->data = $data;
		}
	}

	/**
	 * Get the value of state
	 *
	 * @return  int
	 */
	public function getState(): int
	{
		return $this->state;
	}

	/**
	 * Set the value of state
	 *
	 * @param   int  $state  
	 *
	 * @return void
	 */
	public function setState(int $state): void
	{
		$this->state = $state;
	}

	public function isTotalCopy(): bool
	{
		return $this->state === self::DB_TOTAL_EXACT_COPY;
	}

	public function isPartialCopy(): bool
	{
		return $this->state === self::DB_PARTIAL_EXACT_COPY;
	}

	public function isUniqueCopy(): bool
	{
		return $this->state === self::DB_UNIQUE_EXACT_COPY;
	}

	public function isNew(): bool
	{
		return $this->state === self::DB_NEW;
	}

	public function isPartialUpdated(): bool
	{
		return $this->state === self::DB_PARTIAL_UPDATED;
	}

	public function isTotalUpdated(): bool
	{
		return $this->state === self::DB_TOTAL_UPDATED;
	}

	public function isUniqueUpdated(): bool
	{
		return $this->state === self::DB_UNIQUE_UPDATED;
	}
}
