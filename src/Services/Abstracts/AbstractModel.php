<?php

namespace Services\Abstracts;

use Services\Components\Collection;

abstract class AbstractModel
{
  public function __construct(array $data = [])
  {

    if (!empty($data)) {
      $data = $this->convertStringToArray($data);
      $this->hydrate($this->convert($data));
    } else {
      $this->hydrate([]);
    }
  }

  public function getObjectVars(): array
  {
    $reflectionClass = new \ReflectionClass($this);
    $proprietes = $reflectionClass->getProperties();

    $vars = [];
    foreach ($proprietes as $propriete) {
      $propriete->setAccessible(true); // Rendre la propriété accessible
      if (is_object($propriete->getValue($this))) {
        if ($propriete->getValue($this) instanceof AbstractModel) {
          $vars[$propriete->getName()] = $propriete->getValue($this)->getId();
        } elseif ($propriete->getValue($this) instanceof \DateTime) {
          $vars[$propriete->getName()] = $propriete->getValue($this)->format('d/m/Y - H:i');
        } elseif ($propriete->getValue($this) instanceof Collection) {
          if (!is_null($propriete->getValue($this)->all())) {
            $vars[$propriete->getName()] = [];
            foreach ($propriete->getValue($this)->all() as $item) {
              $vars[$propriete->getName()][] = $item->getId();
            }
          } else {
            $vars[$propriete->getName()] = null;
          }
        }
      } else {
        $vars[$propriete->getName()] = $propriete->getValue($this);
      }
    }
    $vars = $this->convertArrayToString($vars);
    return $vars;
  }

  public function hydrate(array $data): void
  {
    foreach ($data as $key => $value) {
      $method = "set" . ucfirst($key);
      if (method_exists($this, $method)) {
        $this->$method($value);
      }
    }
  }
  
  abstract public function convert(array $data = []): array;

  private function convertStringToArray(array $Data): array
  {

    foreach ($Data as $key => $value) {
      if (is_string($value) && str_starts_with($value, "[") && str_ends_with($value, "]")) {
        $value = substr($value, 1, -1);
        $value = explode(";", $value);
        $Data[$key] = isset($value[1]) ? $value : $value[0];
      }
    }
    return $Data;
  }

  private function convertArrayToString(array $Data): array
  {
    foreach ($Data as $key => $value) {
      if (is_array($value)) {
        $Data[$key] = "[" . implode(";", $value) . "]";
      }
    }
    return $Data;
  }
}
