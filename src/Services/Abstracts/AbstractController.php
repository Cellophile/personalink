<?php
namespace Services\Abstracts;

use Models\User;
use Services\Components\Request;
use Services\Components\Response;
use Services\FileAccess;
use SimpleXMLElement;

abstract class AbstractController 
{
  use FileAccess;
  use Response;
  
  public const CONTENT_TYPES = [
    'json' => 'application/json',
    'xml' => 'application/xml',
    'html' => 'text/html',
    'text' => 'text/plain',
  ];

  protected static function render($view, $data = [], $status = self::HTTP_OK, $headers = []): void
  {
    http_response_code($status);
    foreach ($headers as $key => $value) {
      header($key . ': ' . $value);
    }

    extract($data);

    if(file_exists(__DIR__.'/../../../templates/'.$view.'.php')) {
      include __DIR__.'/../../../templates/'.$view.'.php';
    }
		exit();
  }

  protected static function json(mixed $data, int $statusCode = self::HTTP_OK, array $headers = []): void
  {
    self::responds($data, $statusCode, $headers, 'json');
  }

  protected static function xml(mixed $data, int $statusCode = self::HTTP_OK, array $headers = []): void
  {
    self::responds($data, $statusCode, $headers, 'xml');
  }

  protected static function html(mixed $data, int $statusCode = self::HTTP_OK, array $headers = []): void
  {
    self::responds($data, $statusCode, $headers, 'html');
  }

  protected static function text(mixed $data, int $statusCode = self::HTTP_OK, array $headers = []): void
  {
    self::responds($data, $statusCode, $headers, 'text');
  }

  protected static function erreur404(): void
	{
		http_response_code(self::HTTP_NOT_FOUND);
		header("HTTP/1.0 404 Not Found");
		header("location: ".URLSITE."?echec=404");
		exit();
	}

  protected static function redirection(string $lien): void
	{
    http_response_code(self::HTTP_MOVED_PERMANENTLY);
		header('HTTP/1.0 Moved Permanently'); 
		header('Location: '.$lien);      
		exit();
	}

  /**
   * Méthode pour rediriger vers une autre route de l'application.
   *
   * @param string $route La route est le chemin après le nom de domaine. Par exemple "/dashboard/mes-liens".
   * @param integer $statusCode Le statut de la requête.
   * @return void
   */
  protected static function redirectToRoute(string $route,int $statusCode = self::HTTP_OK): void
  {
    http_response_code($statusCode);
    header('HTTP/1.0 ' . self::$statusTexts[$statusCode]);
    header('Location: '.$route);      
    exit();
  }

  protected function getUser(): User|false
  {
    return isset($_SESSION['user']) ? unserialize($_SESSION['user']) : false;
  }

  protected function request(): Request
  {
    return new Request();
  }

  /**
   * Fonction permettant de rédiger une réponse, en choisissant le type de format de réponse : JSON, HTML, XML, TEXT.
   *
   * @param mixed $data
   * @param [type] $statusCode
   * @param array $headers
   * @param string $type [json, html, xml, text]
   * @return void
   */
  private static function responds(mixed $data, int $statusCode = self::HTTP_OK, array $headers = [], string $type = 'json'): void
  {
    http_response_code($statusCode);
    header('Content-Type: ' . self::CONTENT_TYPES[$type]);
    header('Status: ' . $statusCode . ' ' . self::$statusTexts[$statusCode], false, $statusCode);
    foreach ($headers as $key => $value) {
      header($key . ': ' . $value);
    }

    if (is_object($data)) {
      $data = $data->getObjectVars();
    }

    switch ($type) {
      case 'json':
        echo json_encode($data);
        break;
      case 'xml':
        echo self::convertToXML($data);
        break;
      case 'html':
        echo self::convertToHTML($data);
        break;
      case 'text':
        echo self::convertToText($data);
        break;
    }
		exit();
  }

  private static function convertToXML(mixed $data): string
  {
    $xml = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
    self::convertToXMLRec($data, $xml);
    return $xml->asXML();
  }

  private static function convertToXMLRec(array $data, SimpleXMLElement $xml): SimpleXMLElement
  {
    if (is_array($data)) {
      foreach ($data as $key => $value) {
        if (is_array($value)) {
          $child = $xml->addChild($key);
          self::convertToXMLRec($value, $child);
        } else {
          $xml->addChild($key, $value);
        }
      }
    } else {
      $xml->addChild('data', $data);
    }
    return $xml;
  }


  private static function convertToHTML(mixed $data): string
  {
    $html = '<ul>';
    if (is_array($data)) {
      foreach ($data as $key => $value) {
        if (is_array($value)) {
          $html .= '<li>' . $key . ' : ' . self::convertToHTML($value) . '</li>';
        } else {
          $html .= '<li>' . $key . ' : ' . $value . '</li>';
        }
      }
    } else {
      $html .= '<li>' . $data . '</li>';
    }
    $html .= '</ul>';
    return $html;
  }

  private static function convertToText(mixed $data): string
  {
    $text = '';
    if (is_array($data)) {
      foreach ($data as $key => $value) {
        if (is_array($value)) {
          $text .= $key . ' : ' . self::convertToText($value) . "\n";
        } else {
          $text .= $key . ' : ' . $value . "\n";
        }
      }
    } else {
      $text .= $data . "\n";
    }
    return $text;
  }
}