<?php

namespace Services\Abstracts;

use Services\Components\CachedItem;
use Services\Components\Collection;
use Services\Database;

abstract class AbstractRepository
{
  protected $db;
  protected $class;
  protected $className;

  private const ACTION_RESOLVER = ["lecture", "suppression", "ecriture"];
  private const ACTION_RESOLVER_LECTURE = self::ACTION_RESOLVER[0];
  private const ACTION_RESOLVER_SUPPRESSION = self::ACTION_RESOLVER[1];
  private const ACTION_RESOLVER_ECRITURE = self::ACTION_RESOLVER[2];

  public function __construct()
  {
    $this->db = new Database();
    $this->class = str_replace(["Repositories\\", "Repository"], ["Models\\", ""], get_class($this));
    $this->className = str_replace("Models\\", "", $this->class);
  }

  public function findAll(): array
  {
    $array = $this->db->ReadAllCSV($this->getCSV());
    $array = $this->resolverTableIntermediaire($array, self::ACTION_RESOLVER_LECTURE);

    foreach ($array as $key => $value) {
      $array[$key] = new $this->class($value);
    }
    if (is_array($array)) {
      return $array;
    }
    return array();
  }

  public function find(int $id): Object|null
  {
    $array = $this->db->ReadOneUniqueDataOfCSV($this->getCSV(), $id);
    if ($array === null) {
      return null;
    } else {
      return new $this->class($array);
    }
  }

  public function findBy(array $criteria): Object|array|null
  {
    $array = [];
    $position = null;
    $object = new $this->class;
    $OrderData = $object->convert([]);
    foreach ($criteria as $critere => $valeur) {
      $i = 0;
      foreach ($OrderData as $key => $value) {
        if ($key == $critere) {
          $position = $i;
        }
        $i++;
      }
      if ($position === null) {
        // On a pas trouvé ce critère dans cet objet, on renvoie null.
        return null;
      }
      $array = array_merge($array, $this->db->ReadOneMaybeMultiDataOfCSV($this->getCSV(), $valeur, $position));
    }
    if (count($array) == 0) {
      return null;
    }

    $array = array_unique($array, SORT_REGULAR);
    $array = $this->resolverTableIntermediaire($array, self::ACTION_RESOLVER_LECTURE);
    foreach ($array as $key => $value) {
      $array[$key] = new $this->class($value);
    }
    return count($array) > 1 ? $array : $array[0];
  }

  public function add(object $element): Object|bool
  {
    if ($element instanceof $this->class) {
      $this->db->WriteIntoCSV(NomCSV: $this->getCSV(), Data: $element, cacheState: CachedItem::DB_NEW);
      $element->setId($this->db->getLastInsertId());
      // On ajoute l'element dans les tables intermédiaires si elles existent
      $this->resolverTableIntermediaire([$element], self::ACTION_RESOLVER_ECRITURE);

      return $element;
    }
    return false;
  }

  public function update(Object $element): bool
  {
    if ($element instanceof $this->class) {
      $array = $this->replace($element);
      if (is_int($this->db->WriteIntoCSV($this->getCSV(), $array, "wb"))) {
        return true;
      }
    }
    return false;
  }

  public function delete(Object $element): bool
  {
    if ($element instanceof $this->class) {
      $array = $this->remove($element->getId());
      $this->db->WriteIntoCSV($this->getCSV(), $array, "wb");
      $this->resolverTableIntermediaire([$element], self::ACTION_RESOLVER_SUPPRESSION);
      return true;
    }
    return false;
  }

  public function count(): int
  {
    return count($this->findAll());
  }

  public function alreadyExists(array $criteria): bool
  {
    return $this->findBy($criteria) != null;
  }

  /**
   * Permet d'enlever un élément de la liste, avant d'en faire autre chose.
   * Cette méthode est appelée par update et delete.
   *
   * @param int $id
   * @return array
   */
  protected function remove(int $id): array
  {
    $array = $this->findAll();
    $newArray = array();
    foreach ($array as $actualElement) {
      if ($actualElement->getId() != $id) {
        $newArray[] = $actualElement;
      }
    }
    return $newArray;
  }

  protected function replace(object $element): array
  {
    $array = $this->findAll();
    foreach ($array as $key => $actualElement) {
      if ($actualElement->getId() == $element->getId()) {
        $array[$key] = $element;
      }
    }
    return $array;
  }

  abstract public function getCSV(): string;

  /**
   * Permet de resoudre les relations entre les tables.
   *
   * @param array $data Les données à compléter grâce aux tables intermédiaires
   * @return array
   */
  private function resolverTableIntermediaire(array $data, string $action = self::ACTION_RESOLVER_LECTURE): array
  {
    // On récupère tous noms des fichiers csv existants
    $CSVs = $this->db->getAllCSV();
    foreach ($CSVs as $CSV) {
      // On cherche si la classe est dans le nom du fichier
      if (str_contains($CSV, 'DB_Relations_') && str_contains($CSV, $this->className)) {
        // On reconstitue le nom du fichier
        $relation = explode('_', $CSV);
        $relation = [$relation[2], rtrim($relation[3], '.csv')];
        $fichier = 'DB_Relations_' . implode('_', $relation);
        $DataIntermediaires = $this->db->ReadAllCSV($fichier);

        if ($relation[0] == $this->className) {
          $positionElement = 0;
          $positionRelation = 1;
          $nomRelation = $relation[1];
        } else {
          $positionElement = 1;
          $positionRelation = 0;
          $nomRelation = $relation[0];
        }
        // En fonction de l'action à effectuer, on résoud la relation
        foreach ($data as $key => $element) {
          switch ($action) {
            case self::ACTION_RESOLVER_LECTURE:
              $data[$key][strtolower($nomRelation)] = $this->findDataIntoTableIntermediaire(DataIntermediaires: $DataIntermediaires, positionElement: $positionElement, id: $element[0] ?? ($element['id'] ?? $element->getId()), instanciableClass: $nomRelation);
              break;

            case self::ACTION_RESOLVER_SUPPRESSION:
              $this->deleteDataIntoTableIntermediaire(DataIntermediaires: $DataIntermediaires, fichier: $fichier, positionElement: $positionElement, id: $element->getId());
              break;

            case self::ACTION_RESOLVER_ECRITURE:
              $getter = method_exists($element, 'get' . $nomRelation) ? 'get' . $nomRelation : 'get' . $nomRelation . 's';
              $ElementsRelationnels = $element->$getter();
              if ($ElementsRelationnels instanceof Collection) {
                $ElementsRelationnels = $ElementsRelationnels->all();
                foreach ($ElementsRelationnels as $elementRelationnel) {
                  $DataIntermediaires[] = [
                    $positionElement => $element->getId(),
                    $positionRelation => $elementRelationnel->getId()
                  ];
                }
              } else {
                $DataIntermediaires[] = [
                  $positionElement => $element->getId(),
                  $positionRelation => $ElementsRelationnels->getId()
                ];
              }
              array_walk($DataIntermediaires, 'ksort');
              $this->writeDataIntoTableIntermediaire(DataIntermediaires: $DataIntermediaires, fichier: $fichier);
              break;

            default:
              $data[$key][strtolower($nomRelation)] = $this->findDataIntoTableIntermediaire(DataIntermediaires: $DataIntermediaires, positionElement: $positionElement, id: $element[0] ?? ($element['id'] ?? $element->getId()), instanciableClass: $nomRelation);
              break;
          }
        }
      }
    }
    return $data;
  }

  /**
   * Permet de trouver les données dans les tables intermédiaires, et de remplir chaque ligne avec les données associées
   *
   * @param array $DataIntermediaires
   * @param int $position
   * @param int $id
   * @param ?string $instanciableClass
   * @return Collection
   */
  private function findDataIntoTableIntermediaire(array $DataIntermediaires, int $positionElement, int $id, string $instanciableClass): Collection
  {
    if ($instanciableClass != null) {
      $repo = 'Repositories\\' . $instanciableClass . 'Repository';
      $repo = new $repo;
      $collection = new Collection();

      foreach ($DataIntermediaires as $element) {
        if ($element[$positionElement] == $id) {
          $positionElement == 0 ? $collection->add($repo->find($element[1])) : $collection->add($repo->find($element[0]));
        }
      }

      return $collection;
    }
  }

  private function deleteDataIntoTableIntermediaire(array $DataIntermediaires, string $fichier, int $positionElement, int $id): void
  {
    $finalData = [];
    foreach ($DataIntermediaires as $element) {
      if ($element[$positionElement] != $id) {
        $finalData[] = $element;
      }
    }
    $finalData = implode("\n", array_map(function ($item) {
      return implode(",", $item);
    }, $finalData));

    $this->db->WriteIntoCSV($fichier, $finalData, 'wb');
  }

  private function writeDataIntoTableIntermediaire(array $DataIntermediaires, string $fichier): void
  {
    $finalData = implode("\n", array_map(function ($item) {
      return implode(",", $item);
    }, $DataIntermediaires));
    $this->db->WriteIntoCSV($fichier, $finalData, 'wb');
  }
}
