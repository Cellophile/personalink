<?php
namespace Services;

use Services\FileAccess;

class EnvoiEmail
{
  use FileAccess;

  /**
   * Envoie un email
   *
   * @param string $contenu
   * @param string|null $sujet Par défaut : "Nouvel email de " . self::getNomDomaine()  
   * @param string|null $destinataire Par défaut : ADMINEMAIL
   * @param array $headers 
   * @return bool
   */
  public static function send(string $contenu, string $sujet = null, string $destinataire = null, array $headers = []): bool
  {
    $destinataire = $destinataire ?? ADMINEMAIL;
    $sujet = $sujet ?? "Nouvel email de " . self::getNomDomaine();
    $headers = array_merge(self::getHeadersBasiques(), $headers);

    if (mail($destinataire, $sujet, $contenu, $headers)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Permet de récupérer les headers par défaut
   *
   * @return array
   */
  public static function getHeadersBasiques(): array
  {

    return [
      "Content-Type" => " text/html; charset=UTF-8",
      "From" => self::getNomDomaine() . " <" . ENVOIEMAIL . ">",
      "Reply-To" => self::getNomDomaine() . " <" . REPONSEEMAIL . ">",
    ];
  }
}
