<?php

namespace Services;

use Models\Ip;
use Models\IpBlacklistee;
use Repositories\IpBlacklisteeRepository;
use Repositories\IpRepository;
use Repositories\UrlBlacklisteeRepository;
use Services\EnvoiEmail;
use Services\FileAccess;

class AntiPishing
{
  use FileAccess;

  public static function antipishing()
  {
    $date = new \DateTime;
    $ip   = self::getIp();
    if (self::isIpBlacklist($ip->getIp())) {
      header("location: https://www.cnil.fr/fr/spam-phishing-arnaques-signaler-pour-agir");
      exit();
    }

    $retour = self::VerifierNbUtilisationIp($ip);


    // si la somme des intervalles entre les dates des 5 derniers liens créés est inférieure à 5 minutes, alors on blackliste l'IP : ( 5 liens, 5 intervalles car on garde la valeur entre le 1er des 5 derniers liens et celui d'avant : 5x60 = 300)
    if ($retour['warn'] == 5 && $retour['diff'] <= 300) {
      $ipBl = new IpBlacklistee;
      $ipBl->setIp($ip->getIp());
      $ipBl->setDateBlacklistage($date);

      $ipBlRepo = new IpBlacklisteeRepository;
      $ipBlRepo->add($ipBl);

      self::EnvoiMailAdminIpBlacklist($retour['infosLiens']);

      header("location: /?echec=665");
      exit();
    } else {
      return true;
    }
  }

  /**
   * Methode qui permet de récupérer l'adresse IP du client, le domaine du client, le navigateur et le REFERER, s'ils existent.
   *
   * @return array
   */
  private static function findip(): array
  {
    // récupération de l'adresse IP du client (on cherche d'abord à savoir si il est derrière un proxy)
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
      $ip  = $_SERVER['HTTP_CLIENT_IP'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    // récupération du domaine du client
    $host = gethostbyaddr($ip);

    // récupération du navigateur et de l'OS du client
    $navigateur = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'inconnu';

    $referer = isset($referer) ? $_SERVER['HTTP_REFERER'] : '';

    return [
      'ip' => $ip,
      'host' => $host,
      'navigateur' => $navigateur,
      'referer' => $referer
    ];
  }

  public static function getIp(): Ip
  {
    $IpRepo = new IpRepository;
    $ip = $IpRepo->findBy(['ip' => self::findip()['ip']]);
    if ($ip) {
      return $ip;
    } else {
      $ip = new Ip([null, self::findip()['ip']]);
      $ip = $IpRepo->add($ip);
      return $ip;
    }
  }

  /**
   * urlblacklist permet de savoir si l'url passée en paramètre est blacklistée ou pas.
   *
   * @param string $url
   * @return boolean [false si l'url n'est pas blacklistée, true si l'url est blacklistée]
   */
  public static function isUrlblacklist(string $url): bool
  {
    $url = parse_url($url)['host'];
    $sousDomaines = explode('.', $url);
    if (count($sousDomaines) > 2) {
      $url = $sousDomaines[count($sousDomaines) - 2] . '.' . $sousDomaines[count($sousDomaines) - 1];
    } else {
      $url = $sousDomaines[0] . '.' . $sousDomaines[1];
    }
    $urlblacklistRepo = new UrlBlacklisteeRepository;
    $urlblacklist = $urlblacklistRepo->findBy(['url' => $url]);
    if ($urlblacklist) {
      header('location: /?echec=405');
      exit();
    }
    return false;
  }

  /**
   * Permet de vérifier si l'IP utilisée par le client est dans la liste noire.
   *
   * @param string $ip
   * @return bool true si l'IP est dans la liste noire, false sinon
   */
  public static function isIpBlacklist(string|Ip $ip): bool
  {
    $ipBlacklistRepo = new IpBlacklisteeRepository;
    if (is_string($ip)) {
      $ipBlacklist = $ipBlacklistRepo->findBy(['ip' => $ip]);
    } else {
      $ipBlacklist = $ipBlacklistRepo->findBy(['ip' => $ip->getIp()]);
    }
    if ($ipBlacklist) {
      header('location: /?echec=665');
      exit();
    }
    return false;
  }

  /**
   * Permet d'ajouter une nouvelle IP à la liste des IPs utilisées par le client.
   *
   * @param string $ip
   * @param string $date
   * @param string $lienperso
   * @param string $urlchoisie
   * @param boolean $blackList true si l'IP doit aller dans la liste noire
   * @return void
   */
  private static function EnregistrerIp(string $ip, string $date, string $lienperso, string $urlchoisie, bool $blackList = false): void
  {
    $fichier = $blackList ? self::CSV_IP_BLACKLIST : self::CSV_IP;
    $list = array($ip, $date, $lienperso, $urlchoisie, 'À vérifier');
    $fp = fopen($fichier, "ab");
    fputcsv($fp, $list);
    fclose($fp);
  }

  private static function VerifierNbUtilisationIp(Ip $ip): array
  {
    $warn = 0;
    $diff = 0;
    $a = 0; // comptera le nombre de liens faits avec cette ip
    if (is_array($ip->getLiens())) {
      foreach ($ip->getLiens() as $lien) {
        $a++;
        if ($a == 1) { // si c'est le premier enregistrement :
          $ipdateenr[$a] = $lien->getDcreation();
          $lienpersoenr[$a] = $lien->getUpersok();
          $urlchoisieenr[$a] = $lien->getUrlbase();
          $ipdiff[$a] = 0;
        } elseif ($a > 1 && $a < 5) { // si c'est le 2, 3 ou 4eme :
          $ipdateenr[$a] = $lien->getDcreation();
          $lienpersoenr[$a] = $lien->getUpersok();
          $urlchoisieenr[$a] = $lien->getUrlbase();

          $ipdiff[$a] = abs($ipdateenr[$a - 1] - $ipdateenr[$a]);
        } elseif ($a >= 5) {
          // s'il y a plus de 5 enregistrements, on remplace le premier par le dernier pour ne garder que les 5 derniers.
          $j = $a % 5;
          if ($j == 0) {
            $j = 5;
          }
          $ipdateenr[$j] = $lien->getDcreation();
          $lienpersoenr[$j] = $lien->getUpersok();
          $urlchoisieenr[$j] = $lien->getUrlbase();

          if ($j == 1) {
            $ipdiff[$j] = abs($ipdateenr[5] - $ipdateenr[$j]);
          }
          if ($j != 1) {
            $ipdiff[$j] = abs($ipdateenr[$j - 1] - $ipdateenr[$j]);
          }
        }
      }


      // on additionne ensuite toutes les différences de dates :
      for ($k = 1; $k < 6; $k++) {
        if (!isset($ipdiff[$k])) {
          break;
        }
        $diff = $diff + $ipdiff[$k];
        $warn++;
      }

      return ['warn' => $warn, 'diff' => $diff, 'infosLiens' => ['urlchoisieenr' => $urlchoisieenr, 'lienpersoenr' => $lienpersoenr]];
    }
    return ['warn' => 0, 'diff' => 0, 'infosLiens' => ['urlchoisieenr' => [], 'lienpersoenr' => []]];
  }

  private static function EnvoiMailAdminIpBlacklist(array $infosLiens): void
  {
    $infosUser = self::findip();
    $sujet = "Nouveau blacklistage sur 1lien.top";
    $corps = "l'adresse IP " . $infosUser['ip'] . " a été blacklistée le " . date("Y-m-d H:i:s") . ".
      Infos supplémentaires :
       • Host : " . $infosUser['host'] . "
       • Navigateur : " . $infosUser['navigateur'] . "
       • Referer : " . $infosUser['referer'] . "

       5 Derniers liens perso souhaités:
       • " . $infosLiens['lienpersoenr'][1] . " : " . $infosLiens['urlchoisieenr'][1] . ",
       • " . $infosLiens['lienpersoenr'][2] . " : " . $infosLiens['urlchoisieenr'][2] . ",
       • " . $infosLiens['lienpersoenr'][3] . " : " . $infosLiens['urlchoisieenr'][3] . ",
       • " . $infosLiens['lienpersoenr'][4] . " : " . $infosLiens['urlchoisieenr'][4] . ",
       • " . $infosLiens['lienpersoenr'][5] . " : " . $infosLiens['urlchoisieenr'][5] . ".";


    if (EnvoiEmail::send($corps, $sujet)) {
      echo "mail envoyé";
    } else {
      echo "le mail n'est pas parti";
    }
  }
}
