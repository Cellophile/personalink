<?php 
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class IpRepository extends AbstractRepository
{
  private const CSV = "DB_Ips";

  public function getCSV(): string
  {
    return self::CSV;
  }
}