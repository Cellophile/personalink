<?php 
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class LienRepository extends AbstractRepository
{
  private const CSV = "DB_Liens";

  public function getCSV(): string
  {
    return self::CSV;
  }
}