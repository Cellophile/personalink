<?php
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class UrlBlacklisteeRepository extends AbstractRepository 
{
  private const CSV = "DB_UrlBlacklistees";

  public function getCSV(): string
  {
    return self::CSV;
  }
}