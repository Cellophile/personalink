<?php 
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class UserRepository extends AbstractRepository
{
  private const CSV = "DB_Users";

  public function getCSV(): string
  {
    return self::CSV;
  }
}