<?php
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class ParametreRepository extends AbstractRepository {
  private const CSV = "DB_Parametres";

  public function getCSV(): string
  {
    return self::CSV;
  }
}