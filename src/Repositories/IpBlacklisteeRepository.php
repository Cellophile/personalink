<?php 
namespace Repositories;

use Services\Abstracts\AbstractRepository;

class IpBlacklisteeRepository extends AbstractRepository
{
  private const CSV = "DB_IpBlacklistees";

  public function getCSV(): string
  {
    return self::CSV;
  }
}