<?php
/**
 * ! LA CONFIGURATION DU SITE SE FAIT DEPUIS L'ESPACE ADMIN.
 * Si vous décommentez certaines des lignes ci-dessous, la configuration sera automatiquement mise à jour,
 * en prenant en priorité ce qui se trouve dans ce fichier. Les paramètres sont directement modifiables dans
 * l'espace administrateur. Si vous les écrivez ici, les modifications faites depuis l'espace administrateur
 * ne seront pas prises en compte. 
 */
/**
 * adresse où se situe le programme. ne pas oublier le / final
 */ 
// define('URLSITE', 'http://personalink/');

/**
 * Titre du site
 */
// define('TITRE', 'Un lien clair et court !');

/**
 * Chemin du logo : pas besoin de préciser le dossier, il sera cherché automatiquement
 * dans le dossier /public/assets/images/
 */
// define('LOGO', 'logox200.png');

/**
 * Sous-titre du site
 */
// define('SOUSTITRE', 'Raccourcissez et Personnalisez vos liens');

/**
 * EMAILS (Tout est à modifier) :
 * - ADMINEMAIL : adresse de l'administrateur
 * - ENVOIEMAIL : adresse qui permettra d'écrire aux utilisateurs
 * - REPONSEEMAIL : adresse qui permettra aux utilisateurs de contacter l'administrateur
 * - BLACKLISTEMAIL : adresse qui permettra aux utilisateurs de contacter l'administrateur à propos d'un
 * blacklistage contesté.
 * 
 * Ces adresses peuvent être les mêmes. Il est proposé ici de pouvoir les différencier, mais rien ne vous y oblige.
 */
// define('ADMINEMAIL', 'theophile@captp.fr');
// define('ENVOIEMAIL', 'no-reply@captp.fr');
// define('REPONSEEMAIL', 'reponses@captp.fr');
// define('BLACKLISTEMAIL', 'blacklist-1lientop@1lien.top');

/**
 * LANGUE choisie pour le site.
 *  - 'fr_FR' : Français
 */
// define('LANGUAGE', 'fr_FR');

/**
 * API de raccourcissement
 * Si vous souhaitez utiliser l'API pour raccourcir des liens depuis d'autres sites, remplissez les informations ci-dessous.
 * * API_ACTIVATED : 
 *    - doit être sur false pour empêcher l'utilisation de l'API externe
 * * API_ACCESS
 * peut contenir deux valeurs :
 *    - '*' pour autoriser tous les sites à utiliser l'API (non recommandé)
 *     - ['https://VOTREDOMAINE.COM'] pour autoriser uniquement le site https://VOTREDOMAINE.COM (vous pouvez mettre plusieurs sites ici, separés par une virgule : ['https://VOTREDOMAINE.COM', 'https://VOTREDOMAINE2.COM'])
 * * API_KEY :
 *    - doit être utilisée que si API_ACCESS est '*'. mettre à null si vous n'en voulez pas (non recommandé). Vous pouvez avoir autant de clefs que vous voulez, en les séparant par une virgule.
 * * API_METHOD
 * peut contenir deux valeurs :
 *    - 'POST' pour utiliser la methode POST
 *    - 'GET' pour utiliser la methode GET.
 * ! Si vous utiliser API_ACCESS = '*' et une API_KEY, il est fortement recommandé de mettre API_METHOD = 'POST'. 
 */
// define('API_ACTIVATED', true); // définir 'true' ou 'false'
// define(
//   'API_ACCESS',
//   // [
//   //   'https://VOTRE_DOMAINE_EXTERNE.COM' // À modifier
//   // ]
//   '*'
// );
// define('API_KEY', [
//   'bsdq45cnkfsdvdsakop563h89azpotgzjh', // À modifier
// ]);
// define('API_METHOD', ['POST', 'GET']); // définir 'POST' ou 'GET' ou les deux.

/**
 * Version du programme
 * NE PAS MODIFIER
 */
define('VERSION', 'Version 2.0.1 — 15/10/2024');

/**
 * Si votre nom de domaine pointe directement sur le dossier public, pas besoin de modifier cela. Sinon, indiquez le chemin nécessaire (par exemple : /public/)
 */
define('HOME_URL', '/');