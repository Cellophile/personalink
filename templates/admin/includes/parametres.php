<div style="display:flex;">
  <h2>Paramètres</h2>
</div>
<table class="tableau_paramètres">
  <tr class="titre_colonne">
    <th scope="col">Caractéristiques</th>
    <th scope="col">Spécification</th>
    <th scope="col">Outils</th>
  </tr>
  <tr>
    <td>Version du programme</td>
    <td><?= VERSION ?></td>
    <td></td>
  </tr>
  <tr>
    <td>Version php</td>
    <td><?= PHP_VERSION ?></td>
    <td></td>
  </tr>
  <tr>
    <td>Titre du site</td>
    <td><input type="text" name="TITRE" class="input_disabled" disabled value="<?= TITRE ?>"></td>
    <td>
      <img id="modTITRE" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('TITRE')">
      <img id="sauvTITRE" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('TITRE')">
    </td>
  </tr>
  <tr>
    <td>Logo du site</td>
    <td>
      <img id="LOGO" src="/assets/images/<?= LOGO ?>" style="width: 200px; height: auto;" alt="LOGO du site <?= LOGO ?>">
      <input id="INPUTLOGO" type="file" name="LOGO" class="input_disabled displayNone" disabled value="<?= LOGO ?>">
    </td>
    <td>
      <img id="modLOGO" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('LOGO')">
      <img id="sauvLOGO" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('LOGO')">
    </td>
  </tr>
  <tr>
    <td>Sous-titre du site</td>
    <td><input type="text" name="SOUSTITRE" class="input_disabled" disabled value="<?= SOUSTITRE ?>"></td>
    <td>
      <img id="modSOUSTITRE" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('SOUSTITRE')">
      <img id="sauvSOUSTITRE" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('SOUSTITRE')">
    </td>
  </tr>
  <tr>
    <td>Email d'administration</td>
    <td><input type="email" name="ADMINEMAIL" class="input_disabled" disabled value="<?= ADMINEMAIL ?>"></td>
    <td>
      <img id="modADMINEMAIL" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('ADMINEMAIL')">
      <img id="sauvADMINEMAIL" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('ADMINEMAIL')">
    </td>
  </tr>
  <tr>
    <td>Email d'envoi automatique</td>
    <td><input type="email" name="ENVOIEMAIL" class="input_disabled" disabled value="<?= ENVOIEMAIL ?>"></td>
    <td>
      <img id="modENVOIEMAIL" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('ENVOIEMAIL')">
      <img id="sauvENVOIEMAIL" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('ENVOIEMAIL')">
    </td>
  </tr>
  <tr>
    <td>Email auquel les personnes peuvent répondre</td>
    <td><input type="email" name="REPONSEEMAIL" class="input_disabled" disabled value="<?= REPONSEEMAIL ?>"></td>
    <td>
      <img id="modREPONSEEMAIL" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('REPONSEEMAIL')">
      <img id="sauvREPONSEEMAIL" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('REPONSEEMAIL')">
    </td>
  </tr>
  <tr>
    <td>Email de réclamation pour les blacklistages</td>
    <td><input type="email" name="BLACKLISTEMAIL" class="input_disabled" disabled value="<?= BLACKLISTEMAIL ?>"></td>
    <td>
      <img id="modBLACKLISTEMAIL" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('BLACKLISTEMAIL')">
      <img id="sauvBLACKLISTEMAIL" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('BLACKLISTEMAIL')">
    </td>
  </tr>
  <tr>
    <td>Langue de l'application</td>
    <td><input type="text" name="LANGUAGE" class="input_disabled" disabled value="<?= LANGUAGE ?>"></td>
    <td>
      <img id="modLANGUAGE" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('LANGUAGE')">
      <img id="sauvLANGUAGE" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('LANGUAGE')">
    </td>
  </tr>
  <tr>
    <td>Api externe</td>
    <td>
      <span class="API_ACTIVATED"><?= API_ACTIVATED ? "✅ activée" : "❌ désactivée" ?></span>
      <input type="checkbox" name="API_ACTIVATED" class="input_disabled displayNone" style="width: auto;" disabled <?= API_ACTIVATED ? "checked" : "" ?>>
      <label for="API_ACTIVATED" class="displayNone">Activer</label>
    </td>
    <td>
      <img id="modAPI_ACTIVATED" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_param('API_ACTIVATED')">
      <img id="sauvAPI_ACTIVATED" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_param('API_ACTIVATED')">
    </td>
  </tr>
</table>

<div id="toast" class="displayNone"></div>


<script>
  const toast = document.getElementById('toast');

  function modifier_param(param) {
    document.getElementsByName(param)[0].disabled = false;
    document.getElementsByName(param)[0].focus();

    document.getElementById("mod" + param).style.display = "none";
    document.getElementById("sauv" + param).style.display = "inline";

    if (param == "LOGO") {
      let inputLogo = document.getElementById('INPUTLOGO');
      inputLogo.classList.remove('displayNone');
      inputLogo.disabled = false;

      inputLogo.addEventListener('change', function(event) {
        const files = event.target.files;
        if (files.length > 0) {
          let file = files[0];
          if (file.type == "image/png" || file.type == "image/jpeg") {
            document.getElementById('LOGO').src = URL.createObjectURL(file);
          } else {
            displayToast("error", "Veuillez choisir une image au format png ou jpeg");
          }
        }
      })
    }

    if (param == "API_ACTIVATED") {
      document.getElementsByName(param)[0].classList.remove('displayNone');
      document.querySelector('label[for="API_ACTIVATED"]').classList.remove('displayNone');
      document.getElementsByClassName('API_ACTIVATED')[0].classList.add('displayNone');
    }
  }

  function sauvegarder_param(param) {
    document.getElementsByName(param)[0].disabled = true;

    document.getElementById("mod" + param).style.display = "inline";
    document.getElementById("sauv" + param).style.display = "none";
    let value = document.getElementsByName(param)[0].value;

    if (param == "LOGO") {
      document.getElementById("INPUTLOGO").classList.add('displayNone');
      document.getElementById("INPUTLOGO").disabled = true;
      value = document.getElementById('INPUTLOGO').files[0];
    }

    if (param == "API_ACTIVATED") {
      value = document.getElementsByName(param)[0].checked ? true : false;
      document.getElementsByClassName('API_ACTIVATED')[0].innerText = value ? "✅ activée" : "❌ désactivée";
      document.getElementsByName(param)[0].classList.add('displayNone');
      document.querySelector('label[for="API_ACTIVATED"]').classList.add('displayNone');
      document.getElementsByClassName('API_ACTIVATED')[0].classList.remove('displayNone');
    }
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState === 4 && r.status === 200) {
        displayToast("success", "Paramètre sauvegardé");
        document.querySelector('#logo img').src = document.getElementById('LOGO').src;
      } else if (r.readyState === 4 && r.status !== 200) {
        try {
          displayToast("error", "Quelque chose s'est mal passé : " + JSON.parse(r.responseText));
        } catch (e) {
          displayToast("error", "Quelque chose s'est mal passé...");
        }
      } else {
        displayToast("info", "Veuillez patienter...");
      }
    }
    r.open("POST", API_URL + "/parametres/update/" + param, true);
    let formData = new FormData();
    formData.append("parametre", param);
    formData.append("valeur", value);
    r.send(formData);
  }

  function displayToast(type, message) {
    toast.innerHTML = message;
    toast.classList.remove('displayNone', 'error', 'success', 'info');
    toast.classList.add(type);
    setTimeout(function() {
      toast.classList.replace(type, 'displayNone');
    }, 3000);
  }
</script>