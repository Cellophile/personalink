<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/assets/css/style_dashboard.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/style_admin.css" type="text/css">
  <script>
    const HOME_URL = '<?= HOME_URL ?>';
    const API_URL = '<?= HOME_URL ?>admin/webapi';
  </script>

  <title>Tableau d'Administration</title>
</head>

<body>
  <div id="entete" class="entete">
    <div id="logo">
      <img src="/assets/images/<?= LOGO ?>" alt="Logo <?= TITRE ?>" onclick="location.href='<?= URLSITE ?>'">
    </div>
    <h1>Tableau d'Administration</h1>
    <form action="/deconnexion" method="post">
      <input class="bouton" type="submit" name="deconnexion" value="Se déconnecter">
    </form>
  </div>