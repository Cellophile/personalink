<?php
?>
<div style="display:flex;">
  <h2>Liste des utilisateurs inscrits</h2>
</div>
<table class="tableau_users">
  <tr class="titre_colonne">
    <th scope="col">Nom</th>
    <th scope="col">Prénom</th>
    <th scope="col">Email</th>
    <th scope="col">Date d'inscription</th>
    <th scope="col">Dernière connexion</th>
    <th scope="col">Nb d'erreurs de connexion</th>
    <th scope="col">Compte activé</th>
    <th scope="col">Rôle</th>
    <th scope="col">Ip(s) associée(s)</th>
    <th scope="col" style="width: 70px;">Outils</th>
  </tr>
  <?php
  if (empty($users)) { ?>
    <tr>
      <th colspan="7">Aucun utilisateur inscrit </th>
    </tr>
    <?php
  } else {
    $i = 0;
    foreach ($users as $user) {
      $i++; ?>
      <tr <?= $user->isBlacklisted() ? 'style="background-color: #e5e5e5"' : '' ?>>
        <th scope="row"><?= $user->getNom() ?></th>
        <td ><?= $user->getPrenom() ?></td>
        <td class="titre_ligne" id="email<?= $i; ?>"><?= $user->getEmail() ?></td>
        <td><?= $user->getDateCreation()->format('d/m/Y - H:i') ?></td>
        <td><?= $user->getDateDernierLogin() ? $user->getDateDernierLogin()->format('d/m/Y - H:i') : 'jamais connecté' ?></td>
        <td><?= $user->getErreursAuthentification() ?></td>
        <td>
          <div class="etat_div">
            <?php if ($user->isBlacklisted()) { ?>
              <a class="decorationNone" href="#" title="Déblacklister le compte" onclick="OpenUserPopup('deblacklistageUser', '<?= $user->getEmail() ?>', '<?= $i ?>')">Déblacklister</a>
            <?php } else if ($user->isActivated()) { ?>
              <a class="decorationNone" href="#" title="Désactiver le compte" onclick="OpenUserPopup('desactivationUser', '<?= $user->getEmail() ?>', '<?= $i ?>')">✅</a>
            <?php } else { ?>
              <a class="decorationNone" href="#" title="Activer le compte" onclick="OpenUserPopup('activationUser', '<?= $user->getEmail() ?>', '<?= $i ?>')">❌</a>
            <?php } ?>
          </div>
        </td>
        <td><?= $user->getRole() ?></td>
        <td class="ip_users">
          <?php foreach ($user->getIp()->all() as $ip) {?>
            <a href="https://www.g-force.ca/hebergement/ip-whois?ip=<?= $ip->getIp() ?>" target="_blank" <?= Services\Functions::ObjectInArray($ip, $ipBlacklistees, 'ip') ? 'style="text-decoration: line-through"' : '' ?>><?= $ip->getIp() ?></a><br>
          <?php } ?>
        </td>


        <td>
          <img src="/assets/images/signalement.svg" title="Blacklister cet utilisateur" onclick="OpenUserPopup('blacklistageUser', '<?= $user->getEmail() ?>', '<?= $i ?>')">

          <img src="/assets/images/Corbeille.svg" title="Supprimer l'utilisateur" onclick="OpenUserPopup('suppressionUser','<?= $user->getEmail() ?>', '<?= $i ?>')">
        </td>
      </tr>
  <?php }
  } ?>
</table>