<div id="fenetrepopup" class="fenetrepopup displayNone">
  <div id="Popup" class="Popup">
    <span class="labelretour" onclick="FermerPopup()">
      <img src="/assets/images/fermer.svg" title="fermer">
    </span>
    <div id="ContenuPopupLiens">
      <input id="lien_courant" type="hidden" name="lien" value="">
      <div id="ajouter_motdepasse" class="section center displayNone">
        <h3>Ajouter un mot de passe :</h3>
        <form>
          <label for="ajout_password_lien">Mot de passe : </label>
          <input id="ajout_password_lien" class="champ_modifiable" type="password" name="ajout_password_lien"><br>
          <label for="ajout_password_lien_confirm">Confirmer le mot de passe : </label>
          <input id="ajout_password_lien_confirm" class="champ_modifiable" type="password" name="ajout_password_lien_confirm"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="mot_de_passe_lien('ajout_password_lien','ajout_password_lien_confirm')" value="Enregistrer">
        </form>
      </div>
      <div id="modifier_motdepasse" class="section center displayNone">
        <h3>Modifier/supprimer le mot de passe :</h3>
        <div id="info_motdepasse">
          <p>Laisser les champs vide pour supprimer le mot de passe.</p>
        </div>
        <form>
          <label for="password_lien">Nouveau mot de passe : </label>
          <input id="password_lien" class="champ_modifiable" type="password" name="password_lien"><br>
          <label for="password_lien">Confirmer le mot de passe : </label>
          <input id="password_lien_confirm" class="champ_modifiable" type="password" name="password_lien_confirm"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="mot_de_passe_lien('password_lien','password_lien_confirm')" value="Enregistrer">
        </form>
      </div>
      <div id="ajouter_expiration" class="section center displayNone">
        <h3>Ajouter une date d'expiration :</h3>
        <form>
          <input id="ajout_datetime" type="datetime-local" class="champ_modifiable" name="datexpi"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="date_expiration('ajout_datetime')" value="Enregistrer">
        </form>
      </div>
      <div id="modifier_expiration" class="section center displayNone">
        <h3>Modifier/supprimer la date d'expiration :</h3>
        <form>
          <input id="mod_datetime" type="datetime-local" class="champ_modifiable" name="datexpi"><br>
          <input id="supprimer_expi" type="checkbox" name="supprimer_expi">
          <label for="supprimer_expi">Supprimer la date d'expiration</label><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="date_expiration('mod_datetime', true)" value="Enregistrer">
        </form>
      </div>
      <div id="suppression" class="section center displayNone">
        <h3>Êtes-vous sûr de vouloir supprimer ce lien ?</h3>
        <form>
          <input id="confirm_suppression" type="checkbox" name="suppression">
          <label for="confirm_suppression">Supprimer définitivement</label><br>
          <input type="button" class="bouton center" onclick="supprimer_lien()" value="Supprimer le lien">
        </form>
      </div>
    </div>

    <div id="ContenuPopupUsers">
      <input id="user_adminAction" type="hidden" name="user" value="">
      <div id="desactivationUser" class="section center displayNone">
        <h3>Êtes-vous certain de vouloir désactiver cet utilisateur ?</h3>
        <form>
          <input id="confirm_desactivationUser" type="checkbox" name="confirm_desactivationUser">
          <label for="confirm_desactivationUser">Désactiver le compte</label><br>
          <input type="button" class="bouton center" onclick="desactiver_utilisateur()" value="Enregistrer">
        </form>
      </div>
      <div id="activationUser" class="section center displayNone">
        <h3>Êtes-vous certain de vouloir activer cet utilisateur ?</h3>
        <form>
          <input id="confirm_activationUser" type="checkbox" name="confirm_activationUser">
          <label for="confirm_activationUser">Activer le compte</label><br>
          <input type="button" class="bouton center" onclick="activer_utilisateur()" value="Enregistrer">
        </form>
      </div>
      <div id="blacklistageUser" class="section center displayNone">
        <h3>Êtes-vous certain de vouloir blacklister cet utilisateur ?</h3>
        <form>
          <input id="confirm_blacklistage" type="checkbox" name="confirm_blacklistage">
          <label for="confirm_blacklistage">Blacklister</label><br>
          <input type="button" class="bouton center" onclick="blacklister_utilisateur()" value="Enregistrer">
        </form>
      </div>
      <div id="deblacklistageUser" class="section center displayNone">
        <h3>Êtes-vous certain de vouloir déblacklister cet utilisateur ?</h3>
        <form>
          <input id="confirm_deblacklistage" type="checkbox" name="confirm_deblacklistage">
          <label for="confirm_deblacklistage">Déblacklister</label><br>
          <input type="button" class="bouton center" onclick="deblacklister_utilisateur()" value="Enregistrer">
        </form>
      </div>
      <div id="suppressionUser" class="section center displayNone">
        <h3>Êtes-vous sûr de vouloir supprimer cet utilisateur ?</h3>
        <form>
          <input id="confirm_suppression_user" type="checkbox" name="confirm_suppression_user">
          <label for="confirm_suppression_user">Supprimer définitivement</label><br>
          <input id="suppr_all_liens" type="checkbox" name="suppr_all_liens">
          <label for="suppr_all_liens">Supprimer tous les liens de cet utilisateur</label><br>
          <input type="button" class="bouton center" onclick="supprimer_utilisateur()" value="Supprimer l'utilisateur">
        </form>
      </div>
    </div>
    <div id="suppressionIpBl" class="section center displayNone">
      <h3>Êtes-vous sûr de vouloir retirer cette ip de la liste noire ?</h3>
      <form>
        <input id="confirm_suppressionIpBl" type="checkbox" name="confirm_suppressionIpBl">
        <label for="confirm_suppressionIpBl">Supprimer définitivement</label><br>
        <input type="button" class="bouton center" onclick="suppressionIpBl()" value="Retirer l'IP">
      </form>
    </div>
    <div id="suppressionUrlBl" class="section center displayNone">
      <h3>Êtes-vous sûr de vouloir retirer ce domaine de la liste noire ?</h3>
      <form>
        <input id="confirm_suppressionUrlBl" type="checkbox" name="confirm_suppressionUrlBl">
        <label for="confirm_suppressionUrlBl">Supprimer définitivement</label><br>
        <input type="button" class="bouton center" onclick="suppressionUrlBl()" value="Retirer le domaine">
      </form>
    </div>
    <div id="validationIpBl" class="section center displayNone">
      <h3>Validez-vous la pertinence du blacklistage de cette IP ?</h3>
      <form>
        <input id="confirm_validationIpBl" type="checkbox" name="confirm_validationIpBl">
        <label for="confirm_validationIpBl">Oui, je valide</label><br>
        <input type="button" class="bouton center" onclick="validationIpBl()" value="Vérifier l'IP">
      </form>
    </div>
    <div id="message_retour_api" class="message_sauvegarde"></div>
  </div>

</div>