<h2>Statistiques des liens</h2>
<div class="selecteur-periode">
  <label for="graphic-periode" class="graphic-periode">Afficher les statistiques </label>
  <select name="periode" id="select-graphic-periode">
    <option value="mois" selected>sur les 12 derniers mois</option>
    <option value="annees">par année</option>
    <option value="jours">sur les 31 derniers jours</option>
  </select>
</div>
<div class="graphics">
  <div class="graphic-liens">
    <h3>Nombre de liens créés</h3>
    <div class="graphic-liens-annees">
      <?php
      if ($stats->NbLiensCreesParPeriode["annees"] !== [] && max($stats->NbLiensCreesParPeriode["annees"]) !== 0) {
        $max = max($stats->NbLiensCreesParPeriode["annees"]);
      ?>
        <div class="annee legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbLiensCreesParPeriode["annees"] as $annee => $valeur) { ?>
          <div class="annee" style="width:calc(100% / <?= count($stats->NbLiensCreesParPeriode["annees"]) ?>)">
            <div class="valeur" style="height: <?= $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
    <div class="graphic-liens-mois">
      <?php
      if ($stats->NbLiensCreesParPeriode["mois"] !== [] && max($stats->NbLiensCreesParPeriode["mois"]) !== 0) {
        $max = max($stats->NbLiensCreesParPeriode["mois"]);
      ?>
        <div class="mois legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbLiensCreesParPeriode["mois"] as $annee => $valeur) { ?>
          <div class="mois">
            <div class="valeur" style="height: <?= $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
    <div class="graphic-liens-jours">
      <?php
      if ($stats->NbLiensCreesParPeriode["jours"] !== [] && max($stats->NbLiensCreesParPeriode["jours"]) !== 0) {
        $max = max($stats->NbLiensCreesParPeriode["jours"]);
      ?>
        <div class="jour legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbLiensCreesParPeriode["jours"] as $annee => $valeur) { ?>
          <div class="jour">
            <div class="valeur" style="height: <?= $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
  </div>
  <div class="graphic-users">
    <h3>Nombre d'utilisateurs enregistrés</h3>
    <div class="graphic-users-annees">
      <?php
      if ($stats->NbUsersCreesParPeriode["annees"] !== [] && max($stats->NbUsersCreesParPeriode["annees"]) !== 0) {
        $max = max($stats->NbUsersCreesParPeriode["annees"]);
      ?>
        <div class="annee legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbUsersCreesParPeriode["annees"] as $annee => $valeur) { ?>
          <div class="annee" style="width:calc(100% / <?= count($stats->NbUsersCreesParPeriode["annees"]) ?>)">
            <div class="valeur" style="height: <?= $max === 0 ? 0 : $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
    <div class="graphic-users-mois">
      <?php
      if ($stats->NbUsersCreesParPeriode["mois"] !== [] && max($stats->NbUsersCreesParPeriode["mois"]) !== 0) {
        $max = max($stats->NbUsersCreesParPeriode["mois"]);
      ?>
        <div class="mois legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbUsersCreesParPeriode["mois"] as $annee => $valeur) { ?>
          <div class="mois">
            <div class="valeur" style="height: <?= $max === 0 ? 0 : $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
    <div class="graphic-users-jours">
      <?php
      if ($stats->NbUsersCreesParPeriode["jours"] !== [] && max($stats->NbUsersCreesParPeriode["jours"]) !== 0) {
        $max = max($stats->NbUsersCreesParPeriode["jours"]);
      ?>
        <div class="jour legende">
          <span class="echelon"><?= $max ?></span>
          <span class="echelon"><?= $max / 2 ?></span>
          <span class="echelon">0</span>
        </div>
        <?php foreach ($stats->NbUsersCreesParPeriode["jours"] as $annee => $valeur) { ?>
          <div class="jour">
            <div class="valeur" style="height: <?= $max === 0 ? 0 : $valeur * 200 / $max ?>px" title="<?= $valeur ?>"></div>
            <div class="clef"><?= $annee ?></div>
          </div>
        <?php }
      } else { ?>
        <p class="center vertical-center">Aucune statistique disponible</p>
      <?php } ?>
    </div>
  </div>
</div>
<div class="resume-stats">
  <div class="stat-card" onclick="afficher_partie('/admin/liens')">
    <span><?= $stats->nb_liens ?></span>
    <p>Nombre total de liens</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/utilisateurs')">
    <span><?= $stats->nb_users ?></span>
    <p>Nombre d'utilisateurs enregistrés</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/liens')">
    <span><?= $stats->MoyenneNbLiensParUser ?></span>
    <p>Moyenne de liens par utilisateur</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/liens')">
    <span><?= $stats->MoyenneNbClicsParLien ?></span>
    <p>Moyenne de clics par lien</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/liens')">
    <span><?= $stats->nb_liensAvecDatePeremption ?></span>
    <p>Nombre de liens avec une date d'expiration</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/liens')">
    <span><?= $stats->nb_liensAvecMdp ?></span>
    <p>Nombre de liens avec un mot de passe</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/blacklists/domaines')">
    <span><?= $stats->nb_urlBlacklistees ?></span>
    <p>Nombre d'urls blacklistées</p>
  </div>
  <div class="stat-card" onclick="afficher_partie('/admin/blacklists/ips')">
    <span><?= $stats->nb_ipBlacklistees ?></span>
    <p>Nombre d'IPs blacklistées</p>
  </div>
</div>


<script>
  let select = document.getElementById('select-graphic-periode');
  select.onchange = function() {
    document.querySelectorAll('[class^="graphic-liens-"]').forEach(el => el.style.display = 'none');
    document.querySelectorAll('[class^="graphic-users-"]').forEach(el => el.style.display = 'none');
    document.querySelector('.graphic-liens-' + select.value).style.display = 'flex';
    document.querySelector('.graphic-users-' + select.value).style.display = 'flex';
  }
</script>