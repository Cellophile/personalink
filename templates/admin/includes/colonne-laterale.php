<a href="<?= URLSITE ?>" class="btn-retour-accueil">Retour à l'accueil</a>
<h2>Bienvenue, <?= $currentUser->getPrenom() ?> !</h2>
<ul class="puce">
  <li><a href="<?= URLSITE ?>dashboard" class="btn-retour-accueil li-retour">Tableau de bord</a></li>
  <li class="li-statistiques" onclick="afficher_partie('/admin/statistiques')">Statistiques</li>
  <li class="li-liens" onclick="afficher_partie('/admin/liens')">Liens</li>
  <li class="li-utilisateurs" onclick="afficher_partie('/admin/utilisateurs')">Utilisateurs</li>
  <li class="li-blacklists" onclick="afficher_partie('/admin/blacklists')">Listes noires</li>
  <li class="li-parametres" onclick="afficher_partie('/admin/parametres')">Paramètres</li>
</ul>