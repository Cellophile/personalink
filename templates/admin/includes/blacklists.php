<h2>Listes noires</h2>
<div class="menu_listes_noires">
  <button class="bouton" onclick="ouvresection('ips')">Ips blacklistées</button>
  <button class="bouton" onclick="ouvresection('domaines')">domaines blacklistés</button>
</div>
<hr>
<div id="ips">
  <div class="flex">
    <input id="nouvelleIpBlacklist" type="text" name="ip" placeholder="Adresse IP à blacklister" class="champ_modifiable">
    <input type="button" value="Ajouter" class="bouton" onclick="ajouterIpBlacklist()">
    <span id="message_ajout_ip"></span>
  </div>
  <table class="tableau_ips_blacklistees">
    <tr class="titre_colonne">
      <th scope="col">IPs</th>
      <th scope="col">Date de Blacklistage</th>
      <th scope="col">État</th>
      <th scope="col">Outils</th>
    </tr>
    <?php
    if (empty($ipBlacklistees)) { ?>
      <tr>
        <th colspan="4">Aucune IP en liste noire </th>
      </tr>
      <?php
    } else {
      $i = 0;
      foreach ($ipBlacklistees as $IpBlacklistee) {
        $i++;
      ?>
        <tr>
          <td class="titre_ligne"><?= $IpBlacklistee->getIp() ?></td>
          <td><?= $IpBlacklistee->getDateBlacklistage()->format('d/m/Y - H:i') ?></td>
          <td class="etat"><?= $IpBlacklistee->getEtat() == "Vérifiée" ? '✅' : '⚠️' ?> <?= $IpBlacklistee->getEtat() ?></td>
          <td>
            <?php if ($IpBlacklistee->getEtat() == "À vérifier") { ?>
              <a id="valider_ip<?= $i; ?>" href="#" onclick="OpenPopup('validationIpBl','<?= $IpBlacklistee->getIp() ?>')"><img src="/assets/images/modifier.svg" title="Vérifier l'IP"></a>
            <?php } ?>
            <a id="supprimer_ip<?= $i; ?>" href="#" onclick="OpenPopup('suppressionIpBl','<?= $IpBlacklistee->getIp() ?>')"><img src="/assets/images/Corbeille.svg" title="Supprimer le domaine"></a>
          </td>
        </tr>
    <?php
      }
    }
    ?>
  </table>
</div>
<div id="domaines" class="displayNone">
  <div class="flex">
    <input id="nouvelleUrlBlacklist" type="text" name="url" placeholder="Domaine à blacklister" class="champ_modifiable">
    <input type="button" value="Ajouter" class="bouton" onclick="ajouterUrlBlacklist()">
    <span id="message_ajout_url"></span>
  </div>
  <table class="tableau_domaines_blacklistes">
    <tr class="titre_colonne">
      <th scope="col">domaine</th>
      <th scope="col">Outils</th>
    </tr>
    <?php
    if (empty($urlBlacklistees)) { ?>
      <tr>
        <th colspan="4">Aucun domaine en liste noire </th>
      </tr>
      <?php
    } else {
      $i = 0;
      foreach ($urlBlacklistees as $urlBlacklistee) {
        $i++;
      ?>
        <tr>
          <td class="titre_ligne"><?= $urlBlacklistee->getUrl() ?></td>
          <td>
            <a id="supprimer_domaine<?= $i; ?>" href="#" onclick="OpenPopup('suppressionUrlBl','<?= $urlBlacklistee->getUrl() ?>')">
              <img src="/assets/images/Corbeille.svg" title="Supprimer le domaine">
            </a>
          </td>
        </tr>
    <?php
      }
    }
    ?>
  </table>
</div>

<script>
  function ajouterIpBlacklist() {
    let nouvelleIpBlacklist = document.getElementById('nouvelleIpBlacklist').value;
    if (nouvelleIpBlacklist != "") {
      let r = new XMLHttpRequest();
      r.onreadystatechange = function() {
        if (r.readyState == 4 && r.status == 200) {
          document.getElementById('message_ajout_ip').innerHTML = "L'ip a bien été ajoutée à la liste noire";
          document.getElementById('message_ajout_ip').classList.remove('reponse-info');
          document.getElementById('message_ajout_ip').classList.add('reponse-positive');
          document.getElementById('message_ajout_ip').style.display = "block";
          disparitionMessage2s('message_ajout_ip');
          nouvelleIpBlacklist = JSON.parse(r.responseText);
          ModifyTableauAfterAjout(nouvelleIpBlacklist, "tableau_ips_blacklistees");
          return true;
        } else if (r.readyState == 4 && r.status !== 200) {
          try {
            document.getElementById('message_ajout_ip').innerHTML = "Une erreur est survenue : " + JSON.parse(r.responseText);
            document.getElementById('message_ajout_ip').classList.remove('reponse-info');
            document.getElementById('message_ajout_ip').classList.add('reponse-negative');
            document.getElementById('message_ajout_ip').style.display = "block";
            disparitionMessage2s('message_ajout_ip');
          } catch (e) {
            document.getElementById('message_ajout_ip').innerHTML = "Une erreur est survenue, veuillez reessayer";
            document.getElementById('message_ajout_ip').classList.remove('reponse-info');
            document.getElementById('message_ajout_ip').classList.add('reponse-negative');
            document.getElementById('message_ajout_ip').style.display = "block";
            disparitionMessage2s('message_ajout_ip');
          }
          return false;
        } else {
          document.getElementById('message_ajout_ip').innerHTML = "Veuillez patienter...";
          document.getElementById('message_ajout_ip').classList.add('reponse-info');
          document.getElementById('message_ajout_ip').style.display = "block";
        }
      };
      r.open('POST', API_URL + '/blacklists/addip', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send('ip=' + encodeURIComponent(nouvelleIpBlacklist));
    } else {
      document.getElementById('message_ajout_ip').innerHTML = "Veuillez entrer une adresse IP";
      document.getElementById('message_ajout_ip').classList.add('reponse-negative');
      document.getElementById('message_ajout_ip').style.display = "block";
      return false;
    }
  }

  function ajouterUrlBlacklist() {
    var nouvelleUrlBlacklist = document.getElementById('nouvelleUrlBlacklist').value;
    if (nouvelleUrlBlacklist != "") {
      let r = new XMLHttpRequest();
      r.onreadystatechange = function() {
        if (r.readyState == 4 && r.status == 200) {
          document.getElementById('message_ajout_url').innerHTML = "Le domaine a bien été ajouté à la liste noire";
          document.getElementById('message_ajout_url').classList.remove('reponse-info');
          document.getElementById('message_ajout_url').classList.add('reponse-positive');
          document.getElementById('message_ajout_url').style.display = "block";
          disparitionMessage2s('message_ajout_url');
          nouvelleUrlBlacklist = JSON.parse(r.responseText);
          ModifyTableauAfterAjout(nouvelleUrlBlacklist, "tableau_domaines_blacklistes");
          return true;
        } else if (r.readyState == 4 && r.status !== 200) {
          try {
            document.getElementById('message_ajout_url').innerHTML = "Une erreur est survenue : " + JSON.parse(r.responseText);
            document.getElementById('message_ajout_url').classList.remove('reponse-info');
            document.getElementById('message_ajout_url').classList.add('reponse-negative');
            document.getElementById('message_ajout_url').style.display = "block";
            disparitionMessage2s('message_ajout_url');
          } catch (e) {
            document.getElementById('message_ajout_url').innerHTML = "Une erreur est survenue, veuillez reessayer";
            document.getElementById('message_ajout_url').classList.remove('reponse-info');
            document.getElementById('message_ajout_url').classList.add('reponse-negative');
            document.getElementById('message_ajout_url').style.display = "block";
            disparitionMessage2s('message_ajout_url');
          }
          return false;
        } else {
          document.getElementById('message_ajout_url').innerHTML = "Veuillez patienter...";
          document.getElementById('message_ajout_url').classList.add('reponse-info');
          document.getElementById('message_ajout_url').style.display = "block";
        }
      };
      r.open('POST', API_URL + '/blacklists/addurl', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send('url=' + encodeURIComponent(nouvelleUrlBlacklist));
    } else {
      document.getElementById('message_ajout_url').innerHTML = "Veuillez entrer un domaine";
      document.getElementById('message_ajout_url').classList.add('reponse-negative');
      document.getElementById('message_ajout_url').style.display = "block";
      return false;
    }
  }

  function ModifyTableauAfterAjout(element, table) {
    let ligne = '';
    let tableau = document.querySelector('table.' + table + ' tbody');
    let i = tableau.children.length - 1;
    if (table == "tableau_ips_blacklistees") {
      lign = tableau
      ligne = `<tr>
          <td class="titre_ligne">${element.ip}</td>
          <td>${element.dateBlacklistage}</td>
          <td>${element.etat == "Vérifiée" ? "✅ Vérifiée" : "⚠️ À vérifier"}</td>
          <td>
            <a id="supprimer_ip<?= $i; ?>" href="#" onclick="OpenPopup('suppressionIpBl','${element.ip}')"><img src="/assets/images/Corbeille.svg" title="Supprimer le domaine"></a>
          </td>
        </tr>`
    } else if (table == "tableau_domaines_blacklistes") {
      ligne = `<tr>
          <td class="titre_ligne">${element.url}</td>
          <td>
            <a id="supprimer_domaine${i}" href="#" onclick="OpenPopup('suppressionUrlBl','${element.url}')">
              <img src="/assets/images/Corbeille.svg" title="Supprimer le domaine">
            </a>
          </td>
        </tr>`
    }
    tableau.innerHTML += ligne;
  }

  function disparitionMessage2s(section) {
    setTimeout(function() {
      document.getElementById(section).innerHTML = "";
      document.getElementById(section).style.display = "none";
      document.getElementById(section).classList.remove('reponse-positive', 'reponse-negative');
    }, 2000);
  }

  function suppressionIpBl() {
    let ip = document.getElementById('lien_courant').value;
    if (ip != "" && document.getElementById('confirm_suppressionIpBl').checked) {
      let r = new XMLHttpRequest();
      r.onreadystatechange = function() {
        if (r.readyState == 4 && r.status == 200) {
          message_positif("L'adresse IP a bien été supprimée de la liste noire");
          let q = SelectLigne(ip, "tableau_ips_blacklistees");
          ModifyRowAfterSuppression(q, "tableau_ips_blacklistees");
        } else {
          try {
            message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
          } catch (e) {
            message_negatif('Quelque chose s\'est mal passé...');
          }
        }
      };
      r.open('POST', API_URL + '/blacklists/removeip', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send('ip=' + encodeURIComponent(ip));
    } else {
      message_negatif('Veuillez confirmer la suppression de l\'adresse IP');
      return false;
    }

  }

  function suppressionUrlBl() {
    let url = document.getElementById('lien_courant').value;
    if (url != "" && document.getElementById('confirm_suppressionUrlBl').checked) {
      let r = new XMLHttpRequest();
      r.onreadystatechange = function() {
        if (r.readyState == 4 && r.status == 200) {
          message_positif("Le domaine a bien été supprimé de la liste noire");
          let q = SelectLigne(url, "tableau_domaines_blacklistes");
          ModifyRowAfterSuppression(q, "tableau_domaines_blacklistes");
        } else {
          try {
            message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
          } catch (e) {
            message_negatif('Quelque chose s\'est mal passé...');
          }
        }
      };
      r.open('POST', API_URL + '/blacklists/removeurl', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send('url=' + encodeURIComponent(url));
    } else {
      message_negatif('Veuillez confirmer la suppression du domaine');
      return false;
    }
  }

  function validationIpBl() {
    let ip = document.getElementById('lien_courant').value;
    if (ip != "") {
      let r = new XMLHttpRequest();
      r.onreadystatechange = function() {
        if (r.readyState == 4 && r.status == 200) {
          message_positif("L'adresse IP a bien été vérifiée");
          let q = SelectLigne(ip, "tableau_ips_blacklistees");
          ModifyLigneAfterVerification(q);
        } else {
          try {
            message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
          } catch (e) {
            message_negatif('Quelque chose s\'est mal passé...');
          }
        }
      };
      r.open('POST', API_URL + '/blacklists/validateip', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send('ip=' + encodeURIComponent(ip));
    }
  }

  function ModifyLigneAfterVerification(q) {
    q.row.querySelector('.etat').innerHTML = '✅ Vérifiée';
    q.row.querySelector('#valider_ip' + q.nbRow).remove();
  }
</script>