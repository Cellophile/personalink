<script defer>
  afficher_partie(window.location.pathname);
  ouvresection(window.location.pathname);

  function afficher_partie(UrlPage) {
    let page;
    let split;
    let partie = '';
    if (UrlPage == '/admin' || UrlPage == '/admin/') {
      page = 'statistiques';
      split = ['statistiques', null];
    } else {
      UrlPage = UrlPage.split('/');
      if (UrlPage[0] == '') {
        UrlPage.shift();
      }
      if (UrlPage[1] != undefined) {
        split = UrlPage[1].split('?');
        page = split[0];
      }
      if (UrlPage[2] != undefined) {
        split = UrlPage[2].split('?');
        partie = split[0];
      }
    }
    let section_visible = document.getElementById(page);
    let li_active = document.querySelector('.li-' + page);
    let section = [];
    let li = [];
    section[1] = document.getElementById('statistiques');
    section[2] = document.getElementById('liens');
    section[3] = document.getElementById('utilisateurs');
    section[4] = document.getElementById('blacklists');
    section[5] = document.getElementById('parametres');
    li[1] = document.querySelector('.li-statistiques');
    li[2] = document.querySelector('.li-liens');
    li[3] = document.querySelector('.li-utilisateurs');
    li[4] = document.querySelector('.li-blacklists');
    li[5] = document.querySelector('.li-parametres');
    for (let i = 1; i < 6; i++) {
      section[i].classList.add("displayNone");
      li[i].classList.remove('active');
      }
      section_visible.classList.remove("displayNone");
    li_active.classList.add('active');

    let get = (split[1] == null) ? '' : '?' + split[1];
    history.pushState(null, null, '/admin/' + page + "/" + partie + get);

    ouvresection(UrlPage);
  }

  function ouvresection(section) {
    if (section == '/admin/blacklists/ips' || section == '/admin/blacklists' || section == 'ips') {
      section = 'ips';
      document.getElementById('ips').classList.remove('displayNone');
      document.getElementById('domaines').classList.add('displayNone');
      history.pushState(null, null, '/admin/blacklists/ips');

    } else if (section == '/admin/blacklists/domaines' || section == 'domaines') {
      section = 'domaines';
      document.getElementById('ips').classList.add('displayNone');
      document.getElementById('domaines').classList.remove('displayNone');
      history.pushState(null, null, '/admin/blacklists/domaines');
    }
  }

  function montrer_pwd(pwd, txt, btn1, btn2) {
    pwd = document.getElementById(pwd);
    txt = document.getElementById(txt);
    btn1 = document.getElementById(btn1);
    btn2 = document.getElementById(btn2);
    pwd.style.display = 'none';
    txt.style.display = 'block';
    btn1.style.display = 'none';
    btn2.style.display = 'block';
  }

  function cacher_pwd(pwd, txt, btn1, btn2) {
    pwd = document.getElementById(pwd);
    txt = document.getElementById(txt);
    btn1 = document.getElementById(btn1);
    btn2 = document.getElementById(btn2);
    pwd.style.display = 'block';
    txt.style.display = 'none';
    btn1.style.display = 'block';
    btn2.style.display = 'none';
  }

  function copier_lien_court(lien, validation) {
    let a = document.getElementById(validation);
    // Si le lien n'est pas vide
    if (lien.length) {
      // si navigator.clipboard fonctionne :
      if (navigator.clipboard && window.isSecureContext) {
        // navigator clipboard api method'
        navigator.clipboard.writeText(lien).then(() => {
          // Prévenir que le texte est copié :
          txt_copie_ok(a);
        })
      } else {
        // text area method
        let textArea = document.createElement("textarea");
        textArea.value = lien;
        // make the textarea out of viewport
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        return new Promise((res, rej) => {
          // here the magic happens
          document.execCommand('copy') ? res() : rej();
          textArea.remove();
          // Prévenir que le texte est copié :
          txt_copie_ok(a);
        });
      }
    }
  }

  function txt_copie_ok(a) {
    a.style.display = 'block';
    a.style.opacity = 1;
    a.style.visibility = 'visible';
    setTimeout(function() {
      a.style.display = 'none';
      a.style.opacity = 0;
      a.style.visibility = 'hidden';
    }, 3000);
  }

  function OpenPopup(section, lien, info1) {
    let affichage = document.getElementById('fenetrepopup');
    affichage.classList.remove('displayNone');

    section = document.getElementById(section);
    section.classList.remove('displayNone');

    let input_lien = document.getElementById('lien_courant');
    input_lien.value = lien;
  }

  function OpenUserPopup(section, User, info1) {
    let affichage = document.getElementById('fenetrepopup');
    affichage.classList.remove('displayNone');

    section = document.getElementById(section);
    section.classList.remove('displayNone');

    let user = document.getElementById('user_adminAction');
    user.value = User;
  }


  function FermerPopup() {
    let affichage = document.getElementById('fenetrepopup');
    affichage.classList.add('displayNone');
    let sections = document.getElementsByClassName('section');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.add('displayNone');
    }
  }

  document.addEventListener('DOMContentLoaded', () => {
    let affichage = document.getElementById('fenetrepopup');
    window.addEventListener('click', (event) => {
      if (event.target === affichage) {
        FermerPopup()
      }
    });
  });

  function message_positif(message) {
    let retour = document.getElementById('message_retour_api');
    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "green";
    retour.style.backgroundColor = '#e7fae7';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 2000);
    setTimeout(function() {
      FermerPopup();
      decocherCases();
    }, 2500);
  }

  function message_negatif(message) {
    let retour = document.getElementById('message_retour_api');

    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "red";
    retour.style.backgroundColor = '#ffdada';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 3000);
  }

  function message_info(message) {
    let retour = document.getElementById('message_retour_api');

    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "blue";
    retour.style.backgroundColor = '#e7f7ff';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 3000);
  }

  function supprimer_lien(lien) {
    lien = document.getElementById('lien_courant').value;

    if (document.getElementById('confirm_suppression').checked == false) {
      message_negatif('Veuillez cocher la case.')
    } else {

      let r = new XMLHttpRequest();

      let modif_lien = 'upersok=' + encodeURIComponent(lien);

      r.onreadystatechange = function() {
        if (r.status === 202) {
          message_positif('Le lien a bien été supprimé');
          let q = SelectLigne(lien, "tableau_liens");
          ModifyRowAfterSuppression(q, "tableau_liens");
        } else {
          try {
            message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
          } catch (e) {
            message_negatif('Quelque chose s\'est mal passé... ');
          }
          return false;
        }
      };
      r.open('POST', API_URL + '/lien/delete', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send(modif_lien);
    }
  }

  function date_expiration(dateTime, suppr = false) {
    let lien = document.getElementById('lien_courant').value;
    dateTime = document.getElementById(dateTime).value;
    suppr = suppr ? document.getElementById('supprimer_expi').checked : false;
    if (suppr === true) {
      dateTime = null;
    } else {
      if (!dateTime) {
        message_negatif('Veuillez renseigner une date d\'expiration complète (avec l\'heure).')
        return false;
      }
    }
    let modif_lien = 'upersok=' + encodeURIComponent(lien) + '&datexpi=' + encodeURIComponent(dateTime);


    let r = new XMLHttpRequest();

    r.onreadystatechange = function() {
      if (r.status === 202) {
        message_positif('La date d\'expiration a bien été modifiée');
        let q = SelectLigne(lien, "tableau_liens");
        ModifyRowAfterExpirationChange(q, dateTime);
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
        return false;
      }
    };
    r.open('POST', API_URL + '/lien/update', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send(modif_lien);
  }

  function mot_de_passe_lien(mdp, mdp1) {
    let lien = document.getElementById('lien_courant').value;
    mdp = document.getElementById(mdp).value;
    mdp1 = document.getElementById(mdp1).value;
    if (mdp == mdp1) {
      var modif_lien = 'upersok=' + encodeURIComponent(lien) + '&mdp=' + encodeURIComponent(mdp);
    } else {
      message_negatif('Les mots de passes ne sont pas identiques.')
      throw new Error('Not identical password');
    }

    let r = new XMLHttpRequest();

    r.onreadystatechange = function() {
      if (r.status === 202) {
        message_positif('Le mot de passe a bien été modifié');
        let q = SelectLigne(lien, "tableau_liens");
        ModifyRowAfterPasswordChange(q, mdp);
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
        return false;
      }
    };
    r.open('POST', API_URL + '/lien/update', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send(modif_lien);
  }

  function SelectLigne(recherche, table) {
    let lignes = document.querySelectorAll('table.' + table + ' tr');

    for (let i = 0; i < lignes.length; i++) {
      let titreLigne = lignes[i].querySelector('.titre_ligne');

      if (titreLigne && titreLigne.textContent.trim() === recherche) {
        let pwdDiv = lignes[i].querySelector('.pwd_div');
        let etatDiv = lignes[i].querySelector('.etat_div');

        if (pwdDiv) {
          return {
            row: lignes[i],
            nbRow: i,
            pwdDiv: pwdDiv
          };
        } else if (etatDiv) {
          return {
            row: lignes[i],
            nbRow: i,
            etatDiv: etatDiv
          };
        } else {
          return {
            row: lignes[i],
            nbRow: i
          };
        }
      }
    }
    return null;
  }

  function addPasswordContent(nbRow, pwd) {
    return `
    <input id="pwd${nbRow}" class="pwd_display cacher_pwd" type="password" name="cacher_pwd" autocomplete="off" readonly="" value="${pwd}">
    <img id="btn_voir${nbRow}" class="img_voir_pwd" src="/assets/images/voir.svg" onclick="montrer_pwd('pwd${nbRow}','txt${nbRow}','btn_voir${nbRow}','btn_cacher${nbRow}')">
    <input id="txt${nbRow}" class="pwd_display montrer_pwd" type="text" name="montrer_pwd" autocomplete="off" readonly="" value="${pwd}">
    <img id="btn_cacher${nbRow}" class="img_cacher_pwd" src="/assets/images/cacher.svg" onclick="cacher_pwd('pwd${nbRow}','txt${nbRow}','btn_voir${nbRow}','btn_cacher${nbRow}')">
    `;
  }


  function ModifyRowAfterPasswordChange(q, mdp) {
    let a = q.row.querySelector('#motdepasse' + q.nbRow);
    let img = q.row.querySelector('#motdepasse' + q.nbRow + ' img');
    let lien = q.row.querySelector('.titre_ligne').textContent;
    if (mdp.length > 0) {
      q.pwdDiv.innerHTML = addPasswordContent(q.nbRow, mdp);
      img.src = '/assets/images/cadenas fermé.svg';
      a.onclick = function() {
        OpenPopup('modifier_motdepasse', lien, mdp)
      };
    } else {
      q.pwdDiv.innerHTML = '';
      img.src = '/assets/images/cadenas ouvert.svg';
      a.onclick = function() {
        OpenPopup('ajouter_motdepasse', lien, null)
      };
    }
  }

  function ModifyRowAfterExpirationChange(q, dateTime) {
    let lien = q.row.querySelector('.titre_ligne').textContent;
    let datexpiDiv = q.row.querySelector('#date_expiration' + q.nbRow);
    let img = q.row.querySelector('#expiration' + q.nbRow + ' img');
    let a = q.row.querySelector('#expiration' + q.nbRow);
    if (dateTime !== null && dateTime.length > 0) {
      dateTime = new Date(dateTime);
      datexpiDiv.innerHTML = new Intl.DateTimeFormat('fr-FR', {
        dateStyle: 'short',
        timeStyle: 'short'
      }).format(dateTime);
      futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 15);
      if (futureDate.getTime() > dateTime.getTime()) {
        img.src = '/assets/images/expiration proche.svg';
      } else {
        img.src = '/assets/images/expiration active.svg';
      }
      a.onclick = function() {
        OpenPopup('modifier_expiration', lien, dateTime)
      };
    } else {
      a.onclick = function() {
        OpenPopup('ajouter_expiration', lien, null)
      };
      datexpiDiv.innerHTML = 'jamais';
      img.src = '/assets/images/expiration inactive.svg';
    }
  }

  function ModifyRowAfterSuppression(q, table) {
    let tableau = document.querySelector('table.' + table + ' tbody');
    tableau.removeChild(q.row);
  }

  function desactiver_utilisateur() {
    if (!document.getElementById('confirm_desactivationUser').checked) {
      message_negatif('Veuillez confirmer la désactivation de l\'utilisateur');
      return false;
    }
    let user = document.getElementById('user_adminAction').value;
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState == 4 && r.status == 200) {
        message_positif('L\'utilisateur a bien été désactivé');
        let q = SelectLigne(user, "tableau_users");
        ModifyRowAfterActivationChangeOrBlacklist(q, "desactiver");
      } else if (r.readyState == 4 && r.status !== 200) {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
      } else {
        message_
      }
    };
    r.open('POST', API_URL + '/user/desactivation', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('user=' + encodeURIComponent(user));
  }

  function ModifyRowAfterActivationChangeOrBlacklist(q, etat, ipBlacklist = null) {
    let lignes;
    let a = q.row.querySelector('.etat_div a');
    let email = q.row.querySelector('#email' + q.nbRow).textContent;
    switch (etat) {
      case "desactiver":
        a.textContent = '❌';
        a.title = "Activer le compte";
        a.onclick = function() {
          OpenUserPopup('activationUser', email, q.nbRow);
        }
        break;
      case "activer":
        a.textContent = '✅';
        a.title = "Desactiver le compte";
        a.onclick = function() {
          OpenUserPopup('desactivationUser', email, q.nbRow);
        }
        break;
      case "blacklister":
        lignes = document.querySelectorAll('table.tableau_users tr');

        if (!Array.isArray(ipBlacklist)) {
          ipBlacklist = [ipBlacklist];
        }
        for (let i = 0; i < ipBlacklist.length; i++) {
          for (let j = 1; j < lignes.length; j++) {
            let ips = lignes[j].querySelectorAll('.ip_users a');
            for (let k = 0; k < ips.length; k++) {
              if (ips[k].textContent === ipBlacklist[i]) {
                lignes[j].style.backgroundColor = '#e5e5e5';
                ips[k].style.textDecoration = 'line-through';
                lignes[j].querySelector('.etat_div a').style.textDecoration = 'none';
                lignes[j].querySelector('.etat_div a').textContent = 'Déblacklister';
                lignes[j].querySelector('.etat_div a').title = "déblacklister le compte";
                lignes[j].querySelector('.etat_div a').onclick = function() {
                  OpenUserPopup('deblacklistageUser', lignes[j].querySelector('#email' + j).textContent, j);
                }
                break;
              }
            }
          }
        }
        break;

      case "déblacklister":
        lignes = document.querySelectorAll('table.tableau_users tr');

        if (!Array.isArray(ipBlacklist)) {
          ipBlacklist = [ipBlacklist];
        }
        for (let i = 0; i < ipBlacklist.length; i++) {
          for (let j = 0; j < lignes.length; j++) {
            let ips = lignes[j].querySelectorAll('.ip_users a');
            for (let k = 0; k < ips.length; k++) {
              if (ips[k].textContent === ipBlacklist[i]) {
                lignes[j].style.backgroundColor = 'white';
                ips[k].style.textDecoration = 'underline';
                lignes[j].querySelector('.etat_div a').textContent = '❌';
                lignes[j].querySelector('.etat_div a').style.textDecoration = 'none';
                lignes[j].querySelector('.etat_div a').title = 'Réactiver le compte';
                lignes[j].querySelector('.etat_div a').onclick = function() {
                  OpenUserPopup('activationUser', lignes[j].querySelector('#email' + j).textContent, j);
                }
                break;
              }
            }
          }
        }
        break;
    }
  }

  function activer_utilisateur() {
    if (!document.getElementById('confirm_activationUser').checked) {
      message_negatif('Veuillez confirmer l\'activation de l\'utilisateur');
      return false;
    }
    let user = document.getElementById('user_adminAction').value;
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState == 4 && r.status == 200) {
        message_positif('L\'utilisateur a bien été activé');
        let q = SelectLigne(user, "tableau_users");
        ModifyRowAfterActivationChangeOrBlacklist(q, "activer");
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
      }
    };
    r.open('POST', API_URL + '/user/activation', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('user=' + encodeURIComponent(user));
  }

  function blacklister_utilisateur() {
    if (!document.getElementById('confirm_blacklistage').checked) {
      message_negatif('Veuillez confirmer le blacklistage de l\'utilisateur');
      return false;
    }
    let user = document.getElementById('user_adminAction').value;
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState == 4 && r.status == 200) {
        message_positif('L\'utilisateur a bien été blacklisté');
        let q = SelectLigne(user, "tableau_users");
        ModifyRowAfterActivationChangeOrBlacklist(q, "blacklister", JSON.parse(r.responseText));
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
      }
    };
    r.open('POST', API_URL + '/user/blacklist', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('user=' + encodeURIComponent(user));
  }

  function deblacklister_utilisateur() {
    if (!document.getElementById('confirm_deblacklistage').checked) {
      message_negatif('Veuillez confirmer le déblacklistage de l\'utilisateur');
      return false;
    }
    let user = document.getElementById('user_adminAction').value;
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState == 4 && r.status == 200) {
        message_positif('L\'utilisateur a bien été déblacklisté');
        let q = SelectLigne(user, "tableau_users");
        ModifyRowAfterActivationChangeOrBlacklist(q, "déblacklister", JSON.parse(r.responseText));
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
      }
    };
    r.open('POST', API_URL + '/user/deblacklist', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('user=' + encodeURIComponent(user));
  }

  function supprimer_utilisateur() {
    if (!document.getElementById('confirm_suppression_user').checked) {
      message_negatif('Veuillez confirmer la suppression de l\'utilisateur');
      return false;
    }
    let user = document.getElementById('user_adminAction').value;
    let r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.readyState == 4 && r.status == 200) {
        message_positif('L\'utilisateur a bien été supprimé');
        let q = SelectLigne(user, "tableau_users");
        ModifyRowAfterSuppression(q, "tableau_users");
      } else {
        try {
          message_negatif('Quelque chose s\'est mal passé : ' + JSON.parse(r.responseText));
        } catch (e) {
          message_negatif('Quelque chose s\'est mal passé... ');
        }
      }
    };
    r.open('POST', API_URL + '/user/delete', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('user=' + encodeURIComponent(user));
  }

  function decocherCases() {
    let popup = document.getElementById('Popup');
    let cases = popup.querySelectorAll('input[type="checkbox"]');
    for (let i = 0; i < cases.length; i++) {
      cases[i].checked = false;
    }
  }
</script>