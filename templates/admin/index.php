<?php include(__DIR__ . '/includes/Header.php'); ?>
<div id="contenu_page">
  <div id="col_latérale">
    <?php include(__DIR__ . '/includes/colonne-laterale.php'); ?>
  </div>
  <div id="contenu">
    <div id="statistiques">
      <?php include(__DIR__ . '/includes/stats.php'); ?>

    </div>
    <div id="liens" class="displayNone">
      <?php include(__DIR__ . '/includes/liens.php'); ?>
    </div>

    <div id="utilisateurs" class="displayNone">
      <?php include(__DIR__ . '/includes/users.php'); ?>
    </div>

    <div id="blacklists" class="displayNone">
      <?php include(__DIR__ . '/includes/blacklists.php'); ?>
    </div>

    <div id="parametres" class="displayNone">
      <?php include(__DIR__ . '/includes/parametres.php'); ?>
    </div>
  </div>
</div>
<?php include(__DIR__ . '/includes/popup.php'); ?>
<?php include(__DIR__ . '/includes/Footer.php'); ?>