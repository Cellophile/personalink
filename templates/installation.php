<?php include __DIR__ . '/includes/Header.php'; ?>

<div id="conteneur">
  <div id="header">
    <a href="<?= URLSITE ?>">
      <img src="/assets/images/<?= LOGO ?>" class="logo" alt="logo <?= TITRE ?>"></a>
  </div>
  <h1>Vous y êtes presque !</h1>
  <h2>L'installation est bientôt terminée.</h2>
  <p>Pour finaliser votre installation, veuillez créer votre compte administrateur :</p>
  <form action="/installation" method="POST">
    <p class="reponse-negative <?= isset($erreur) ? '' : 'displayNone' ?>">
    <?php if (isset($erreur)) { ?>
        <?= $erreur ?>
        <?php } ?>
      </p>
    <div id="admin">
      <label for="nom">Nom :</label>
      <input id="nom" type="text" name="nom" value="<?php if (isset($user)) echo $user->getNom() ?>" required><br>
      <?php if (isset($erreurs['nom'])) { ?><div class="reponse-negative"> <?php foreach ($erreurs['nom'] as $erreur) echo $erreur ?></div> <?php } ?>
      <label for="prenom">Prénom :</label>
      <input id="prenom" type="text" name="prenom" value="<?php if (isset($user)) echo $user->getPrenom() ?>" required><br>
      <label for="ADMINEMAIL">Email d'administration :</label>
      <input id="ADMINEMAIL" type="email" name="ADMINEMAIL" value="<?= isset($_POST['ADMINEMAIL']) ? $_POST['ADMINEMAIL'] : '' ?>" required><br>
      <label for="password">Votre mot de passe :</label>
      <input id="password" type="password" name="password" required><br>
      <label for="confirm_password">Confirmez votre mot de passe :</label>
      <input id="confirm_password" type="password" name="confirm_password" required><br>
      <button href="#" class="bouton center" id="suivant">Suivant</button>
    </div>
    <div id="infos" class="displayNone">
      <label for="TITRE">Titre du site :</label>
      <input id="TITRE" type="text" name="TITRE" placeholder="<?= TITRE ?>" value="<?= isset($_POST['TITRE']) ? htmlentities($_POST['TITRE']) : '' ?>" required><br>
      <label for="SOUSTITRE">Sous-titre :</label>
      <input id="SOUSTITRE" type="text" name="SOUSTITRE" placeholder="<?= SOUSTITRE ?>" value="<?= isset($_POST['SOUSTITRE']) ? htmlentities($_POST['SOUSTITRE']) : '' ?>" required><br>
      <label for="URLSITE">Vérifiez l'url de votre site :</label>
      <input id="URLSITE" type="url" name="URLSITE" value="https://<?= $_SERVER['HTTP_HOST'] ?>/" required><br> 
      <p>Toutes ces informations seront modifiables depuis votre espace admin.</p>
      <input type="submit" class="bouton center" name="Finir l'installation">
    </div>
  </form>
  <?php include 'includes/Footer.php'; ?>
</div>

<script defer>
  document.getElementById("suivant").addEventListener("click", function(e) {
    e.preventDefault();
    if(document.getElementById("password").value != document.getElementById("confirm_password").value) {
      document.querySelector(".reponse-negative").classList.remove("displayNone");
      document.querySelector(".reponse-negative").innerHTML = "Les mots de passe ne sont pas identiques";
    } else {
      document.getElementById("admin").classList.add("displayNone");
      document.getElementById("infos").classList.remove("displayNone");
    }
  });
</script>
</body>

</html>