<?php
include __DIR__ . '/includes/Header.php';
include __DIR__ . '/includes/dashboardPanel.php';
?>
	<div id="conteneur">
		<div id="header">
			<a href="<?= URLSITE ?>">
				<img src="/assets/images/<?= LOGO ?>" class="logo" alt="logo <?= TITRE ?>"></a>
			<h1><?= TITRE ?></h1>
		</div>
		<h2><?= SOUSTITRE ?></h2>
		<?php
		// TRAITEMENT DES INFOS
		include_once __DIR__ . '/includes/GestionSucces.php';
		include_once __DIR__ . '/includes/GestionErreurs.php';
		?>
		<form class="form" method="post" action="/creation-lien">
			<div id="form">
				<label for="url-à-raccourcir">
					<h3> <b>L'URL à raccourcir</b></h3>
				</label>
				<input id="url-à-raccourcir" class="form-control" type="url" name="ubase" placeholder=" https://" required="">
				<label for="texte-personnalisé">
					<h3> <b>Texte du raccourci personnalisé</b></h3>
				</label>
				<input id="texte-personnalisé" class="form-control" type="text" name="uperso" placeholder=" Chocolat" required="">
				<input type="button" onclick="sectionouvrable('Section1')" class="boutondesection" value="Plus de réglages">
				<div id="Section1" class="cacher">
					<label for="date-expiration">
						<h5>Date d'expiration du lien (optionnel)</h5>
					</label>
					<input id="date-expiration" class="form-options-champs" type="datetime-local" name="datexpi">
					<label for="mot-de-passe">
						<h5>Mot de passe (optionnel)</h5>
					</label>
					<input id="mot-de-passe" class="form-options-champs" type="password" name="mdp">
				</div>
				<h3> <b>Recopiez le Captcha</b></h3>
				<div id="validation">
					<div id="captcha-valid">
						<img src="/captcha" alt="">
					</div>
					<div id="text-valid">
						<label for="text-captcha" style="display: none;">captcha</label>
						<input type="text" id="text-captcha" name="captcha" placeholder=" Captcha" required>
					</div>
					<div id="bouton-valid">
						<input class="bouton" name="creationLien" type="submit" value="Allons-y !">
					</div>
				</div>
			</div>
		</form>

		<?php include_once __DIR__ . '/includes/sectionExplications.php'; ?>
		<?php include_once __DIR__ . '/includes/Footer.php'; ?>
	</div>



</body>

</html>