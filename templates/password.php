<?php include __DIR__ . '/includes/Header.php'; ?>

	<div id="conteneur">
		<div id="header">
			<a href="<?= URLSITE ?>">
			<img src="/assets/images/<?= LOGO ?>" class="logo" alt="logo <?= TITRE ?>"></a>
		</div>
		<p>Le lien que vous voulez suivre est protégé par un mot de passe.<p>
		<form class="form" method="POST" action="#">
			<div id="form">
				<input id="password" class="form-options-champs" type="password" name="mdpaverifier" required="">
				<div id="bouton-valid-password">
					<input class="bouton" type="submit" value="Allons-y !">
				</div>
			</div>
			</form>
			<?php 
			if ($erreurpassword == 1) {
				echo '
				<div id="reponse-negative">
					Mot de passe incorrect.
					</div>';
			}
			if (!empty($_GET['echec'])){
				if ($echec == 1){
			echo '
			<div id="reponse-negative">
				Ce lien court n\'existe pas ou a expiré.
				</div>';
				}
			}


			include __DIR__ . '/includes/Footer.php';
			?>

	</div>
</body>
</html>
