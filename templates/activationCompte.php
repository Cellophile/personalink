<?php include __DIR__ . '/includes/Header.php'; ?>

  <div id="conteneur">
    <div id="header">
      <a href="<?= URLSITE ?>">
        <img src="/assets/images/<?= LOGO ?>" class="logo" alt="logo <?= TITRE ?>"></a>
    </div>
    <h1>Activation du compte</h1>
    <h2>Bienvenue, <?= $user->getPrenom() . ' ' . $user->getNom(); ?> ! </h2>
    <p>Pour activer votre compte, veuillez entrer le mot de passe que vous avez choisi :</p>
    <form action="activation" method="POST">
      <input type="hidden" name="email" value="<?= $user->getEmail() ?>">
      <input type="hidden" name="token" value="<?= $token ?>">
      <input type="password" class="champ_modifiable" name="password" placeholder="Mot de passe" required>
      <?php
      // Traitement de l'erreur de mot de passe
      if (!empty($_GET['echec'])) {
        if ($_GET['echec'] == 1) {
          echo '
                <div id="reponse-negative">
                Mot de passe invalide. Veuillez réessayer.
                </div>';
        }
      }
      ?>
      <input type="submit" class="bouton center" name="Activer mon compte">
    </form>
    <p><a href="<?= URLSITE ?>">Revenir à la page d'accueil</a><br><br></p>
    <?php include 'includes/Footer.php'; ?>

  </div>
</body>

</html>