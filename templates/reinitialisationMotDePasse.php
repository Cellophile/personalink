<?php include __DIR__ . '/includes/Header.php'; ?>

  <div id="conteneur">
    <div id="header">
      <a href="<?= URLSITE ?>">
        <img src="/assets/images/<?= LOGO ?>" class="logo" alt="logo <?= TITRE ?>"></a>
    </div>
    <h1>Réinitialisation de mot de passe</h1>
    <h2>Bonjour, <?= $user->getPrenom() . ' ' . $user->getNom(); ?> ! </h2>
    <p>Veuillez indiquer votre nouveau mot de passe :</p>
    <form action="reinitialisation-mdp" method="POST" onsubmit="verificationpassword(event)">
      <input type="hidden" name="email" value="<?= $user->getEmail() ?>">
      <input type="hidden" name="token" value="<?= $token ?>">
      <input id="password1" type="password" class="champ_modifiable" name="password1" placeholder="Mot de passe" required>
      <input id="password2" type="password" class="champ_modifiable" name="password2" placeholder="Mot de passe" required>
      <div id="passwordpasidentiques" class="reponse-negative"></div>
      <?php
      // Traitement de l'erreur de mot de passe
      if (!empty($_GET['echec'])) {
        if ($_GET['echec'] == 1) {
          echo '
                <div id="reponse-negative">
                Les mots de passe ne sont pas identiques.
                </div>';
        }
      }
      ?>
      <input type="submit" class="bouton center" name="Activer mon compte">
    </form>
    <p><a href="<?= URLSITE ?>">Revenir à la page d'accueil</a><br><br></p>
    <?php include 'includes/Footer.php'; ?>
  </div>
  <script>
    function verificationpassword(e) {
      var p1 = document.getElementById('password1').value;
      var p2 = document.getElementById('password2').value;
      var reponse = document.getElementById('passwordpasidentiques');

      if (p1 === p2) {
        return true;
      } else {
        e.preventDefault();
        reponse.style.display = "block";
        reponse.innerHTML = "Les mots de passe ne sont pas identiques";
        return false;
      }
    }
  </script>
</body>

</html>