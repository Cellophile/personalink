<?php include('includes/Header.php'); ?>
<div id="contenu_page">
  <div id="col_latérale">
    <?php include('includes/colonne-laterale.php'); ?>
  </div>
  <div id="contenu">
    <div id="mon-compte" class="displayNone">
      <?php include('includes/mon-compte.php'); ?>

    </div>
    <div id="mes-liens">
      <?php include('includes/mes-liens.php'); ?>
    </div>

    <div id="confidentialite" class="displayNone">
      <?php include('includes/confidentialite.php'); ?>
    </div>
  </div>
</div>
<?php include('includes/Footer.php'); ?>