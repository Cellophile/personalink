<h2>Confidentialité</h2>
<h3 class="supprimer_compte">Supprimer le compte</h3>
<p>Êtes-vous certain de vouloir supprimer votre compte ? Ceci entrainera la suppression de toutes les données de votre compte sur ce site (y compris les liens). Pour supprimer votre compte, saisissez votre mot de passe ci-dessous.</p>

<label for="password_suppr">Mot de passe :</label>
<input id="password_suppr" type="password" name="supprimer_compte" required>
<div id="m_n">Mot de passe incorrect</div>
<input id="suppr_liens" type="checkbox" name="suppr_liens">
<label for="suppr_liens">Voulez-vous supprimer tous vos liens en même temps que votre compte ? (Si la case n'est pas cochée, les liens continueront de fonctionner, jusqu'à leur expiration).</label>
<input class="bouton" type="button" name="envoyer" value="Supprimer le compte" onclick="supprimerCompte()">


<script>
  function supprimerCompte() {
    let password = document.getElementById('password_suppr').value;
    let suppr_liens = document.getElementById('suppr_liens').checked;
    let m_n = document.getElementById('m_n');
    if (password == '') {
      m_n.style.visibility = 'visible';
      m_n.style.opacity = 1;
      setTimeout(function() {
        m_n.style.opacity = 0;
        m_n.style.visibility = 'hidden';
      }, 3000);
    }

    r = new XMLHttpRequest();
    r.onreadystatechange = function() {
      if (r.status === 202) {
        m_n.style.visibility = 'visible';
        m_n.style.opacity = 1;
        m_n.style.color = 'green';
        m_n.style.backgroundColor = '#e7fae7';
        m_n.textContent = 'Votre compte a bien été supprimé. Vous allez être redirigé vers la page d\'accueil dans quelques secondes.';
        setTimeout(function() {
          document.location.href = HOME_URL;
        }, 3000);
      } else {
        m_n.style.visibility = 'visible';
        m_n.style.opacity = 1;
        setTimeout(function() {
          m_n.style.opacity = 0;
          m_n.style.visibility = 'hidden';
        }, 3000);
      }
    };
    r.open('POST', API_URL + '/user/delete', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send('password=' + password + '&suppr_liens=' + suppr_liens);
  }
</script>