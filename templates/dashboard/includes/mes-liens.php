<?php
$dans_quinze_jours = new DateTime('+15 days');
?>
<div style="display:flex;">
  <h2>Historique des liens en cours</h2>
  <button onclick="location.href= HOME_URL" class="bouton btn-new-lien">Créer un nouveau lien</button>
</div>
<table class="tableau_liens">
  <tr class="titre_colonne">
    <th scope="col">Lien court</th>
    <th scope="col">URL du lien</th>
    <th scope="col">Clics</th>
    <th scope="col">Date de Création</th>
    <th scope="col">Date d'expiration</th>
    <th scope="col">Mot de passe</th>
    <th scope="col">Outils</th>
  </tr>
  <?php
  if (empty($liens)) { ?>
    <tr>
      <th colspan="7">Vous n'avez aucun lien en court de validité ! </th>
    </tr>
    <?php
  } else {
    $i = 0;
    foreach ($liens as $lien) {
      $i++; ?>
      <tr>
        <th scope="row" class="titre_ligne"><?= $lien->getUpersok() ?></th>
        <td class="url_du_lien" title="<?= $lien->getUrlbase() ?>"><a href="<?= $lien->getUrlbase() ?>"><?= $lien->getUrlbase() ?></a></td>
        <td><?= $lien->getNbclics() ?></td>
        <td><?= $lien->getDcreation()->format('d/m/Y - H:i') ?></td>
        <td id="date_expiration<?= $i ?>">
          <?php if (!$lien->getDatexpi()) {
            echo "jamais";
          } else {
            echo $lien->getDatexpi()->format('d/m/Y - H:i');
          }
          ?>
        </td>
        <td>
          <div class="pwd_div">
            <?php if ($lien->getMdp()) { ?>
              <input id="pwd<?= $i; ?>" class="pwd_display cacher_pwd" type="password" name="cacher_pwd" autocomplete="off" readonly value="<?= $lien->getMdp() ?>">
              <img id="btn_voir<?= $i; ?>" class="img_voir_pwd" src="/assets/images/voir.svg" onclick="montrer_pwd('pwd<?= $i; ?>','txt<?= $i; ?>','btn_voir<?= $i; ?>','btn_cacher<?= $i; ?>')">
              <input id="txt<?= $i; ?>" class="pwd_display montrer_pwd" type="text" name="montrer_pwd" autocomplete="off" readonly value="<?= $lien->getMdp() ?>">
              <img id="btn_cacher<?= $i; ?>" class="img_cacher_pwd" src="/assets/images/cacher.svg" onclick="cacher_pwd('pwd<?= $i; ?>','txt<?= $i; ?>','btn_voir<?= $i; ?>','btn_cacher<?= $i; ?>')">
            <?php } ?>
          </div>
        </td>
        <td>
          <img src="/assets/images/copier.svg" title="Copier le lien court" onclick="copier_lien_court('<?= URLSITE . $lien->getUpersok() ?>','txt_copie<?= $i; ?>')">
          <p id="txt_copie<?= $i; ?>" class="txt_copie">Lien court copié !</p>
          <?php if ($lien->getMdp()) { ?>
            <a id="motdepasse<?= $i; ?>" href="#" onclick="OpenPopup('modifier_motdepasse','<?= $lien->getUpersok() ?>','<?= $lien->getMdp() ?>')"><img src="/assets/images/cadenas fermé.svg" title="Modifier ou supprimer le mot de passe"></a>
          <?php } else { ?>
            <a id="motdepasse<?= $i; ?>" href="#" onclick="OpenPopup('ajouter_motdepasse','<?= $lien->getUpersok() ?>','null')"><img src="/assets/images/cadenas ouvert.svg" title="Ajouter un mot de passe au lien court"></a>
            <?php }
          if ($lien->getDatexpi()) {
            if ($dans_quinze_jours > $lien->getDatexpi()) { ?>
              <a id="expiration<?= $i; ?>" href="#" onclick="OpenPopup('modifier_expiration','<?= $lien->getUpersok() ?>','<?= $lien->getDatexpi()->format('d/m/Y - H:i') ?>')"><img src="/assets/images/expiration proche.svg" title="Date d'expiration proche ! Cliquer pour la modifier"></a>
            <?php } else { ?>
              <a id="expiration<?= $i; ?>" href="#" onclick="OpenPopup('modifier_expiration','<?= $lien->getUpersok() ?>','<?= $lien->getDatexpi()->format('d/m/Y - H:i') ?>')"><img src="/assets/images/expiration active.svg" title="Modifier/Supprimer la date d'expiration"></a>
            <?php }
          } else { ?>
            <a id="expiration<?= $i; ?>" href="#" onclick="OpenPopup('ajouter_expiration','<?= $lien->getUpersok() ?>','null')"><img src="/assets/images/expiration inactive.svg" title="Ajouter une date d'expiration"></a>
          <?php } ?>
          <a id="supprimer_lien<?= $i; ?>" href="#" onclick="OpenPopup('suppression','<?= $lien->getUpersok() ?>')"><img src="/assets/images/Corbeille.svg" title="Supprimer le lien"></a>
        </td>
      </tr>
  <?php }
  } ?>
</table>


<div id="fenetrepopup" class="fenetrepopup displayNone">
  <div id="Popup" class="Popup">
    <span class="labelretour" onclick="FermerPopup()">
      <img src="/assets/images/fermer.svg" title="fermer">
    </span>
    <input id="lien_courant" type="hidden" name="lien" value="">
    <div id="ContenuPopup">
      <div id="ajouter_motdepasse" class="ajouter_motdepasse section center displayNone">
        <h3>Ajouter un mot de passe :</h3>
        <form>
          <label for="ajout_password_lien">Mot de passe : </label>
          <input id="ajout_password_lien" class="champ_modifiable" type="password" name="ajout_password_lien"><br>
          <label for="ajout_password_lien_confirm">Confirmer le mot de passe : </label>
          <input id="ajout_password_lien_confirm" class="champ_modifiable" type="password" name="ajout_password_lien_confirm"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="mot_de_passe_lien('ajout_password_lien','ajout_password_lien_confirm')" value="Enregistrer">
        </form>
      </div>
      <div id="modifier_motdepasse" class="modifier_motdepasse section center displayNone">
        <h3>Modifier/supprimer le mot de passe :</h3>
        <div id="info_motdepasse">
          <p>Laisser les champs vide pour supprimer le mot de passe.</p>
        </div>
        <form>
          <label for="password_lien">Nouveau mot de passe : </label>
          <input id="password_lien" class="champ_modifiable" type="password" name="password_lien"><br>
          <label for="password_lien">Confirmer le mot de passe : </label>
          <input id="password_lien_confirm" class="champ_modifiable" type="password" name="password_lien_confirm"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="mot_de_passe_lien('password_lien','password_lien_confirm')" value="Enregistrer">
        </form>
      </div>
      <div id="ajouter_expiration" class="ajouter_expiration section center displayNone">
        <h3>Ajouter une date d'expiration :</h3>
        <form>
          <input id="ajout_datetime" type="datetime-local" class="champ_modifiable" name="datexpi"><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="date_expiration('ajout_datetime')" value="Enregistrer">
        </form>
      </div>
      <div id="modifier_expiration" class="modifier_expiration section center displayNone">
        <h3>Modifier/supprimer la date d'expiration :</h3>
        <form>
          <input id="mod_datetime" type="datetime-local" class="champ_modifiable" name="datexpi"><br>
          <input id="supprimer_expi" type="checkbox" name="supprimer_expi">
          <label for="supprimer_expi">Supprimer la date d'expiration</label><br>
          <input type="button" class="bouton center" name="Enregistrer" onclick="date_expiration('mod_datetime', true)" value="Enregistrer">
        </form>
      </div>
      <div id="suppression" class="suppression section center displayNone">
        <h3>Êtes-vous sûr de vouloir supprimer ce lien ?</h3>
        <form>
          <input id="confirm_suppression" type="checkbox" name="suppression">
          <label for="confirm_suppression">Supprimer définitivement</label><br>
          <input type="button" class="bouton center" onclick="supprimer_lien()" value="Supprimer le lien">
        </form>
      </div>
      <div id="message_sauvegarde_lien" class="message_sauvegarde" style="margin: 0 auto;"></div>
    </div>
  </div>
</div>