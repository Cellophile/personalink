<h2>Mes informations</h2>
<div id="message_sauvegarde" class="message_sauvegarde"></div>
<label for="nom">Nom</label><br>
<input id="nom" class="champ_modifiable" type="text" value="<?= $user->getNom() ?>" readonly>
<img id="mod1" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_champ('nom','mod1','sauv1')">
<img id="sauv1" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_champ('nom','sauv1','mod1')"><br>
<label for="prenom">Prénom</label><br>
<input id="prenom" class="champ_modifiable" type="text" value="<?= $user->getPrenom() ?>" readonly>
<img id="mod2" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_champ('prenom','mod2','sauv2')">
<img id="sauv2" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_champ('prenom','sauv2','mod2')"><br>
<label for="email">Email</label><br>
<input id="email" class="champ_modifiable" type="text" value="<?= $user->getEmail() ?>" placeholder="<?= $user->getEmail() ?>" readonly>
<img id="mod3" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_champ('email','mod3','sauv3')">
<img id="sauv3" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_champ('email','sauv3','mod3')"><br>
<label for="password">Mot de passe</label><br>
<input id="password" class="champ_modifiable" type="password" value="<?php for ($i = 0; $i < $user->getLenPwd(); $i++) {
                                                                        echo '*';
                                                                      }  ?>" placeholder="Mot de passe actuel" readonly required>
<img id="mod4" class="mod_champ" src="/assets/images/modifier.svg" onclick="modifier_champ('password','mod4','sauv4','pass')">
<img id="sauv4" class="sauv_champ" src="/assets/images/sauvegarder.svg" onclick="sauvegarder_champ('password','sauv4','mod4','pass')"><br>
<input id="new_password" class="champ_modifiable" style="display: none;" type="password" required placeholder="Nouveau mot de passe"><br>
<input id="new_password_bis" class="champ_modifiable" style="display: none;" type="password" required placeholder="Confirmer le mot de passe"><br>
<label for="forfait">Forfait</label><br>
<input type="text" id="forfait" class="champ_modifiable" value="<?= $user->getForfait() ?>" readonly>
<div id="info_connexion">
  <p><?= $user->getDateDernierLogin() ? "Dernière connexion le " . $user->getDateDernierLogin()->format('d/m/Y - H:i') : "Première connexion !" ?></p>
</div>



<script>
  function modifier_champ(champ, cache, montre, pass) {
    var a = document.getElementById(champ);
    var b = document.getElementById(cache);
    var c = document.getElementById(montre);
    a.style.outline = 'auto';
    a.removeAttribute('readonly');
    a.focus();
    b.style.display = 'none';
    c.style.display = 'inline';
    if (pass !== undefined) {
      var pass_1 = document.getElementById('new_password');
      var pass_2 = document.getElementById('new_password_bis');
      pass_1.style.display = 'block';
      pass_2.style.display = 'block';
      a.value = '';
    }

  }

  function sauvegarder_champ(champ, cache, montre, pass) {

    // pass ne sert qu'à modifier le mot de passe :
    if (pass !== undefined) {
      var pass_1 = document.getElementById('new_password');
      var pass_2 = document.getElementById('new_password_bis');
      if (pass_1.value == pass_2.value) {
        if (document.getElementById(champ).value == '' || pass_1.value == '') {
          message_negatif("Impossible de laisser ce champ vide.");
          throw new Error('empty value');
        }
        pass_1.style.display = 'none';
        pass_2.style.display = 'none';
        var pass_identique = true;
      } else {
        message_negatif("Le nouveau mot de passe n'est pas identique.");
        throw new Error('non-identical passwords');
      }
    }



    var a = document.getElementById(champ);
    var b = document.getElementById(cache);
    var c = document.getElementById(montre);



    // on enregistre l'info :
    if (a.value == '') {
      message_negatif("Impossible de laisser ce champ vide.");
      throw new Error('empty value');

    } else {
      a.style.outline = 'none';
      a.setAttribute('readonly', '');
      a.blur();
      b.style.display = 'none';
      c.style.display = 'inline';

      if (pass_identique === true) {
        var info = champ + '=' + encodeURIComponent(a.value) + '&new_password=' + pass_1.value;
      } else {
        var info = champ + '=' + encodeURIComponent(a.value);
      }



      var r = new XMLHttpRequest();

      r.onreadystatechange = function() {
        if (r.readyState == 4) {
          if (r.status === 202) {
            var response = JSON.parse(r.responseText);
            var retour = document.getElementById('message_sauvegarde');
            retour.innerHTML = "<p>Informations mises à jour avec succès !</p>";
            retour.style.color = "green";
            retour.style.backgroundColor = '#e7fae7';
            retour.style.visibility = 'visible';
            retour.style.opacity = 1;
            setTimeout(function() {
              retour.style.opacity = 0;
              retour.style.visibility = 'hidden';
            }, 3000);
          } else if (r.status === 401) {
            message_negatif("Vous n'êtes pas autorisé à effectuer cette action. Veuillez vous reconnecter.");
            setTimeout(function() {
              // Redirection forcée pour se reconnecter.
              location.replace('<?= URLSITE ?>');
            }, 3000);
          } else if (r.status === 409) {
            message_negatif("Un compte est déjà associé à cet email. Veuilliez en choisir une autre, ou bien vous reconnecter avec cet email.");
            var z = document.getElementById('email');
            z.value = z.placeholder;
          } else if (r.status === 406) {
            message_negatif("Mot de passe actuel incorrect.");
            var z = document.getElementById('email');
            z.value = z.placeholder;
          } else {
            message_negatif('Oups, quelque chose s\'est mal passé...<br>' + JSON.parse(r.responseText));
          }
        } else {
          message_neutre('Veuillez patienter...');
        }
      }


      r.open('POST', API_URL + '/user/update', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send(info);

    }

  }

  function message_negatif(message) {
    var retour = document.getElementById('message_sauvegarde');

    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "red";
    retour.style.backgroundColor = '#ffdada';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 3000);
  }


  function message_neutre(message) {
    var retour = document.getElementById('message_sauvegarde');

    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "cornflowerblue";
    retour.style.backgroundColor = '#dae9ff';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 3000);

  }
</script>