<a href="<?= URLSITE ?>" class="btn-retour-accueil">Retour à l'accueil</a>
<h2>Bienvenue, <?= $user->getPrenom() ?> !</h2>
<ul class="puce">
  <li class="li-mon-compte" onclick="afficher_partie('mon-compte')">Mon compte</li>
  <li class="li-mes-liens" onclick="afficher_partie('mes-liens')">Mes liens</li>
  <li class="li-confidentialite" onclick="afficher_partie('confidentialite')">Confidentialité</li>
  <?php if ($user->isAdmin()) : ?>
    <li class="li-administration" onclick="location.href= '<?= URLSITE ?>admin'">Administration</li>
  <?php endif; ?>
</ul>