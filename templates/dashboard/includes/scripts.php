<script>
  afficher_partie(window.location.pathname);

  function afficher_partie(page) {
    let split;
    if (page == '/dashboard' || page == '/dashboard/') {
      page = 'mes-liens';
      split = ['mes-liens', null];
    } else {
      page = page.split('/').pop(1);
      split = page.split('?');
      page = split[0];
    }
    let section_visible = document.getElementById(page);
    let li_active = document.querySelector('.li-' + page);
    let section = [];
    let li = [];
    section[1] = document.getElementById('mon-compte');
    section[2] = document.getElementById('mes-liens');
    section[3] = document.getElementById('confidentialite');
    li[1] = document.querySelector('.li-mon-compte');
    li[2] = document.querySelector('.li-mes-liens');
    li[3] = document.querySelector('.li-confidentialite');
    for (let i = 1; i < 4; i++) {
      section[i].classList.add("displayNone");
      li[i].classList.remove('active');
    }
    section_visible.classList.remove("displayNone");
    li_active.classList.add('active');

    let get = (split[1] == null) ? '' : '?' + split[1];
    history.pushState(null, null, '/dashboard/' + page + get);

  }

  function montrer_pwd(pwd, txt, btn1, btn2) {
    pwd = document.getElementById(pwd);
    txt = document.getElementById(txt);
    btn1 = document.getElementById(btn1);
    btn2 = document.getElementById(btn2);
    pwd.style.display = 'none';
    txt.style.display = 'block';
    btn1.style.display = 'none';
    btn2.style.display = 'block';
  }

  function cacher_pwd(pwd, txt, btn1, btn2) {
    pwd = document.getElementById(pwd);
    txt = document.getElementById(txt);
    btn1 = document.getElementById(btn1);
    btn2 = document.getElementById(btn2);
    pwd.style.display = 'block';
    txt.style.display = 'none';
    btn1.style.display = 'block';
    btn2.style.display = 'none';
  }

  function copier_lien_court(lien, validation) {
    let a = document.getElementById(validation);
    // Si le lien n'est pas vide
    if (lien.length) {
      // si navigator.clipboard fonctionne :
      if (navigator.clipboard && window.isSecureContext) {
        // navigator clipboard api method'
        navigator.clipboard.writeText(lien).then(() => {
          // Prévenir que le texte est copié :
          txt_copie_ok(a);
        })
      } else {
        // text area method
        let textArea = document.createElement("textarea");
        textArea.value = lien;
        // make the textarea out of viewport
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        return new Promise((res, rej) => {
          // here the magic happens
          document.execCommand('copy') ? res() : rej();
          textArea.remove();
          // Prévenir que le texte est copié :
          txt_copie_ok(a);
        });
      }
    }
  }

  function txt_copie_ok(a) {
    a.style.opacity = 1;
    a.style.visibility = 'visible';
    setTimeout(function() {
      a.style.opacity = 0;
      a.style.visibility = 'hidden';
    }, 3000);
  }

  function OpenPopup(section, lien, info1) {
    let affichage = document.getElementById('fenetrepopup');
    affichage.classList.remove('displayNone');

    section = document.getElementById(section);
    section.classList.remove('displayNone');

    let input_lien = document.getElementById('lien_courant');
    input_lien.value = lien;
  }


  function FermerPopup() {
    let affichage = document.getElementById('fenetrepopup');
    affichage.classList.add('displayNone');
    let sections = document.getElementsByClassName('section');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.add('displayNone');
    }
  }

  document.addEventListener('DOMContentLoaded', () => {
    let affichage = document.getElementById('fenetrepopup');
    window.addEventListener('click', (event) => {
      if (event.target === affichage) {
        FermerPopup()
      }
    });
  });

  function message_lien_positif(message) {
    let retour = document.getElementById('message_sauvegarde_lien');
    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "green";
    retour.style.backgroundColor = '#e7fae7';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 2000);
    setTimeout(function() {
      FermerPopup();
    }, 2500);
  }

  function message_lien_neutre(message) {
    let retour = document.getElementById('message_sauvegarde_lien');
    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "cornflowerblue";
    retour.style.backgroundColor = '#dae9ff';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 2000);
    setTimeout(function() {
      FermerPopup();
    }, 2500);
  }

  function message_lien_negatif(message) {
    let retour = document.getElementById('message_sauvegarde_lien');

    retour.innerHTML = "<p>" + message + "</p>";
    retour.style.color = "red";
    retour.style.backgroundColor = '#ffdada';
    retour.style.visibility = 'visible';
    retour.style.opacity = 1;
    setTimeout(function() {
      retour.style.opacity = 0;
      retour.style.visibility = 'hidden';
    }, 3000);
  }

  function supprimer_lien(lien) {
    lien = document.getElementById('lien_courant').value;

    if (document.getElementById('confirm_suppression').checked == false) {
      message_lien_negatif('Veuillez cocher la case.')
    } else {

      let r = new XMLHttpRequest();

      let modif_lien = 'upersok=' + encodeURIComponent(lien);

      r.onreadystatechange = function() {
        if (r.readyState === 4) {
          if (r.status === 202) {
            message_lien_positif('Le lien a bien été supprimé');
            let q = SelectLigne(lien);
            ModifyRowAfterSuppression(q);
          } else {
            message_lien_negatif('Quelque chose s\'est mal passé...<br>' + JSON.parse(r.responseText));
            return false;
          }
        } else {
          message_lien_neutre('Veuillez patienter...');
        }
      };
      r.open('POST', API_URL + '/lien/delete', true);
      r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      r.send(modif_lien);
    }
  }

  function date_expiration(dateTime, suppr = false) {
    let lien = document.getElementById('lien_courant').value;
    dateTime = document.getElementById(dateTime).value;
    suppr = suppr ? document.getElementById('supprimer_expi').checked : false;
    if (suppr === true) {
      dateTime = null;
    } else {
      if (!dateTime) {
        message_lien_negatif('Veuillez renseigner une date d\'expiration complète (avec l\'heure).')
        return false;
      }
    }
    let modif_lien = 'upersok=' + encodeURIComponent(lien) + '&datexpi=' + encodeURIComponent(dateTime);


    let r = new XMLHttpRequest();

    r.onreadystatechange = function() {
      if (r.readyState === 4) {
        if (r.status === 202) {
          message_lien_positif('La date d\'expiration a bien été modifiée');
          let q = SelectLigne(lien);
          ModifyRowAfterExpirationChange(q, dateTime);
        } else {
          message_lien_negatif('Quelque chose s\'est mal passé...<br>' + JSON.parse(r.responseText));
          return false;
        }
      } else {
        message_lien_neutre('Veuillez patienter...');
      }
    };
    r.open('POST', API_URL + '/lien/update', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send(modif_lien);
  }

  function mot_de_passe_lien(mdp, mdp1) {
    let lien = document.getElementById('lien_courant').value;
    mdp = document.getElementById(mdp).value;
    mdp1 = document.getElementById(mdp1).value;
    if (mdp == mdp1) {
      var modif_lien = 'upersok=' + encodeURIComponent(lien) + '&mdp=' + encodeURIComponent(mdp);
    } else {
      message_lien_negatif('Les mots de passes ne sont pas identiques.')
      throw new Error('Not identical password');
    }

    let r = new XMLHttpRequest();

    r.onreadystatechange = function() {
      if (r.readyState === 4) {
        if (r.status === 202) {
          message_lien_positif('Le mot de passe a bien été modifié');
          let q = SelectLigne(lien);
          ModifyRowAfterPasswordChange(q, mdp);
        } else {
          message_lien_negatif('Quelque chose s\'est mal passé...<br>' + JSON.parse(r.responseText));
          return false;
        }
      } else {
        message_lien_neutre('Veuillez patienter...');
      }
    }
    r.open('POST', API_URL + '/lien/update', true);
    r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    r.send(modif_lien);
  }

  function SelectLigne(lien) {
    let lignes = document.querySelectorAll('table tr');

    for (let i = 0; i < lignes.length; i++) {
      let titreLigne = lignes[i].querySelector('.titre_ligne');

      if (titreLigne && titreLigne.textContent.trim() === lien) {
        let pwdDiv = lignes[i].querySelector('.pwd_div');

        if (pwdDiv) {
          return {
            row: lignes[i],
            nbRow: i,
            pwdDiv: pwdDiv
          };
        }
      }
    }
    return null;
  }

  function addPasswordContent(nbRow, pwd) {
    return `
    <input id="pwd${nbRow}" class="pwd_display cacher_pwd" type="password" name="cacher_pwd" autocomplete="off" readonly="" value="${pwd}">
    <img id="btn_voir${nbRow}" class="img_voir_pwd" src="/assets/images/voir.svg" onclick="montrer_pwd('pwd${nbRow}','txt${nbRow}','btn_voir${nbRow}','btn_cacher${nbRow}')">
    <input id="txt${nbRow}" class="pwd_display montrer_pwd" type="text" name="montrer_pwd" autocomplete="off" readonly="" value="${pwd}">
    <img id="btn_cacher${nbRow}" class="img_cacher_pwd" src="/assets/images/cacher.svg" onclick="cacher_pwd('pwd${nbRow}','txt${nbRow}','btn_voir${nbRow}','btn_cacher${nbRow}')">
    `;
  }


  function ModifyRowAfterPasswordChange(q, mdp) {
    let a = q.row.querySelector('#motdepasse' + q.nbRow);
    let img = q.row.querySelector('#motdepasse' + q.nbRow + ' img');
    let lien = q.row.querySelector('.titre_ligne').textContent;
    if (mdp.length > 0) {
      q.pwdDiv.innerHTML = addPasswordContent(q.nbRow, mdp);
      img.src = '/assets/images/cadenas fermé.svg';
      a.onclick = function() {
        OpenPopup('modifier_motdepasse', lien, mdp)
      };
    } else {
      q.pwdDiv.innerHTML = '';
      img.src = '/assets/images/cadenas ouvert.svg';
      a.onclick = function() {
        OpenPopup('ajouter_motdepasse', lien, null)
      };
    }
  }

  function ModifyRowAfterExpirationChange(q, dateTime) {
    let lien = q.row.querySelector('.titre_ligne').textContent;
    let datexpiDiv = q.row.querySelector('#date_expiration' + q.nbRow);
    let img = q.row.querySelector('#expiration' + q.nbRow + ' img');
    let a = q.row.querySelector('#expiration' + q.nbRow);
    if (dateTime !== null && dateTime.length > 0) {
      dateTime = new Date(dateTime);
      datexpiDiv.innerHTML = new Intl.DateTimeFormat('fr-FR', {
        dateStyle: 'short',
        timeStyle: 'short'
      }).format(dateTime);
      futureDate = new Date();
      futureDate.setDate(futureDate.getDate() + 15);
      if (futureDate.getTime() > dateTime.getTime()) {
        img.src = '/assets/images/expiration proche.svg';
      } else {
        img.src = '/assets/images/expiration active.svg';
      }
      a.onclick = function() {
        OpenPopup('modifier_expiration', lien, dateTime)
      };
    } else {
      a.onclick = function() {
        OpenPopup('ajouter_expiration', lien, null)
      };
      datexpiDiv.innerHTML = 'jamais';
      img.src = '/assets/images/expiration inactive.svg';
    }
  }

  function ModifyRowAfterSuppression(q) {
    let tableau = document.querySelector('table tbody');
    tableau.removeChild(q.row);
  }
</script>