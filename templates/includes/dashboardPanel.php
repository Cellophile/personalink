<div id="dashboardpanel">
		<div id="boutonsconnect">
			<?php
			if (isset($_SESSION['auth']) && $_SESSION['auth'] == 'valide') { ?>
				<input id="boutonConnexion" type="button" onclick="location.href='/dashboard/'" value="Mon compte">
			<?php } else { ?>
				<input id="boutonConnexion" type="button" onclick="boutonconnect('connexion', 'registration')" value="Se connecter">
			<?php } ?>
			<input id="boutonRegistration" type="button" onclick="boutonconnect('registration', 'connexion')" value="Créer un compte">
		</div>
		<div id="connexion" class="cacher">
			<form id="auth" method="POST" action="/connexion">
				<input id="identifiant" type="text" placeholder="Adresse mail" name="identifiant" required>
				<div id="mail-necessaire-oubli" class="reponse-negative"></div>
				<input id="password" type="password" name="password" placeholder="mot de passe" required>
				<p><a onclick="oublimdp()" class="lien-reinitialisation-mdp">Mot de passe oublié ?</a></p>
				<input class="bouton connect" name="auth" type="submit" value="Allons-y !">
			</form>
		</div>
		<div id="registration" class="<?php if (!isset($erreurs)) echo "cacher" ?>">
			<form id="register" method="POST" action="/inscription">
				<input id="nom" type="text" placeholder="Nom" name="nom" required value="<?php if (isset($user)) echo $user->getNom(); ?>">
				<?php if (isset($erreurs['nom'])) { ?><div id="nom-necessaire" class="reponse-negative"> <?php foreach ($erreurs['nom'] as $erreur) echo $erreur ?></div> <?php } ?>
				<input id="prenom" type="text" placeholder="Prénom" name="prenom" required value="<?php if (isset($user)) echo $user->getPrenom(); ?>">
				<?php if (isset($erreurs['prenom'])) { ?><div id="prenom-necessaire" class="reponse-negative"> <?php foreach ($erreurs['prenom'] as $erreur) echo $erreur ?></div> <?php } ?>
				<input id="adressemail" type="email" placeholder="Adresse mail" name="adressemail" required value="<?php if (isset($user)) echo $user->getEmail(); ?>">
				<?php if (isset($erreurs['adressemail'])) { ?><div id="adressemail-necessaire" class="reponse-negative"> <?php foreach ($erreurs['adressemail'] as $erreur) echo $erreur ?></div> <?php } ?>
				<input id="pass_word" type="password" placeholder="Mot de passe" name="pass_word" required>
				<input id="confirm_pass_word" type="password" placeholder="Confirmez le Mot de passe" name="confirm_pass_word" required>
				<input class="bouton connect" name="register" type="submit" value="Allons-y !">
			</form>
		</div>
	</div>