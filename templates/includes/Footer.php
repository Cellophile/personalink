<div id="footer">
  <p><?= VERSION ?> — © Théophile Bellintani — Licence <a href="licence.php">TROC</a>, <a href="https://framagit.org/Cellophile/personalink" target="_blank">Opensource</a> et gratuit !</p>
</div>