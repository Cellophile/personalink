<!doctype html>
<html lang="fr">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title><?= TITRE ?></title>
	<link rel="stylesheet" href="/assets/css/style.css" type="text/css">
	<link rel="stylesheet" media="only screen and (max-width: 760px)" href="/assets/css/mobile.css" type="text/css">
	<meta name="description" content="Raccourcissez vos URLs en les personnalisant ! Site Opensource, qui ne garde aucune trace de votre passage.">
	<script>const URLSITE = '<?= URLSITE ?>';</script>
	<script src="/assets/scripts/script.js" defer></script>
</head>

<body id="body">
