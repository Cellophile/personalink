<?php
// Tout s'est bien passé, on affiche l'URL originelle et l'URL raccourcie :

if (isset($reponse) && $reponse == 1) {
  echo "<div id='reponse-positive'>
          Tout s'est bien passé ! <br>
          L'URL originelle : ". $lien->getUrlbase() ." <br>
          L'URL raccourcie : <a href='$urlfinale' target='_blank'>$urlfinale <img class='external-link'src='/assets/images/lien externe.svg'></a>
        </div>";
}

if (!empty($_GET['succes'])) {
  echo "<div id='reponse-positive'>";

  switch ($_GET['succes']) {
    case 2:
      echo "Un mail vient de vous être envoyé ! <br>
            Cliquez sur le lien qu'il contient pour activer votre compte.";
      break;
    case 3:
      echo "Votre compte et vos liens ont été correctement supprimés ! <br>
            Merci et à bientôt !";
      break;
    case 4:
      echo "Votre compte est activé !<br>Vous pouvez vous connecter !";
      break;
    case 5:
      echo "L'email de réinitialisation de mot de passe a bien été envoyé à l'adresse indiquée. Veuillez consulter vos mails.";
      break;
    case 6:
      echo "Le mot de passe a bien été réinitialisé !";
      break;
    case 7:
      echo "L'installation est terminée ! vous pouvez vous connecter pour voir votre espace.";
      break;

    default:
      # code...
      break;
  }

  echo "</div>";

}
