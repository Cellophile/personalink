<?php
namespace public\includes;

use Services\AntiPishing;
// Traitement des erreurs :

if (!empty($_GET['echec'])) {
  echo '<div id="reponse-negative">';

  switch ($_GET['echec']) {
    case 1:
      echo 'URL invalide. Merci de rentrer une adresse URL qui existe.';
      break;
    case 2:
      echo 'Captcha invalide. Veuillez réessayer. Pour info, les lettres sont toutes en majuscules.';
      break;
    case 3:
      echo 'La date et l\'heure ne peut pas être antérieure à aujourd\'hui et maintenant. Veuillez réessayer.';
      break;
    case 4:
      echo 'Le format d\'URL proposé n\'est pas accepté, pour des raisons évidentes de poids sur le serveur.';
      break;
    case 5:
      echo 'Quelque chose s\'est mal passée. Veuillez reessayer.';
      break;
    case 300:
      echo 'Veuillez vous identifier.';
      break;
    case 301:
      echo 'Identifiant ou mot de passe incorrect.';
      break;
    case 302:
      echo 'Un compte est déjà associé à cette adresse mail.';
      break;
    case 303:
      echo 'Le lien utilisé n\'est pas valable, ou le compte est déjà activé.';
      break;
    case 304:
      echo 'Le compte n\'est pas activé. Pour l\'activer, veuillez cliquer sur le lien envoyé par mail.';
      break;
    case 305:
      echo 'Un problème technique a empêché l\'envoi de l\'email. Veuillez réessayer, ou nous contacter si cela se reproduit.';
      break;
    case 306:
      echo 'Le lien de réinitialisation n\'est plus valide.';
      break;
    case 307:
      echo "Les mots de passe ne correspondent pas.";
      break;
    case 308:
      echo "Merci de remplir tous les champs pour valider votre inscription.";
      break;
    case 309:
      echo 'Ce lien d\'activation n\'est pas valide. Veuillez reessayer.';
      break;
    case 404:
      echo 'Ce lien court n\'existe pas ou a expiré.<br><br>
    Afin de vous protéger, un certain nombre de liens renvoyant sur des sites frauduleux (imitant par exemple la page d\'identification de facebook) ont été supprimés. Pour éviter tout désagrément, vérifiez toujours l\'adresse url du site sur lequel vous êtes avant de renseigner vos informations.';
      break;
    case 405:
      echo 'Ce domaine est blacklisté. Vous ne pouvez pas créer de lien vers ce site.';
      break;
    case 665:
      echo 'Vous venez d\'être blacklisté. Si vous voulez être déblacklisté, vous pouvez écrire un mail à cette adresse : <a href="'.BLACKLISTEMAIL.'">'.BLACKLISTEMAIL.'</a>. Merci de votre compréhension.';
      break;
    default:
      break;
  }
  echo '</div>';
}